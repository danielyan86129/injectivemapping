# handle file: exporting the currently selected vertices as handle constraints
# initial mapping: the current mesh with the handle positions

import bpy, os
from mathutils import Vector
import enum

############## helpers ##############
'''
return mass-center of the given object mesh
'''
def masscenter(mesh):
    bboxvts = [Vector(v) for v in mesh.bound_box]
    local_c = sum(bboxvts, Vector()) / len(bboxvts)
    global_c = mesh.matrix_world * local_c
    return global_c

# windows path
datadir = os.path.join('d:', os.path.sep, 'Yajie', 'code', 'geom-opt', 'InjectiveMapping', 'data', 'lbd_test')
# different test type
class TestType(enum.Enum):
    stretch = 0
    rotation = 1
    bend = 2
    stretch_r = 3
    rotation_r = 4
    bend_r = 5

'''
export vertex groups as handles
'''
def export_handles(filename, testtype_str):
    f = open(filename, 'w+')
    obj = bpy.context.object
    gnms = []
    if TestType[testtype_str] in [TestType.rotation, TestType.rotation_r, TestType.stretch, TestType.stretch_r]:
        gnms = ['top', 'bot']
    else:
        gnms = ['top', 'bot', 'mid']
    gs = [obj.vertex_groups[nm].index for nm in gnms]
    # header: # vts in each group
    for gi in gs:
        # gather verts in this vertex-group
        g_vts = [ v for v in obj.data.vertices if gi in [vg.group for vg in v.groups] ]
        f.write( str(len(g_vts)) + ' ' )
    f.write('\n')
    # then write each group to file
    for gi in gs:
        # gather verts in this vertex-group
        g_vts = [ v for v in obj.data.vertices if gi in [vg.group for vg in v.groups] ]
        # write them to file
        for v in g_vts:
            f.write(str(v.index)+'\n')
    f.close()
    print('Done: handles written to file.')

'''
export current mesh if it does not exist
'''
def export_initmap(filename):
    if os.path.exists(filename):
        return
    bpy.ops.export_scene.obj(filepath=filename, use_selection=True, use_materials=False)

def export_direction(filename, testtype_str):
    f = open(filename, 'w+')
    # query direction with different name depending on test type
    p1name, p2name = '', ''
    if TestType[testtype_str] in [TestType.stretch, TestType.stretch_r, TestType.rotation, TestType.rotation_r]:
        p1name = 'dir.p1'
        p2name = 'dir.p2'
    elif TestType[testtype_str] in [TestType.bend, TestType.bend_r]:
        p1name = 'dir.001.p1'
        p2name = 'dir.001.p2'
    # write direction to file
    p1 = masscenter(bpy.data.objects[p1name])
    p2 = masscenter(bpy.data.objects[p2name])
    dstr = ' '.join([str(x) for x in p1[:]+p2[:]])
    f.write(dstr + '\n')
    f.close()
    print('Done: direction info written to file.')

def export(objname, testtype_str, dir = None):
    if dir is None:
        dir = datadir
    # create shape directory & export shape
    objdir = os.path.join(dir, objname)
    if os.path.exists(objdir) == False:
        print("creating " + str(objdir))
        os.makedirs(objdir)
    initmap_file = os.path.join(objdir, objname+'.obj')
    #export_initmap(initmap_file)
    # create the test case for this shape
    if testtype_str not in TestType.__members__:
        raise Exception('Invalid test type: ' + testtype_str)
    testdir = os.path.join(objdir, testtype_str)
    if os.path.exists(testdir) == False:
        print("creating " + str(testdir))
        os.makedirs(testdir)
    # test case contains a handle file and a meta file
    handle_file = os.path.join(testdir, objname+'_handle.txt')
    export_handles(handle_file, testtype_str)
    direction_file = os.path.join(testdir, objname+'_direction.txt')
    export_direction(direction_file, testtype_str)
    #meta_file = os.path.join(testdir, objname+'_meta.txt')
    #export_meta(meta_file, testtype_str)

#export("dolphin", 'rotation')
#export("dolphin", 'stretch')
#export("dolphin", 'bend')
export("armadillo", 'rotation')
#export("armadillo", 'bend')
#export("armadillo", 'stretch')