# handle file: exporting the currently selected vertices as handle constraints
# initial mapping: the current mesh with the handle positions

import bpy, os

'''
export the selected vertices of currently selected mesh
'''
def export_handles(filename):
    sel_vts = [v for v in bpy.context.active_object.data.vertices if v.select]
    if len(sel_vts) == 0:
        return
    f = open(filename, 'w+')
    for v in sel_vts:
        f.write(str(v.index)+'\n')
    f.close()
    print('Done: handles written to file.')

'''
export currently selected mesh
'''
def export_initmap(filename):
    bpy.ops.export_scene.obj(filepath=filename, use_selection=True, use_materials=False)

def export(objname):
    #dir = os.path.join(os.path.abspath(os.sep), 
    #    'Users', 'yyan', 'Desktop', 'project', 'InjMap', 'data', '3d')
    dir = os.path.join('D:',os.sep,\
        'Yajie','code','geom-opt','InjectiveMapping',\
        'data','3d')
    objdir = os.path.join(dir, objname)
    if os.path.exists(objdir) == False:
        os.mkdir(objdir)
    initmap_file = os.path.join(objdir, objname+'_init.obj')
    export_initmap(initmap_file)
    handle_file = os.path.join(objdir, objname+'_handle.txt')
    export_handles(handle_file)

export("cube-1")