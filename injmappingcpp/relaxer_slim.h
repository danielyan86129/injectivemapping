/*
 Use SLIM to relax positively oriented faces/tets
 Created by Yajie Yan on 4/27/18.
*/


#ifndef INJECTIVEMAPPING_RELAXSOLVER_H
#define INJECTIVEMAPPING_RELAXSOLVER_H

#include "SolverBase.h"
#include <igl/slim.h>

namespace injmap
{
    class RelaxerSLIM : public SolverBase
    {
    public:
        typedef SolverBase Base;
    public:
        RelaxerSLIM();
        ~RelaxerSLIM();

        void initSolver( injmap::SolverData *_data );

        /// run one iteration of solver. output: new positions X_out
        virtual SolverStatus iterate( int _iter );
        /// compute the gradient (flow) at each vertex
        SolverStatus computeStep( int _iter );
        /// take the step (post next V to the output V)
        SolverStatus takeStep( int _iter );

    protected:
        /**
         * data members
         */
        igl::SLIMData m_slim_data; // data for slim solver
    protected:
        /**
         * helpers
         */
        void init_solver_for_good_region( const MatrixXd &_rest_V, const MatrixXd &_init_V,
                                          const MatrixXi &_F,
                                          const VectorXi &_H_ids );
    };
}

#endif //INJECTIVEMAPPING_RELAXSOLVER_H
