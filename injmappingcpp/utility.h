//
// Generally useful helpers
// Created by Yajie Yan on 8/23/17.
//

#ifndef INJECTIVEMAPPING_UTILITY_H
#define INJECTIVEMAPPING_UTILITY_H

#include <cmath>
#include <functional>
#include <chrono>

namespace util
{
    /*****************************
    / General helpers
    ******************************/

    const double eps = 1e-9;
    /**
     * \Brief Is x almost equal to y? (difference smaller than tolerance)
     * @param _t the tolerance
     */
    template<typename T>
    bool equal( T _x, T _y, T _t = (T) eps )
    {
        return std::abs( (T)(_x - _y) ) < _t;
    }

    ///
    /// @Brief implementation of boost::hash_combine
    /// @Link https://stackoverflow.com/questions/4948780/magic-number-in-boosthash-combine
    /// \tparam T
    /// \param [out]_seed
    /// \param _v
    template<typename T>
    void hash_combine( size_t _seed, T const &_v )
    {
        _seed ^= std::hash<T>{}( _v ) + 0x9e3779b9 + (_seed << 6) + (_seed >> 2);
    }
    //
    // the hasher for two numbers
    template<typename T>
    size_t hash( const T &_x, const T &_y )
    {
        auto _seed = 0;
        hash_combine( _seed, _x );
        hash_combine( _seed, _y );
        return _seed;
    }

    /// a light time-struct
    typedef std::chrono::high_resolution_clock myclock;
    struct timecost
    {
        typedef std::chrono::time_point<std::chrono::high_resolution_clock> timepoint;

        typedef std::chrono::duration<double> duration;

        timepoint s, e;

        duration d;

        inline const timepoint& start()
        {
            s = myclock::now();
            return s;
        }
        inline const timepoint& end()
        {
            e = myclock::now();
            elapse();
            return e;
        }
        inline duration elapse()
        {
            d = e - s;
            return d;
        }
    };
}

#endif //INJECTIVEMAPPING_UTILITY_H
