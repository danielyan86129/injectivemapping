/***********************************
** Created by Yajie Yan on 4/30/18.
***********************************/

#include <SolverUtil.h>
#include "symmdirichlet.h"

namespace injmap
{
    SymmDirichletEnergy::SymmDirichletEnergy()
    {}

    SymmDirichletEnergy::SymmDirichletEnergy( const Settings *_s )
    {
        this->setSettings( _s );
    }

    double SymmDirichletEnergy::eval( int _d, const MatrixXd &_X_new, const SparseMatrixXd &_T, const VectorXi &_Fi,
                                      const VectorXd &_FW ) const
    {
        double E = 0.;
        // only compute for given set of face indices
        for ( auto i = 0; i < _Fi.size(); ++i )
        {
            int fi = _Fi( i );
            MatrixXd Ti = util::getMap( _T * _X_new, fi, _d );
            MatrixXd Ti_inv = util::pinv( Ti );
            E += (Ti.squaredNorm() + Ti_inv.squaredNorm()) * _FW( fi );
        }
        E *= 0.5;
        return E;
    }

    void SymmDirichletEnergy::grad( const Eigen::SparseMatrix<double> &_T, const Eigen::VectorXd &_TX,
                                    const Eigen::VectorXd &_W,
                                    const Eigen::MatrixXi &_simplices, const VectorXi &_SI,
                                    VectorXd &_grad ) const
    {
        auto d = _simplices.cols() - 1;
        auto sd = d + 1;
        MatrixXd grad_T_X( d * d, d * sd );
        VectorXi pos_I( d * sd );
        for ( auto i = 0; i < _SI.size(); ++i )
        {
            int fi = _SI( i ); // cur simplex
            double w = _W( fi );
            // grad of energy
            MatrixXd T = util::getMap( _TX, fi, d );
            MatrixXd T_inv = util::pinv( T );
            MatrixXd grad_E_T = T - T_inv.transpose() * T_inv * T_inv.transpose();
            // grad of T w.r.t. X
            EnergyFunction::gradMapT( _T, fi, _simplices.row( fi ), grad_T_X, pos_I );
            // assemble into all-V gradient
            for ( auto j = 0; j < pos_I.size(); ++j )
            {
                _grad( pos_I( j ) ) += w * grad_T_X.transpose().row( j ) * util::vec( grad_E_T );
            }

#ifdef DEBUG
            if ( grad_E_T.array().isInf().any() )
                printf( "Debug: grad_E_T has inf!\n" );
            if ( grad_T_X.array().isInf().any() )
                printf( "Debug: grad_T_X has inf!\n" );
#endif
        }
#ifdef DEBUG
        if ( _grad.array().isInf().any() )
            printf( "Debug: grad_T_X has inf!\n" );
#endif
    }

    VectorXd SymmDirichletEnergy::grad( const SparseMatrix<double> &_T, const VectorXd &_TX, const VectorXd &_metrics,
                                        const MatrixXi &_simplices, const VectorXi _SI ) const
    {
        VectorXd grad_ret = VectorXd::Zero( _TX.size() );
        this->grad( _T, _TX, _metrics, _simplices, _SI, grad_ret );
        return grad_ret;
    }
}
