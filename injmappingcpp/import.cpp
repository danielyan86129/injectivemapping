//
// importer of mesh data (2d and 3d)
// Created by Yajie Yan on 4/14/18.
//

#include "import.h"
#include "logging.h"
#include <igl/readOBJ.h>
#include <igl/readDMAT.h>

namespace util
{
    bool Importer::importMesh2D( const string &_filename, minimesh &_tri_mesh )
    {
        MatrixXd V;
        MatrixXi F;
        auto suc = igl::readOBJ (_filename, V, F);
        if ( !suc )
            return false;

        _tri_mesh = minimesh (V, F);
        return true;
    }
    bool Importer::importMeshData( const string &_rest_file, const string &_init_file, const string &_hdl_file,
                                          util::minimesh &_rest_m, util::minimesh &_init_m, Eigen::VectorXi &_hdls )
    {
        using logging::Logger;

        if ( importMesh2D( _rest_file, _rest_m ) )
            Logger::info( "Rest mesh " + _rest_file + " read in." );
        else
        {
            Logger::fatal( "Cannot read rest mesh." );
            return false;
        }
        if ( importMesh2D( _init_file, _init_m ) )
            Logger::info( "Initial mesh " + _init_file + " read in." );
        else
        {
            Logger::fatal( "Cannot read initial mesh." );
            return false;
        }
        // read in handles
        if ( igl::readDMAT( _hdl_file, _hdls ) )
            Logger::info( "Handles read in." );
        else
        {
            Logger::fatal( "Cannot read handle spec file." );
            return false;
        }
        return true;
    }
    bool Importer::importTetData( const string &_filebase, util::minimesh &_rest, util::minimesh &_init,
                                         Eigen::MatrixXi &_BF, Eigen::MatrixXi &_hdls )
    {
        MatrixXd V;
        MatrixXi T;
        bool suc = true;
        suc = suc && igl::readDMAT( _filebase + ".node.dmat", V );
        suc = suc && igl::readDMAT( _filebase + ".elem.dmat", T );
        _rest = minimesh( V, T );

        suc = suc && igl::readDMAT( _filebase + "_init.node.dmat", V );
        suc = suc && igl::readDMAT( _filebase + "_init.elem.dmat", T );
        _init = minimesh( V, T );

        suc = suc && igl::readDMAT( _filebase + ".bf.dmat", _BF );
        suc = suc && igl::readDMAT( _filebase + "_hdls.dmat", _hdls );

        return suc;
    }
}