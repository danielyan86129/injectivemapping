//
// base class for any optimization class (used only in AugLag solver in internship)
// Created by Yajie Yan on 8/14/17.
//

#ifndef INJECTIVEMAPPING_OPT_OBJECT_H
#define INJECTIVEMAPPING_OPT_OBJECT_H

#include "opt_utility.h"
#include "settings.h"

namespace injmap
{
    class OptObject
    {
    public:
        OptObject()
        {}

        OptObject( const Settings *_s ) : m_s( _s )
        {}

        virtual ~OptObject()
        {}

        void setSettings( const Settings *_s )
        { m_s = _s; }
    protected:
        // the setting that controls the behavior of this object
        const Settings *m_s;
    };
}
#endif //INJECTIVEMAPPING_OPT_OBJECT_H
