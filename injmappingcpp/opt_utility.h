//
// linear algebra utility
// Created by Yajie Yan on 8/14/17.
//

#ifndef INJECTIVMAPPING_OPT_UTILITY_H
#define INJECTIVMAPPING_OPT_UTILITY_H

#include <Eigen/Eigen>
#include <cassert>
#include "logging.h"
#include "utility.h"
#include "geom_utility.h"

namespace injmap
{
    using Eigen::VectorXd;
    using Eigen::VectorXi;
    using Eigen::MatrixXd;
    using Eigen::MatrixXi;
    using Eigen::Map;
    typedef Eigen::Array<bool, Eigen::Dynamic, 1> ArrayXb;
    typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;

    namespace util
    {
        /*****************************
        /  matrix operations additional to Eigen
        ******************************/

        /**
         * \Brief return the rows whose ids are given in *indices*
         */
        inline MatrixXd getEntry( const MatrixXd &_M, const VectorXi &_indices )
        {
            MatrixXd res = MatrixXd::Zero( _indices.size(), _M.cols() );
            for ( auto i = 0; i < _indices.size(); ++i )
                res.row( i ) = _M.row( _indices( i ) );
            return res;
        }

        /**
         * \return _c: Kronecker-product between a and b
         */
        inline void kroneckerProduct( const MatrixXd &_a, const MatrixXd &_b, MatrixXd &_c )
        {
#ifdef DEBUG
            if ( _c.rows() != _a.rows() * _b.rows() ||
                 _c.cols() != _a.cols() * _b.cols() )
                logging::Logger::fatal( "kroneckerProduct: sizes of matrices don't match!" );
#endif
            auto br = _b.rows(), bc = _b.cols();
            for ( auto i = 0; i < _a.rows(); ++i )
                for ( auto j = 0; j < _a.cols(); ++j )
                {
                    _c.block( i * br, j * bc, br, bc ) = _a( i, j ) * _b;
                }
        }

        //
        // vec a matrix
        inline VectorXi vec( const MatrixXi &_m )
        {
            return Map<const VectorXi>( _m.data(), _m.size() );
        }

        inline VectorXd vec( const MatrixXd &_m )
        {
            return Map<const VectorXd>( _m.data(), _m.size() );
        }

        ///
        /// unvec a vector
        /// \tparam VecType type of the vector
        /// \param _v       the vector to unvec
        /// \param _c       desired col #
        /// \return         unvec'ed
        inline MatrixXd unvec( const VectorXd &_v, int _c )
        {
            return Map<const MatrixXd>( _v.data(), _v.size() / _c, _c );
        }

        inline MatrixXi unvec( const VectorXi &_v, int _c )
        {
            return Map<const MatrixXi>( _v.data(), _v.size() / _c, _c );
        }

        ///
        /// unvec a vector representing a map of 2-or-3-dimension
        inline MatrixXd unvecMap( const VectorXd &_V )
        {
            assert( _V.size() == 4 || _V.size() == 9 );
            return unvec( _V, _V.size() == 4 ? 2 : 3 );
        }

        // the structure containing the svd results
        struct svdstruct
        {
            MatrixXd U;
            MatrixXd V;
            VectorXd S;
        };

        /// \return the map in vector form for i-th simplex (of dim d)
        inline VectorXd getMapVec( const VectorXd &_TX, int _t, int _d )
        {
            return _TX.segment( _d * _d * _t, _d * _d );
        };

        /// \return the map for i-th simplex (of dim d) from TX, the col-stacked maps
        inline MatrixXd getMap( const VectorXd &_TX, int _t, int _d )
        {
            return unvecMap( _TX.segment( _d * _d * _t, _d * _d ) );
        };

        //
        // determine flip-or-not flag for a mapping
        inline bool isflipping( const MatrixXd &_m )
        {
            return _m.determinant() >= 0;
        }

        //
        // compute signed-svd decomposition for a list of maps
        inline void computeSVDs( const VectorXd &_TX, int _d, int _m, std::vector<svdstruct> &_svds )
        {
            _svds.resize( _m );
            for ( auto i = 0; i < _m; ++i )
            {
                const auto &Jt = getMap( _TX, i, _d ); // the map
                auto jt_svd = Jt.jacobiSvd( Eigen::ComputeFullU | Eigen::ComputeFullV );
                auto &svd = _svds[i];
                svd.U = jt_svd.matrixU();
                svd.V = jt_svd.matrixV();
                svd.S = jt_svd.singularValues();
                // adjust singular value according to flip-ness of the map
                if ( Jt.determinant() < 0.0 )
                {
                    svd.S( _d - 1 ) *= -1;
                    svd.U.col( _d - 1 ) *= -1;
                }
            }
        }

        ///
        /// \Brief find the closest rotation for each svd representing a map
        /// \param [in] _svds
        /// \param [out] _rots A dd * m matrix with col i being the rotation map for simplex i
        inline void findRots( const std::vector<svdstruct> &_svds, MatrixXd &_rots )
        {
            for ( auto i = 0; i < _svds.size(); ++i )
            {
                const auto &svd = _svds[i];
                _rots.col( i ) = util::vec( (const MatrixXd &) (svd.U * svd.V.transpose()) );
            }
        }

        //
        // start and end of the entries in TX
        inline Eigen::Vector2i rowrangeOfSimp( int _t, int _d )
        {
            return {_d * _d * _t, _d * _d * (_t + 1) - 1};
        }

        //
        // area/volume of simplices for given mesh
        inline void metric( const ::util::minimesh &_mesh, VectorXd &_areas )
        {
            Eigen::Vector3d u, v, w;
            u.setZero();
            v.setZero();
            w.setZero();
            double a = 0.0;
            for ( auto i = 0; i < _mesh.m(); ++i )
            {
                const auto &f = _mesh.F().row( i );
                u = _mesh.V3d().row( f( 0 ) ) - _mesh.V3d().row( f( 1 ) );
                v = _mesh.V3d().row( f( 0 ) ) - _mesh.V3d().row( f( 2 ) );
                if ( _mesh.d() == 2 )
                    a = 0.5 * (u.cross( v ).norm());
                else if ( _mesh.d() == 3 )
                {
                    w = _mesh.V3d().row( f( 0 ) ) - _mesh.V3d().row( f( 3 ) );
                    a = (1.0 / 6.0) * std::abs( u.cross( v ).dot( w ) );
                }
                _areas( i ) = a;
            }
        }

        //
        // index of entries in X for vertex i
        inline VectorXi idsOfVertexInX( int _i, int _d, int _n )
        {
            VectorXi ids( _d );
            if ( _d >= 2 )
            {
                ids( 0 ) = _i;
                ids( 1 ) = ids( 0 ) + _n;
            }
            if ( _d == 3 )
            {
                ids( 2 ) = ids( 1 ) + _n;
            }
            return ids;
        }

        //
        // pseudo-inverse of a matrix
        template<class MatT>
        Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
        pinv( const MatT &mat,
              typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4} /* choose appropriately*/)
        {
            typedef typename MatT::Scalar Scalar;
            auto svd = mat.jacobiSvd( Eigen::ComputeFullU | Eigen::ComputeFullV );
            const auto &singularValues = svd.singularValues();
            Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
                    singularValuesInv( mat.cols(), mat.rows() );
            singularValuesInv.setZero();
            for ( unsigned int i = 0; i < singularValues.size(); ++i )
            {
                if ( singularValues( i ) > tolerance )
                {
                    singularValuesInv( i, i ) = Scalar{1} / singularValues( i );
                }
                else
                {
                    singularValuesInv( i, i ) = Scalar{0};
                }
            }
            return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
        }

        //
        // normalize each row / col of given matrix (dim = 1 / 2 for row / col)
        inline void normalizeRowCol( const MatrixXd &_X, MatrixXd &_Y, int _dim = 1, double _tol = 1e-7 )
        {
            _Y = _X;
            if ( _dim == 1 )
            {
                for ( auto i = 0; i < _Y.rows(); ++i )
                {
                    double len = _Y.row( i ).norm();
                    if ( len > _tol )
                        _Y.row( i ) /= len;
                }
            }
            else // normalize each col
            {
                for ( auto i = 0; i < _Y.cols(); ++i )
                {
                    double len = _Y.col( i ).norm();
                    if ( len > _tol )
                        _Y.col( i ) /= len;
                }
            }
        }
    }
}

#endif //INJECTIVMAPPING_OPT_UTILITY_H
