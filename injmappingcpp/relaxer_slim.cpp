//
// Use SLIM to relax positively oriented faces/tets
// Created by Yajie Yan on 4/27/18.
//

#include <igl/slice_mask.h>
#include <igl/slice.h>
#include <igl/setunion.h>
#include "relaxer_slim.h"

namespace injmap
{
    RelaxerSLIM::RelaxerSLIM()
    {

    }

    RelaxerSLIM::~RelaxerSLIM()
    {

    }

    void RelaxerSLIM::initSolver( injmap::SolverData *_data )
    {
        SolverBase::initSolver( _data );
    }

    SolverStatus RelaxerSLIM::iterate( int _iter )
    {

        return SUCCESS;
    }

    SolverStatus RelaxerSLIM::computeStep( int _iter )
    {
        auto rest_m = Base::m_data->rest_m;
        auto init_m = Base::m_data->init_m;
        auto out_m = Base::m_data->out_m;
        auto &Xnext = Base::m_data->Xnext;
        auto hdls = Base::m_data->hdls;
        auto &flip = Base::m_data->flip;

        // initialize information
        int d = init_m->F().cols() - 1;
        int m = init_m->F().rows();
        int n = init_m->n();
        const MatrixXd &V_prev = out_m->V3d(); // by default prev & next are all prev output
        Xnext = out_m->V3d();

        if ( !(flip.array() == 0.).any() ) // only continue if there are good face(s)
            return SKIPPED;

        MatrixXi good_F = igl::slice_mask( init_m->F(), (flip.array() == 0.), 1 );
        MatrixXi bad_F = SolverUtil::getFlipFaces( init_m->F(), flip );
        VectorXi tmp1, tmp2;

        /*** relax good faces ***/
        // find hdls for inj-only solver
        VectorXi injonly_H, bad_bnd_Vi; // again, all inj-only hdls = original + boundary of bad faces
        SolverUtil::getBoundaryVids( V_prev, bad_F, bad_bnd_Vi );
        igl::setunion( bad_bnd_Vi, *hdls, injonly_H, tmp1, tmp2 );
        if ( injonly_H.size() == n ) // skip if there is no freedom for inj solver
        {
            printf( "relax: skipped due to 0 freedom.\n" );
            return SUCCESS;
        }
        // else, init the solver
        init_solver_for_good_region( rest_m->V3d(), V_prev.leftCols( d ), good_F, injonly_H );
        printf( "Done: inj-solver updated. \n" );
        // do injective solve
        int inj_iter_n = 10;
        //m_slim_data.exp_factor = 5.;
        MatrixXd tmp_Vo = igl::slim_solve( m_slim_data, inj_iter_n );
        Xnext = m_slim_data.V_o;
        minimesh::augmentVtsForRender( Xnext );
        printf( "Done: inj-solver solved. \n" );
        m_step = Xnext - V_prev;
        double len_step = m_step.norm();
        if ( std::isnan( len_step ) || std::isinf( len_step ) )
        {
            printf( "Error: relax step len = %f \n", len_step );
            exit( -1 );
        }
        printf( "relax: #hdls,step = %d,%f\n", (int) injonly_H.size(), len_step );

        return SUCCESS;
    }

    SolverStatus RelaxerSLIM::takeStep( int _iter )
    {
        auto out_m = Base::m_data->out_m;
        // post Xnext to out mesh X
        out_m->setV( Base::m_data->Xnext );
        // update flip
        SolverUtil::detFlips( out_m->d(), out_m->m(), m_data->T, out_m->V3d(), 0., m_data->flip );

        return SUCCESS;
    }

    /**
     * helpers
     */
    void RelaxerSLIM::init_solver_for_good_region( const MatrixXd &_rest_V, const MatrixXd &_init_V,
                                                   const MatrixXi &_F,
                                                   const VectorXi &_H_ids )
    {
//        if ( m_slim_data.has_pre_calc && (m_slim_data.b.array() == _H_ids.array()).all() )
//        {
//            m_slim_data.V_o = _init_V; // only need to update init V
//            m_slim_data.energy = igl::slim::compute_energy( m_slim_data, const_cast<MatrixXd&>(_init_V) );
//            return;
//        }

        m_slim_data.has_pre_calc = false;
        m_slim_data.first_solve = false;

        int d = _F.cols() - 1;
        MatrixXd hdl_P = igl::slice( _init_V, _H_ids, 1 ).leftCols( d );
        double hdl_penalty = 1e7;
        igl::slim_precompute( _rest_V, _F, _init_V, m_slim_data,
                              igl::SLIMData::SLIM_ENERGY::SYMMETRIC_DIRICHLET, const_cast<VectorXi &>(_H_ids),
                              hdl_P,
                              hdl_penalty );

        printf( "Done: inj-solver re-inited. " );
    }

}