//
// The solver that performs flow & relaxation iteratively.
// Created by Yajie Yan on 4/15/18.
//

#include "IterInjSolver.h"
#include "SolverUtil.h"
#include "rendersubmit.h"
#include "my_cotmatrix.h"
#include "flowsolver.h"
#include "relaxer_slim.h"
#include "relaxer_precond_gd.h"
#include <igl/massmatrix.h>
#include <igl/slice_mask.h>
#include <igl/slice_into.h>
#include <igl/is_border_vertex.h>
#include <igl/unique_rows.h>
#include <igl/flip_avoiding_line_search.h>
#include <igl/vertex_triangle_adjacency.h>
#include <igl/slim.h>
#include <igl/setunion.h>
#include <igl/sort.h>
#include <functional>

namespace injmap
{
    IterInjSolver::IterInjSolver()
    {
        m_flow_solver = nullptr;
        m_inj_solver = nullptr;
        m_flow_cnt = 0;
        m_relax_cnt = 0;
    }

    IterInjSolver::~IterInjSolver()
    {
        if ( m_flow_solver )
            delete m_flow_solver;
        if ( m_inj_solver )
            delete m_inj_solver;
    }

    void IterInjSolver::initSolver( SolverData *_data )
    {
        /// init base solver
        Base::initSolver( _data );

        /// init information
        int d = m_data->init_m->d();
        int m = m_data->init_m->m();

        /// configure flow solver
        m_flow_solver = (SolverBase *) new FlowSolver();
        m_flow_solver->initSolver( _data );
        m_flow_n = 1;
        printf( "Done: flow solver init.ed\n" );

        /// configure rlax solver
        //m_inj_solver = (SolverBase *) new RelaxerSLIM();
        m_inj_solver = (SolverBase *)
                new RelaxerPrecondGD();
        m_inj_solver->initSolver( _data );
        m_relax_n = 4;
        printf( "Done: relaxation solver init.ed\n" );

        /// init other states
        m_cur_step = COMPUTE_FLOW;
    }

    void IterInjSolver::setFlowN( int _n_flow )
    {
        this->m_flow_n = _n_flow;
    }

    void IterInjSolver::setViewer( igl::opengl::glfw::Viewer &_viewer )
    {
        m_viewer_ptr = &_viewer;
    }

    void IterInjSolver::initViewer()
    {
        auto init_m = Base::m_data->init_m;
        auto out_m = Base::m_data->out_m;
        auto &hdls = Base::m_data->hdls;
        auto &flip = Base::m_data->flip;

        // upload mesh
        m_viewer_ptr->data().set_mesh( init_m->V3d(), init_m->F() );
        Eigen::RowVector3d mesh_C( 0.6, 0.6, 0.6 );
        m_viewer_ptr->data().set_colors( mesh_C );
        // add handle points
        auto green = Eigen::RowVector3d( 0.0, 0.7, 0.0 );
        m_viewer_ptr->data().set_points( igl::slice( init_m->V3d(), *hdls, 1 ), green );
        // upload flipped faces' edges
        MatrixXi E;
        MatrixXd E_C;
        getEdgesAndColorsOfFlipped( out_m->V3d(), init_m->F(), flip, E, E_C );
        m_viewer_ptr->data().set_edges( out_m->V3d(), E, E_C );
        // settings
        m_viewer_ptr->core.background_color = Eigen::Vector4f( 0.8, 0.8, 0.8, 1.0 );
        m_viewer_ptr->core.lighting_factor = 0.0f;
        m_viewer_ptr->data().show_overlay_depth = false;
        m_viewer_ptr->data().show_lines = false;
        m_viewer_ptr->data().line_width = 15.0;
    }

    void IterInjSolver::getPerVertStep( MatrixXd &_dir ) const
    {

    }

    SolverStatus IterInjSolver::state() const
    {
        if ( (m_data->flip.array() > 0.).count() == 0 )
            return SolverStatus::CONVERGED;
        return SolverStatus::CONTINUE;
    }

    void IterInjSolver::setIterMode( IterMode _itermode )
    {
        Base::setIterMode( _itermode );
        if ( _itermode == BY_STEP )
            dynamic_cast<RelaxerPrecondGD *>(m_inj_solver)->setFixBadBoundary( true );
        else if ( _itermode == BY_STEP_MIX )
            dynamic_cast<RelaxerPrecondGD *>(m_inj_solver)->setFixBadBoundary( false );
    }

    SolverStatus IterInjSolver::iterate( int _iter )
    {
        if ( m_iter_mode == STOP )
            return SUCCESS;

        // initialize information
        auto init_m = Base::m_data->init_m;
        auto out_m = Base::m_data->out_m;
        auto &hdls = Base::m_data->hdls;
        auto &flip = Base::m_data->flip;
        int d = init_m->F().cols() - 1;
        int m = init_m->F().rows();
        MatrixXd V_prev = out_m->V3d();
        SolverStatus err = SUCCESS;

        if ( m_iter_mode == FULL_ITER || m_iter_mode == NON_STOP )
        {
            printf( "--------- Flow ---------\n" );
            while ( can_flow_more() )
            {
                m_cur_step = COMPUTE_FLOW;
                computeFlowStep( _iter );
                m_cur_step = TAKE_FLOW;
                takeFlow( _iter );
                m_flow_cnt++;
            }
            printf( "----------- Flow performed %d times -----------\n", m_flow_cnt );
            m_flow_cnt = 0; // reset flow counter
            printf( "--------- Relax ---------\n" );
            while ( can_relax_more() )
            {
                m_cur_step = COMPUTE_RELAX;
                err = computeRelaxStep( _iter );
                if ( err != SolverStatus::SUCCESS && err != SolverStatus::SKIPPED )
                {
                    printf( "Relax step is not taken due to issues.\n" );
                    break;
                }
                m_cur_step = TAKE_RELAX;
                err = takeRelax( _iter );
                m_relax_cnt++;
                if ( err != SolverStatus::SUCCESS )
                    break;
            }
            printf( "----------- Relax performed %d times -----------\n", m_relax_cnt );
            m_relax_cnt = 0;
        }
        else // run one step at a time
            run_one_step( m_iter_mode, _iter );
//        else if ( m_iter_mode == BY_STEP ) // perform one step a time
//        {
//            if ( m_cur_step == COMPUTE_FLOW )
//            {
//                printf( "--------- Flow ---------\n" );
//                computeFlowStep( _iter );
//                m_cur_step = TAKE_FLOW;
//            }
//            else if ( m_cur_step == TAKE_FLOW )
//            {
//                takeFlow( _iter );
//                m_flow_cnt++;
//                printf( "flow: step taken.\n" );
//                if ( can_flow_more() ) // still in multi-flow stage
//                    m_cur_step = COMPUTE_FLOW;
//                else
//                {
//                    printf( "--------- next relax ---------\n" );
//                    m_flow_cnt = 0;
//                    m_cur_step = COMPUTE_RELAX;
//                }
//            }
//            else if ( m_cur_step == COMPUTE_RELAX )
//            {
//                printf( "--------- Relax ---------\n" );
//                computeRelaxStep( _iter );
//                m_cur_step = TAKE_RELAX;
//            }
//            else // take relax
//            {
//                takeRelax( _iter );
//                m_relax_cnt++;
//                printf( "relax: step taken.\n" );
//                if ( can_relax_more() )
//                    m_cur_step = COMPUTE_RELAX;
//                else
//                {
//                    printf( "--------- next flow ---------\n" );
//                    m_relax_cnt = 0;
//                    m_cur_step = COMPUTE_FLOW;
//                }
//            }
//        }
//        else if ( m_iter_mode == BY_STEP_MIX )
//        {
//            if ( aux_dirs.size() != 3 )
//                aux_dirs.resize( 3 ); // will output 3 type of dir.s
//            if ( m_cur_step == COMPUTE_FLOW || m_cur_step == COMPUTE_RELAX )
//                computeMixStep( _iter );
//            else
//                takeMix( _iter );
//        } // end BY_STEP_MIX

        /// prepare aux-dir.s for other modes
        if ( m_iter_mode == STOP || m_iter_mode == BY_STEP || m_iter_mode == FULL_ITER ||
             (m_iter_mode == NON_STOP && _iter % 5 == 0) )
        {
            aux_dirs.clear();
            MatrixXd dir;
            aux_dirs.push_back( m_step );
            /*// upload search direction
            util::normalizeRowCol( Base::m_p, dir, 1 );
            dir *= m_data->init_m->avgEdgeLen() * 0.1;
            aux_dirs.push_back( dir );
            // upload -gradient of energy
            util::normalizeRowCol( -Base::m_grad, dir, 1 );
            dir *= m_data->init_m->avgEdgeLen() * 0.1;
            aux_dirs.push_back( dir );*/
        }
        /// submit dir.s for render
        if ( m_iter_mode != NON_STOP || (m_iter_mode == NON_STOP && _iter % 5 == 0) )
        {
            MatrixXd start = V_prev;
            // upload aux-dir.s (blue, green, yellow, ...)
            MatrixXd dir;
            if ( aux_dirs.size() > 0 )
            {
                dir = aux_dirs[0];
                RenderSubmit::setLines( *m_viewer_ptr, start, dir, Eigen::RowVector3d( 0.0, 0.0, 0.6 ) );
            }
            if ( aux_dirs.size() > 1 )
            {
                dir = aux_dirs[1];
                m_viewer_ptr->data().add_edges( start, start + dir, Eigen::RowVector3d( 0., 0.6, 0. ) );
            }
            if ( aux_dirs.size() > 2 )
            {
                dir = aux_dirs[2];
                m_viewer_ptr->data().add_edges( start, start + dir, Eigen::RowVector3d( 0.8, 0.8, 0. ) );
            }
            // upload face edges and colors
            MatrixXd P1, P2, E_C;
            getEdgesAndColorsOfFlipped( out_m->V3d(), init_m->F(), flip, P1, P2, E_C );
            m_viewer_ptr->data().add_edges( P1, P2, E_C );

            // handle points: those for inj solver
        }
        return SolverStatus::SUCCESS;
    }

    SolverStatus IterInjSolver::computeStep( int _iter )
    {
        return SUCCESS;
    }

    SolverStatus IterInjSolver::computeFlowStep( int _iter )
    {
        SolverStatus err = m_flow_solver->computeStep( _iter );
        if ( err == SUCCESS || err == SKIPPED )
        {// sync with that solver: extract the step ...
            m_flow_solver->getPerVertStep( Base::m_step );
            m_inj_solver->getLinesearch( Base::m_p );
            m_inj_solver->getPerVertGrad( Base::m_grad );
        }
        else
        {
            if ( err == LINFACTOR_FAIL )
                printf( "Cannot factorize!\n" );
            else if ( err == LINSOLVE_FAIL )
                printf( "Cannot backsolve!\n" );
            printf( "Failed to compute flow! Skipped.\n" );
        }

        return err;
    }

    SolverStatus IterInjSolver::computeRelaxStep( int _iter )
    {
        SolverStatus err = m_inj_solver->computeStep( _iter );
        if ( err == SKIPPED )
            printf( "relax is skipped\n" );
        if ( err == SUCCESS || err == SKIPPED )
        {// sync: get the step
            m_inj_solver->getPerVertStep( Base::m_step );
            m_inj_solver->getLinesearch( Base::m_p );
            m_inj_solver->getPerVertGrad( Base::m_grad );
            printf( "relax: |step| = %f\n", Base::m_step.norm() );
            //std::cout << Base::step.array() << std::endl;
        }
        return err;
    }

    SolverStatus IterInjSolver::takeStep( int _iter )
    {
        return SUCCESS;
    }

    SolverStatus IterInjSolver::takeFlow( int _iter )
    {
        // initialize info
        VectorXd tmp_flip = Base::m_data->flip;

        int n_flips_prev = (tmp_flip.array() > 0.0).count();
        m_flow_solver->takeStep( _iter );
        int n_flips_next = (Base::m_data->flip.array() > 0.0).count();
        if ( n_flips_next != n_flips_prev )
        {
            Base::m_data->pre_flip = tmp_flip; // flipness changed. cur flip becomes old flip.
            printf( "flow flipness changed: n-flips before,after = %d,%d\n", n_flips_prev, n_flips_next );
        }

        return SolverStatus::SUCCESS;
    }

    SolverStatus IterInjSolver::takeRelax( int _iter )
    {
        // initialize info
        const auto &pre_flip = Base::m_data->flip;
        VectorXd cur_flip = pre_flip;
        MatrixXd Xnext;
        m_inj_solver->getTempOutX( Xnext );
        SolverUtil::detFlips( m_data->out_m->d(), m_data->out_m->m(), m_data->T, Xnext, 0., cur_flip );
        int n_flips_prev = (pre_flip.array() > 0.0).count();
        int n_flips_next = (cur_flip.array() > 0.0).count();
        if ( n_flips_next > n_flips_prev )
        {
            printf( "relax: step not taken as it flips good faces!\n" );
            return SKIPPED;
        }
        m_inj_solver->takeStep( _iter );
        printf( "relax: n-flips before,after = %d,%d\n", n_flips_prev, n_flips_next );

        return SolverStatus::SUCCESS;
    }

    SolverStatus IterInjSolver::computeMixStep( int _iter )
    {
        auto out_m = m_data->out_m;
        auto flip = m_data->flip;
        const auto &Xprev = out_m->V3d();
        auto d = out_m->d();

        if ( m_cur_step == COMPUTE_FLOW || m_cur_step == COMPUTE_RELAX )
        {
            //if ( (flip.array() > 0.).count() == 0 )
            //    m_cur_step = COMPUTE_RELAX;

            // compute flow & relax step
            MatrixXd flow_step, relax_step, mix_step;
            auto flow_err = computeFlowStep( _iter );
            if ( m_cur_step == COMPUTE_FLOW )
                dynamic_cast<RelaxerPrecondGD *>(m_inj_solver)->setFixBadBoundary( false );
            else
                dynamic_cast<RelaxerPrecondGD *>(m_inj_solver)->setFixBadBoundary( true );
            auto relax_err = computeRelaxStep( _iter );
            m_flow_solver->getPerVertStep( flow_step );
            m_inj_solver->getPerVertStep( relax_step );
            mix_step = flow_step;
            Base::m_step.setZero();

            printf( "mixing flow / relax step = %f,%f\n", flow_step.norm(), relax_step.norm() );
            // mix flow and relax step
            if ( m_cur_step == COMPUTE_FLOW ) // prefer flow step
            {
                double w = 0.5;
                for ( auto i = 0; i < flow_step.rows(); ++i )
                {
                    VectorXd flow_i = flow_step.row( i );
                    VectorXd relax_i = relax_step.row( i );
                    if ( flow_i.norm() < relax_i.norm() )
                        mix_step.row( i ) = flow_i + w * flow_i.norm() / relax_i.norm() * relax_i;
                    else
                        mix_step.row( i ) = flow_i + w * relax_i;
                    //mix_step *= 10.0;
                }
                auto flow_solver_ptr = dynamic_cast<FlowSolver *>(m_flow_solver);
                double s = flow_solver_ptr->nonFlipFlow( Xprev, out_m->F(), mix_step, flip );
                //double s = 0.8 * igl::flip_avoiding::compute_max_step_from_singularities(
                //        Xprev.leftCols( d ), out_m->F(), mix_step );
                Base::m_step = mix_step * s;
                printf( "Mixed step (favoring flow) = %f\n", Base::m_step.norm() );

                m_cur_step = TAKE_FLOW;
            }
            else // m_cur_step == COMPUTE_RELAX // prefer relax step
            {
                for ( auto i = 0; i < relax_step.rows(); ++i )
                {
                    VectorXd flow_i = flow_step.row( i );
                    VectorXd relax_i = relax_step.row( i );
                    if ( relax_i.norm() < flow_i.norm() )
                    {
                        mix_step.row( i ) = relax_i.norm() / flow_i.norm() * flow_i + relax_i;
                    }
                    else
                    {
                        mix_step.row( i ) = flow_i + relax_i;
                    }
                }
                MatrixXi good_F = SolverUtil::getNonFlipFaces( out_m->F(), flip );
                double s = 0.8 * igl::flip_avoiding::compute_max_step_from_singularities(
                        Xprev.leftCols( d ), good_F, mix_step );
                Base::m_step = mix_step * s;
                printf( "Mixed step (favoring relax) = %f\n", Base::m_step.norm() );

                m_cur_step = TAKE_RELAX;
            }
            // update mix, flow, and relax step
            aux_dirs[0] = Base::m_step;
            aux_dirs[1] = flow_step;
            aux_dirs[2] = relax_step;
        }
        return SUCCESS;
    }

    SolverStatus IterInjSolver::takeMix( int _iter )
    {
        auto out_m = m_data->out_m;
        auto flip = m_data->flip;
        const auto &Xprev = out_m->V3d();
        auto d = out_m->d();
        auto m = out_m->m();
        if ( m_cur_step == TAKE_FLOW || m_cur_step == TAKE_RELAX ) // just take the step
        {
            out_m->setV( Xprev + Base::m_step ); // update output X
            VectorXd tmp_flip = Base::m_data->flip;
            int n_flips_prev = (tmp_flip.array() > 0.0).count();
            SolverUtil::detFlips( d, m, m_data->T, out_m->V3d(), 0., m_data->flip ); // update flipness
            int n_flips_next = (Base::m_data->flip.array() > 0.0).count();
            printf( "after mix step, n-flips before,after = %d,%d\n",
                    n_flips_prev, n_flips_next );
        }
        return SUCCESS;
    }

    bool IterInjSolver::can_flow_more() const
    {
        int n_flip = (m_data->flip.array() > 0.).count();
        bool is_flip_changed =
                (m_data->flip.array() > 0.).count() != (m_data->pre_flip.array() > 0.).count();
        return n_flip > 0 && m_flow_cnt < m_flow_n &&
               !is_flip_changed && m_flow_solver->state() == CONTINUE;
    }

    bool IterInjSolver::can_relax_more() const
    {
        return m_relax_cnt < m_relax_n && m_inj_solver->state() == CONTINUE;
    }

    SolverStatus IterInjSolver::run_one_step( IterMode _mode, int _iter )
    {
        std::function<SolverStatus( int )> flow_func;
        std::function<SolverStatus( int )> relax_func;
        std::function<SolverStatus( int )> take_flow_func;
        std::function<SolverStatus( int )> take_relax_func;
        if ( _mode == IterMode::BY_STEP )
        {
            flow_func = [&]( int i ) -> SolverStatus
            { return this->computeFlowStep( i ); };
            relax_func = [&]( int i ) -> SolverStatus
            { return this->computeRelaxStep( i ); };
            take_flow_func = [&]( int i ) -> SolverStatus
            { return this->takeFlow( i ); };
            take_relax_func = [&]( int i ) -> SolverStatus
            { return this->takeFlow( i ); };
        }
        else if ( _mode == IterMode::BY_STEP_MIX )
        {
            flow_func = [&]( int i ) -> SolverStatus
            { return this->computeMixStep( i ); };
            relax_func = [&]( int i ) -> SolverStatus
            { return this->computeMixStep( i ); };
            take_flow_func = [&]( int i ) -> SolverStatus
            { return this->takeMix( i ); };
            take_relax_func = [&]( int i ) -> SolverStatus
            { return this->takeMix( i ); };
            if ( aux_dirs.size() != 3 )
                aux_dirs.resize( 3 );
        }
        if ( m_cur_step == COMPUTE_FLOW )
        {
            printf( "--------- Flow ---------\n" );
            flow_func( _iter );
            m_cur_step = TAKE_FLOW;
        }
        else if ( m_cur_step == TAKE_FLOW )
        {
            take_flow_func( _iter );
            m_flow_cnt++;
            printf( "flow: step taken.\n" );
            if ( can_flow_more() ) // still in multi-flow stage
                m_cur_step = COMPUTE_FLOW;
            else
            {
                printf( "--------- next relax ---------\n" );
                m_flow_cnt = 0;
                m_cur_step = COMPUTE_RELAX;
            }
        }
        else if ( m_cur_step == COMPUTE_RELAX )
        {
            printf( "--------- Relax ---------\n" );
            relax_func( _iter );
            m_cur_step = TAKE_RELAX;
        }
        else // take relax
        {
            take_relax_func( _iter );
            m_relax_cnt++;
            printf( "relax: step taken.\n" );
            if ( can_relax_more() )
                m_cur_step = COMPUTE_RELAX;
            else
            {
                printf( "--------- next flow ---------\n" );
                m_relax_cnt = 0;
                m_cur_step = COMPUTE_FLOW;
            }
        }
        return SUCCESS;
    } // run_one_step

    void IterInjSolver::getEdgesAndColorsOfFlipped( const MatrixXd &_V, const MatrixXi &_F,
                                                    const VectorXd &_flips,
                                                    MatrixXi &_E, MatrixXd &_color ) const
    {
        int sd = _F.cols();
        int d = sd - 1;
        MatrixXi es_f( d == 2 ? 3 : 6, 2 ); // space for edges of a simplex
        auto flip_c = Eigen::RowVector3d( 1.0, 0.0, 0.0 ); // flip color
        auto deform_c = Eigen::RowVector3d( 0.0, 0.0, 1.0 ); // deform color

        // \Note: this comes last so it can take highest priority
        // (by overriding the ealier set colors)
        // set flipped faces' edges to a color
        ArrayXb is_flipped = (_flips.array() > 0.0);
        int n_flipped = is_flipped.count();
        _color.resize( n_flipped * es_f.rows(), 3 );
        _E.resize( n_flipped * es_f.rows(), 2 );
        int k = 0;
        for ( auto i = 0; i < _flips.size(); ++i )
        {
            if ( !is_flipped( i ) )
                continue;
            minimesh::mkEdgesFromSimp( _F.row( i ), es_f );
            for ( auto j = 0; j < es_f.rows(); ++j )
            {
                _color.row( k ) = flip_c;
                auto e = es_f.row( j );
                _E.row( k ) = e;
                k++;
            }
        }
    }

    void IterInjSolver::getEdgesAndColorsOfFlipped( const MatrixXd &_V, const MatrixXi &_F,
                                                    const VectorXd &_flips,
                                                    MatrixXd &_P1, MatrixXd &_P2, MatrixXd &_color ) const
    {
        MatrixXi E;
        getEdgesAndColorsOfFlipped( _V, _F, _flips, E, _color );
        _P1 = igl::slice( _V, E.col( 0 ), 1 );
        _P2 = igl::slice( _V, E.col( 1 ), 1 );
    }


}