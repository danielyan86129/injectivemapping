//
// Created by Yajie Yan on 8/31/17.
//

#include "debug.h"
#include <igl/writeDMAT.h>
#include <Eigen/Eigen>

using namespace injmap::dbg;
using namespace Eigen;

bool iterinfodbg::exportToFiles( const string& _base ) const
{
    auto to_vec = []( const vector<double>& _x ) {
        return Map<const MatrixXd>( _x.data(), _x.size(), 1 );
    };

    bool suc = true;
    if ( !F.empty() )
        suc &= igl::writeDMAT( _base + "F.dmat", to_vec( F ) );
    if ( !D.empty() )
        suc &= igl::writeDMAT( _base + "D.dmat", to_vec( D ) );
    if ( !BD.empty() )
        suc &= igl::writeDMAT( _base + "BD.dmat", to_vec( BD ) );
    if ( !lsq.empty() )
        suc &= igl::writeDMAT( _base + "lsq.dmat", to_vec( lsq ) );
    if ( !arap.empty() )
        suc &= igl::writeDMAT( _base + "arap.dmat", to_vec( arap ) );
    if ( !pmax.empty() )
        suc &= igl::writeDMAT( _base + "pmax.dmat", to_vec( pmax ) );
    if ( !step.empty() )
        suc &= igl::writeDMAT( _base + "step.dmat", to_vec( step ) );
    if ( !pstep.empty() )
        suc &= igl::writeDMAT( _base + "pstep.dmat", to_vec( pstep ) );
    if ( !flipness.empty() )
        suc &= igl::writeDMAT( _base + "flipness.dmat", to_vec( flipness ) );
    if ( !nflips.empty() )
        suc &= igl::writeDMAT( _base + "nflips.dmat", to_vec( nflips ) );

    return suc;
}

void iterinfodbg::clear()
{
    F.clear();
    D.clear();
    BD.clear();
    lsq.clear();
    arap.clear();
    pmax.clear();
    step.clear();
    pstep.clear();
    flipness.clear();
    nflips.clear();
}
