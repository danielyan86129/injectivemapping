//
// Created by Yajie Yan on 8/14/17.
// Definition of a few types of energy functions
//

#ifndef INJECTIVEMAPPING_ENERGY_FUNC_H
#define INJECTIVEMAPPING_ENERGY_FUNC_H

#include <Eigen/Eigen>
#include <vector>
#include <iostream>
#include "settings.h"
#include "opt_object.h"

namespace injmap
{
    using namespace Eigen;
    using std::cout; using std::cerr; using std::endl;

    /************** base class for energy function **************/
    class EnergyFunction : public OptObject
    {
    public:
        EnergyFunction();
        virtual ~EnergyFunction()
        {};

        virtual double eval()
        {
            cerr << "Fatal error: EnergyFunction::evalForSimp() is not implemented!";
            exit( -1 );
        }
        virtual VectorXd grad()
        {
            cerr << "Fatal error: EnergyFunction::gradForSimp() is not implemented!";
            exit( -1 );
        }

        /*****************************
        / common functions for all derived classes
        ******************************/
        ///
        /// \brief compute gradient of map w.r.t. the vertices of the given simplex
        /// \param _T
        /// \param _t
        /// \param _simp_t
        /// @param [out] _gr_Jt: d.d * d.sd, the resulting gradient matrix
        /// (#Jt's entries -by- #vts-coords for simplex t).
        /// @param [out] _pos: d*sd vector. each entry specifies where the corresponding
        /// column of _gr_Jt should be placed in the full dimensional gradient w.r.t. all vts X
        void gradMapT( const SparseMatrix<double> &_T, int _t, const VectorXi &_simp_t,
                       MatrixXd &_gr_Jt, VectorXi &_pos ) const;
    };

    /******Least-square energy term*****/
    class EnergyLSQ : public EnergyFunction
    {
    public:
        EnergyLSQ();
        ~EnergyLSQ() = default;
        explicit EnergyLSQ( const Settings *_s );

        double eval(
                const VectorXd &_TX, const VectorXd &_TXr,
                const VectorXd &_areas ) const;
        VectorXd grad(
                const SparseMatrix<double> &_T,
                const VectorXd &_TX, const VectorXd &_TXr,
                const VectorXd &_areas, const MatrixXi &_simplices ) const;
    };

    /**************ARAP energy term**************/
    class EnergyARAP : public EnergyFunction
    {
    public:
        EnergyARAP();
        ~EnergyARAP() = default;
        explicit EnergyARAP( const Settings *_s );

        double eval( const VectorXd &_TX, const MatrixXd &_rots, const VectorXd &_areas ) const;
        VectorXd grad( const VectorXd &_TX, const SparseMatrix<double> &_T,
                       const MatrixXd &_rots, const VectorXd &_areas,
                       const MatrixXi &_simplices ) const;
    };
}

#endif //INJECTIVEMAPPING_ENERGY_FUNC_H
