//
// Utility functions useful to solver implementation
// Created by adobe on 4/15/18.
//

#ifndef INJECTIVEMAPPING_COMPUTE_MAPS_H
#define INJECTIVEMAPPING_COMPUTE_MAPS_H

#include <Eigen/Eigen>
#include <igl/vertex_triangle_adjacency.h>
#include "utility.h"
#include "geom_utility.h"

namespace injmap
{
    using Eigen::VectorXd;
    using Eigen::VectorXi;
    using Eigen::MatrixXd;
    using Eigen::MatrixXi;
    using Eigen::Map;
    typedef Eigen::Array<bool, Eigen::Dynamic, 1> ArrayXb;
    typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
    using ::util::minimesh;
    typedef Eigen::SparseMatrix<double> SparseMatrixXd;

    enum SolverStatus
    {
        SUCCESS, FAILURE,
        CONVERGED,
        STUCKED, // cannot make any move
        CONTINUE, // can perform another iteration
        SKIPPED,
        LINFACTOR_SUC, LINFACTOR_FAIL, LINSOLVE_SUC, LINSOLVE_FAIL,
        LS_FAIL
    };
    enum IterMode
    {
        FULL_ITER, BY_STEP, BY_STEP_MIX, NON_STOP, STOP
    };

    /**
     * @Brief data shared by possibly multiple solvers during an entire session
     * serves as the solver-solver and solver-outside interface.
     */
    struct SolverData
    {
        const ::util::minimesh *rest_m;
        const ::util::minimesh *init_m; // rest and initial mesh
        ::util::minimesh *out_m; // output mesh (containing X-out)
        const VectorXi *hdls; // input handles
        MatrixXd Xnext; // a temp copy of X-out before actually changing X-out
        VectorXd flip; // reflects flipness of output mesh
        VectorXd pre_flip; // cache previous flipness

        /// other geometry related
        std::vector<std::vector<int>> V_F_adj; // nb faces for each v
        std::vector<std::vector<int>> V_i_in_adjF; // [i,j]: the index of vertex i within its j-th nb face
        SparseMatrixXd T, TT; // V to per-simplex-map operator
    };

    class SolverUtil
    {
    public:
        /// @Brief given faces which index into list of V, return boundary V ids
        static void getBoundaryVids( const MatrixXd &_V, const MatrixXi &_F, VectorXi &_bnd_ids );
        /// @Brief get non-boundary vts' ids into _non_bnd_ids
        static void getNonBoundaryVids( const MatrixXd &_V, const MatrixXi &_F, VectorXi &_non_bnd_ids );

        /// @Brief initialize solver data with given meshes and handles
        static void initSolverData( const minimesh &_rest_m, const minimesh &_init_m, const VectorXi &_hdls,
                                    minimesh &_out_m,
                                    SolverData &_data );

        /// @Brief enforce given constraints: X=c to linear system AX=b
        /// @pre c.cols() == b.cols()
        static void enforceConstraint( const VectorXi &_Xi, const MatrixXd &_C, SparseMatrixXd &_A, MatrixXd &_b );

        ///
        /// @Brief solve AX = b s.t. X[constr_ids] = C
        /// @return _X: values for un-constrained X will be in the place indexed by _free
        static SolverStatus solveWithConstr( const SparseMatrixXd &_A, const MatrixXd &_b,
                                             const VectorXi &_free, const VectorXi &_constr, const MatrixXd &_C,
                                             MatrixXd &_X );

        ///
        /// difference between two sets: {1...n} \ _X
        static VectorXi setMinus( int n, const VectorXi &_X );
        static void setMinus( int n, const VectorXi &_X, VectorXi &_Y );

        ///
        /// @Brief compute operator T: T*X = map for each simplex
        static void computeT( int d, int sd, const ::util::minimesh &_restmesh,
                              Eigen::SparseMatrix<double> &m_T, Eigen::SparseMatrix<double> &m_TT );

        ///
        /// @Brief determine flip-ness
        static void detFlips( int _d, int _m,
                              const Eigen::SparseMatrix<double> _T, const MatrixXd &_X,
                              double _thresh, VectorXd &_flip_flag );
        static void detFlips( int _d, int _m, const VectorXd &_TX, double _thresh, VectorXd &_flip_flag );

        /// @Brief return those nb faces for given vts where the condition[ni] = true for nb-face ni
        static void selectnbfacesIf( const Eigen::VectorXi &_V, const MatrixXi _F,
                                     const std::vector<std::vector<int>> _V_F_adj, const ArrayXb &_cond,
                                     Eigen::MatrixXi &_retF );

        /// @Brief return faces that are flipped (indicated by _flip > 0.)
        static MatrixXi getFlipFaces( const MatrixXi &_F, const VectorXd &_flip );

        /// @Brief return non-flipping faces (_flip = 0.)
        static MatrixXi getNonFlipFaces( const MatrixXi &_F, const VectorXd &_flip );

        /// @Brief obtain two index vector according whether (vector) mask[] is true or false
        static void findIds( ArrayXb _mask, VectorXi &_I1, VectorXi &_I2 );
    };
}

#endif //INJECTIVEMAPPING_COMPUTE_MAPS_H
