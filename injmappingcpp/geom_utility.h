//
// mainly defines a simple mesh structure
// Created by Yajie Yan on 8/23/17.
//

#ifndef INJECTIVEMAPPING_GEOM_UTILITY_H
#define INJECTIVEMAPPING_GEOM_UTILITY_H

#include <Eigen/Eigen>
#include <unordered_set>
#include <unordered_map>
#include "utility.h"

namespace util
{
    using namespace Eigen;

    struct Vec2Hash
    {
        template<typename T>
        std::size_t operator ()( T t ) const
        {
            return util::hash( t( 0 ), t( 1 ) );
        }
    };

    // a minimalistic mesh structure
    struct minimesh
    {
        typedef std::unordered_map<Vector2i, int, Vec2Hash> EdgeIDMap;

        /************** public interface **************/
        minimesh() : m_d( 0 )
        {}

        /**
         * Construct mini-mesh struct by copying from given data
         * @param _V
         * @param _F
         */
        minimesh( const MatrixXd _V, const MatrixXi _F ) : m_V( _V ), m_V3d( _V ), m_F( _F )
        {
            init();
        }

        minimesh &operator =( const minimesh &_other )
        {
            m_V = _other.V();
            m_V3d = _other.V3d();
            m_F = _other.F();
            m_E = _other.E();
            m_d = _other.d();
            m_eid_map = _other.getEdgeMap();
            m_e_len = _other.avgEdgeLen();
            return *this;
        }

        inline int d() const // dimension
        { return m_d; }

        inline int sd() const // # vts in a simplex
        { return d() + 1; }

        inline size_t n() const // # vts
        { return m_V.rows(); }

        inline size_t m() const // # simplices
        { return m_F.rows(); }

        inline const MatrixXd &V() const
        { return m_V; }

        inline const MatrixXd &V3d() const
        { return m_V3d; }

        inline void setV( const MatrixXd &_V )
        {
            int d = _V.cols();
            assert( d == 2 || d == 3 && "Only supports 2 or 3 dimension!" );
            m_V3d = _V;
            if ( d == 2 )
                minimesh::augmentVtsForRender( m_V3d );
            m_V = m_V3d.leftCols( m_d );
        }

        inline const MatrixXi &F() const
        { return m_F; }

        inline MatrixXi &F()
        { return m_F; }

        inline const MatrixXi &E() const
        { return m_E; }

        inline double avgEdgeLen() const
        { return m_e_len; }

        inline double scale() const
        { return m_bbox_l; }

        ///
        /// @return the index of edge in E matrix if found; -1 otherwise.
        /// \param _e
        inline int getEdgeID( const Vector2i &_e ) const
        {
            auto &&res = m_eid_map.find( _e );
            if ( res != m_eid_map.end() )
                return res->second;
            return -1;
        }

        ///
        /// return a const-ref to the edge-id map
        inline const EdgeIDMap &getEdgeMap() const
        { return m_eid_map; };

        ///
        /// collect edges from given simplices
        void mkEdgesFromSimplices( const MatrixXi &_F, MatrixXi &_E )
        {
            std::unordered_set<Eigen::Vector2i, Vec2Hash> E_set;
            int d = this->d();
            MatrixXi es( d == 2 ? d + 1 : 6, 2 );
            for ( auto t = 0; t < _F.rows(); ++t )
            {
                mkEdgesFromSimp( _F.row( t ), es );
                for ( auto i = 0; i < es.rows(); ++i )
                    E_set.insert( es.row( i ) );
            }

            if ( _E.rows() != E_set.size() || _E.cols() != 2 )
                _E.resize( E_set.size(), 2 );
            int i = 0;
            for ( auto &&e : E_set )
            {
                _E.row( i++ ) = e;
            }
        }

        /************** static functions **************/

        ///
        /// @Brief make an edge from two indices (pointing into V)
        static Eigen::Vector2i mkEdge( int _i, int _j )
        {
            return {std::min( _i, _j ), std::max( _i, _j )};
        }

        ///
        /// @Brief return a set of edges from given face
        /// \param [out]_es_of_f #edges in f by 2 matrix
        static void mkEdgesFromSimp( const Eigen::VectorXi &_f, MatrixXi &_es_of_f )
        {
            if ( _f.size() == 3 ) // triangle
            {
                _es_of_f.row( 0 ) = mkEdge( _f( 0 ), _f( 1 ) );
                _es_of_f.row( 1 ) = mkEdge( _f( 1 ), _f( 2 ) );
                _es_of_f.row( 2 ) = mkEdge( _f( 2 ), _f( 0 ) );
            }
            else // tet
            {
                _es_of_f.row( 0 ) = mkEdge( _f( 0 ), _f( 1 ) );
                _es_of_f.row( 1 ) = mkEdge( _f( 1 ), _f( 2 ) );
                _es_of_f.row( 2 ) = mkEdge( _f( 2 ), _f( 0 ) );
                _es_of_f.row( 3 ) = mkEdge( _f( 0 ), _f( 3 ) );
                _es_of_f.row( 4 ) = mkEdge( _f( 1 ), _f( 3 ) );
                _es_of_f.row( 5 ) = mkEdge( _f( 2 ), _f( 3 ) );
            }
        }

        ///
        /// \Brief Augment a list of 2d vertices to 3d (with z = 0). Often useful for rendering purpose.
        /// \param [in,out]_V
        static void augmentVtsForRender( MatrixXd &_V )
        {
            if ( _V.cols() == 3 )
                return;
            else if ( _V.cols() == 2 )
            {
                _V.conservativeResize( Eigen::NoChange_t::NoChange, 3 );
                _V.col( 2 ).setZero();
            }
        }

    protected:
        //

        void init()
        {
            // If all points have 0 at the 3rd coordinate, then the mesh is 2D. Otherwise 3D.
            m_d = 2; // by default 2-dim
            const auto &z = m_V.col( 2 );
            for ( auto i = 0; i < z.size(); ++i )
                if ( !util::equal<double>( z( i ), 0.0 ) )
                {
                    m_d = 3;
                    break;
                }
            // if dim is 2 then delete the z coordinate
            if ( m_d == 2 )
            {
                MatrixXd temp = m_V.block( 0, 0, m_V.rows(), 2 );
                m_V = temp;
            }

            // init edges
            minimesh::mkEdgesFromSimplices( m_F, m_E );

            // build edge map
            m_eid_map.clear();
            for ( auto i = 0; i < m_E.rows(); ++i )
            {
                m_eid_map[m_E.row( i )] = i;
            }

            // avg edge length
            m_e_len = 0.0;
            for ( auto i = 0; i < m_E.rows(); ++i )
            {
                auto e = m_E.row( i );
                m_e_len += (m_V.row( e( 0 ) ) - m_V.row( e( 1 ) )).norm();
            }
            m_e_len /= m_E.rows();

            // bounding box size
            m_bbox_l = (m_V.colwise().maxCoeff() - m_V.colwise().minCoeff()).maxCoeff();
        }

        /************** members **************/
        MatrixXd m_V3d; // of the same size as m_V. only here to save space allocation time.
        MatrixXd m_V;
        MatrixXi m_F;
        MatrixXi m_E; // edges (#edges by 2 matrix)
        double m_e_len; // average edge length
        double m_bbox_l; // bounding box size
        EdgeIDMap m_eid_map;
        int m_d; // effective dimension
    };
}

#endif //INJECTIVEMAPPING_GEOM_UTILITY_H
