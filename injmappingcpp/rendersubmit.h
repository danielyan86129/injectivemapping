//
// Utility functions used to interact with libigl's viewer
// Created by Yajie Yan on 4/18/18.
//

#ifndef INJECTIVEMAPPING_RENDERSUBMIT_H
#define INJECTIVEMAPPING_RENDERSUBMIT_H

#include <igl/opengl/glfw/Viewer.h>
#include <Eigen/Dense>

class RenderSubmit
{
    typedef Eigen::MatrixXd MatrixXd;
    typedef Eigen::VectorXd VectorXd;
    typedef Eigen::MatrixXi MatrixXi;

public:
    static void addLines( igl::opengl::glfw::Viewer &_viewer, const MatrixXd &_start, const MatrixXd &_dir,
                          const MatrixXd &_colors );

    static void setLines( igl::opengl::glfw::Viewer &_viewer, const MatrixXd &_start, const MatrixXd &_dir,
                          const MatrixXd &_colors );
};


#endif //INJECTIVEMAPPING_RENDERSUBMIT_H
