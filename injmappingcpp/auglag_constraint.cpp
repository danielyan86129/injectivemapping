//
// Created by Yajie Yan on 8/19/17.
//

#include "auglag_constraint.h"

namespace injmap
{
    typedef SettingEnum SE;

/*****************************
/Impl of class Detangler
******************************/

    Detangler::Detangler() : Constraint(IQL)
    {

    }

    Detangler::~Detangler()
    {

    }

    Detangler::Detangler(const Settings *_s) : Constraint(IQL)
    {
        this->setSettings(_s);
    }

    double Detangler::isViolated(double _lh) const
    {
        return std::abs(std::min(0.0, _lh));
    }

    double Detangler::evalForSimp(int _t, const VectorXd &_TX) const
    {
        return util::getMap(_TX, _t, (int) m_s->at(SE::DIM)).determinant();
    }

    VectorXd Detangler::gradForSimp(const SparseMatrix<double> &_T,
                                    int _t, const VectorXi &_simp_t, const MatrixXd &_Jt, double _det_Jt) const
    {
        int d = (int) m_s->at(SE::DIM);
        int n = (int) m_s->at(SE::N);
        int sd = (int) m_s->at(SE::SDIM);
        assert(_simp_t.size() == d + 1);

        VectorXd g_Dt = VectorXd::Zero(d * n);
        MatrixXd g_Jt = MatrixXd::Zero(d * d, d * sd);
        VectorXi pos = VectorXi::Zero(d * sd);
        EnergyFunction::gradMapT( _T, _t, _simp_t, g_Jt, pos );
        MatrixXd Jt_inv = util::pinv(_Jt);
        for (auto i = 0; i < pos.size(); ++i) {
            auto ii = pos(i);
            VectorXd p_Jt_p_ii = g_Jt.col(i);
            assert(d == 2 && p_Jt_p_ii.size() == 4 || d == 3 && p_Jt_p_ii.size() == 9);
            g_Dt(ii) = (Jt_inv * util::unvec(p_Jt_p_ii, d)).trace();
        }
        return _det_Jt * g_Dt;
    }

    void Detangler::accumulate_grad(const SparseMatrix<double> &_T, int _t, const VectorXi &_simp_t,
                                    const MatrixXd &_Jt, double _det_Jt, double _k, double _lmd,
                                    MatrixXd &_g_Jt, VectorXi &_pos,
                                    VectorXd &_g_D, double _w) const
    {
        int d = (int) m_s->at(SE::DIM);
        int n = (int) m_s->at(SE::N);
        int sd = (int) m_s->at(SE::SDIM);
        assert(_simp_t.size() == d + 1);

        _g_Jt.setZero();
        _pos.setZero();
        EnergyFunction::gradMapT( _T, _t, _simp_t, _g_Jt, _pos );
        MatrixXd Jt_inv = util::pinv(_Jt);
        for (auto i = 0; i < _pos.size(); ++i) {
            auto ii = _pos(i);
            VectorXd p_Jt_p_ii = _g_Jt.col(i);
            assert(d == 2 && p_Jt_p_ii.size() == 4 || d == 3 && p_Jt_p_ii.size() == 9);
            // accumulate
            _g_D(ii) += _w * (_k * _det_Jt - _lmd) *
                        _det_Jt * (Jt_inv * util::unvec(p_Jt_p_ii, d)).trace();
        }
    }

    double Detangler::eval(const VectorXd &_k, const VectorXd &_lmds,
                           const VectorXd &_TX, const VectorXd &_metric) const
    {
        int m = m_s->at(SE::M);
        assert(_metric.size() == m || _metric.size() == 1);
        double ghat = 0.0;
        for (auto i = 0; i < m; ++i) {
            double w = _metric.size() == 1 ? _metric(0) : _metric(i);
            auto gt = evalForSimp(i, _TX);
            if (gt <= _lmds(i) / _k(i))
                ghat += w * (0.5 * _k(i) * gt * gt - _lmds(i) * gt);
        }
        return ghat;
    }

    /*VectorXd
    Detangler::grad( const VectorXd &_k, const VectorXd &_lmds,
                     const SparseMatrix<double> &_T, const VectorXd &_TX, const MatrixXi &_simplices ) const
    {
        VectorXd g_D = VectorXd::Zero( (int) m_s->at( SE::DIM ) * (int) m_s->at( SE::N ) );
        for ( auto i = 0; i < m_s->at( SE::M ); ++i )
        {
            MatrixXd Jt = util::getMap( _TX, i, m_s->at( SE::DIM ) );
            auto det_Jt = Jt.determinant();
            VectorXd g_Dt = gradForSimp( _T, i, _simplices.row( i ), Jt, det_Jt );
            if ( det_Jt <= _lmds( i ) / _k( i ) )
                g_D += (_k( i ) * det_Jt - _lmds( i )) * g_Dt;
        }
        return g_D;
    }*/
    VectorXd
    Detangler::grad(const VectorXd &_k, const VectorXd &_lmds,
                    const SparseMatrix<double> &_T, const VectorXd &_TX,
                    const MatrixXi &_simplices, const VectorXd &_metric) const
    {
        int d = (int) m_s->at(SE::DIM);
        int n = (int) m_s->at(SE::N);
        int sd = (int) m_s->at(SE::SDIM);
        int m = m_s->at(SE::M);

        assert(_metric.size() == m || _metric.size() == 1);

        MatrixXd g_Jt = MatrixXd::Zero(d * d, d * sd);
        VectorXi pos = VectorXi::Zero(d * sd);
        VectorXd g_D = VectorXd::Zero(d * n);
        for (auto i = 0; i < m; ++i) {
            double w = _metric.size() == 1 ? _metric(0) : _metric(i);
            MatrixXd Jt = util::getMap(_TX, i, d);
            auto det_Jt = Jt.determinant();
            //VectorXd g_Dt = gradForSimp( _T, i, _simplices.row( i ), Jt, det_Jt );
            if (det_Jt <= _lmds(i) / _k(i))
                //g_D += (_k( i ) * det_Jt - _lmds( i )) * g_Dt;
                accumulate_grad(_T, i, _simplices.row(i), Jt, det_Jt, _k(i), _lmds(i), g_Jt, pos, g_D, w);
        }
        return g_D;
    }

/*****************************
/Impl of class BD
******************************/
    BD::BD()
    {

    }

    BD::~BD()
    {

    }

    BD::BD(const Settings *_s) : Constraint(IQL)
    {
        this->setSettings(_s);
    }

    double BD::evalForSimp(const util::svdstruct &_svd) const
    {
        const auto &S = _svd.S;
        return -S(0) + S(S.size() - 1) * m_s->at(BDBND);
    }

    VectorXd BD::gradForSimp(const SparseMatrix<double> &_T, const util::svdstruct &_svd,
                             int _t, const VectorXi &_simp_t) const
    {
        //cout << "U = \n"<<_svd.U<<endl;
        //cout << "V = \n"<<_svd.V<<endl;
        int d = m_s->at(SE::DIM);
        int n = m_s->at(SE::N);
        assert(_simp_t.size() == d + 1);
        VectorXd gbd = VectorXd::Zero(d * n);

        // singular value jacobian
        MatrixXd sv_1_jac = MatrixXd::Zero(d, d);
        MatrixXd sv_d_jac = MatrixXd::Zero(d, d);
        for (auto i = 0; i < d; ++i)
            for (auto j = 0; j < d; ++j) {
                sv_1_jac(i, j) = _svd.U(i, 0) * _svd.V(j, 0);
                sv_d_jac(i, j) = _svd.U(i, d - 1) * _svd.V(j, d - 1);
            }

        // grad-BD for this simplex
        int sd = m_s->at(SE::SDIM);
        MatrixXd gr_Jt = MatrixXd::Zero(d * d, d * sd); // grad-Jt entries
        VectorXi pos = VectorXi::Zero(gr_Jt.cols()); // which entries within gbd to fill with grad-Jt
        EnergyFunction::gradMapT( _T, _t, _simp_t, gr_Jt, pos );

        // fill grad-BD
        VectorXd diff_sv_jac = util::vec(sv_d_jac) - util::vec(sv_1_jac);
        //cout << "gr_Jt^T = \n" << gr_Jt.transpose() << endl;
        //cout << "sv_d_jac = \n" << sv_d_jac << endl;
        //cout << "sv_1_jac = \n" << sv_1_jac << endl;
        //cout << "sv_d_jac-sv_1_jac = " << RowVectorXd( diff_sv_jac ) << endl;
        VectorXd grd_tmp = m_s->at(SE::BDBND) * gr_Jt.transpose() * diff_sv_jac;
        //cout << "par-B-par-t = " << RowVectorXd( grd_tmp ) << endl;
        assert(grd_tmp.size() == pos.size());
        for (auto i = 0; i < pos.size(); ++i)
            gbd(pos(i)) = grd_tmp(i);

        return gbd;
    }

    double BD::eval(const VectorXd &_k, const VectorXd &_lmds, const std::vector<util::svdstruct> &_svds,
                    const VectorXd &_metric) const
    {
        int m = m_s->at(SE::M);
        assert(_metric.size() == m || _metric.size() == 1);
        double val_BD = 0.0;
        for (auto i = 0; i < m; ++i) {
            double w = _metric.size() == 1 ? _metric(0) : _metric(i);
            auto bd = evalForSimp(_svds[i]);
            if (bd <= _lmds(i) / _k(i))
                val_BD += w * (0.5 * _k(i) * bd * bd - _lmds(i) * bd);
        }
        return val_BD;
    }

    VectorXd BD::grad(const std::vector<util::svdstruct> &_svds,
                      const VectorXd &_k, const VectorXd &_lmds,
                      const SparseMatrix<double> &_T,
                      const MatrixXi &_simplices, const VectorXd &_metric) const
    {
        int d = (int) m_s->at(SE::DIM);
        int n = (int) m_s->at(SE::N);
        int m = m_s->at(SE::M);
        assert(_metric.size() == m || _metric.size() == 1);

        VectorXd g_BD = VectorXd::Zero(d * n);
        double bd;
        auto gbd = VectorXd(d * n);
        for (auto t = 0; t < m; ++t) {
            double w = _metric.size() == 1 ? _metric(0) : _metric(t);
            auto bd = evalForSimp(_svds[t]);
            gbd = gradForSimp(_T, _svds[t], t, _simplices.row(t));
            if (bd <= _lmds(t) / _k(t))
                g_BD += w * (_k(t) * bd - _lmds(t)) * gbd;
        }

        return g_BD;
    }

    double BD::isViolated(double _lh) const
    {
        return std::abs(std::min(0.0, _lh));
    }
}