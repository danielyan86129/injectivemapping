#include <iostream>
#include <vector>
#include <iostream>
#include <igl/slice.h>
#include <igl/readOBJ.h>
#include <igl/readDMAT.h>
#include <igl/per_corner_normals.h>
#include "logging.h"
#include "AugLagSolver.h"
#include "import.h"

using namespace Eigen;
using logging::Logger;
using ::util::minimesh;
using std::string;
using std::cout; using std::endl;

typedef injmap::SettingEnum SE;

// 2d tirangle mesh files
string data_dir = "../../data/export/synth/";
string restmesh_file = data_dir + "synth.obj";
string initmesh_file = data_dir + "synth_init.obj";
string hdl_file = data_dir + "synth_hdls.dmat";
// tet mesh files
string data_3d_dir = "../../data/export/eg3/";
string tet_base = data_dir + "eg3";
// folder containing side-product files
string out_folder = data_dir;
// optional setting file
string setting_file = "";
// program flags
enum Mode
{
    TRI = 0, TET = 1, INVALID = 2
};
Mode mode = TRI;
bool no_gui = false;
bool interactive = true;

injmap::iterinout *cur_iter_data = nullptr;
injmap::AugLagSolver *aug_solver = nullptr;

void usage();

bool parse_args( int _argc, char **_argv );

bool preDraw_callback( igl::opengl::glfw::Viewer &_viewer );

bool force_iter = false; // force solver to perform another iteration
bool keyDown( igl::opengl::glfw::Viewer &_viewer, unsigned char _key, int _modifier );

int main( int _argc, char **_argv )
{
    // parse args
    if ( !parse_args( _argc, _argv ) )
    {
        usage();
        return -1;
    }

    /************** read in dataset from files **************/
    // mesh data to fill-in
    minimesh rest_m, init_m;
    Eigen::MatrixXi hdls;
    MatrixXi BF; // boundary faces; specific to tet
    /// TODO:
    /// - wrap the following to a "make" function
    /// - feed filename to the solver as well (it maybe useful internal to the solver)
    // read in meshes
    if ( mode == TRI )
    {
        out_folder = data_dir;
        VectorXi hdl_ids;
        if ( !util::Importer::importMeshData( restmesh_file, initmesh_file, hdl_file,
                                              rest_m, init_m, hdl_ids ) )
            return -1;
        hdls = hdl_ids;
    }
    else if ( mode == TET )
    {
        out_folder = tet_base.substr( 0, tet_base.find_last_of( '/' ) + 1 );
        if ( util::Importer::importTetData( tet_base, rest_m, init_m, BF, hdls ) )
            Logger::info( "Tet dataset read in." );
        else
        {
            Logger::fatal( "Cannot read in all required tet mesh files." );
            return -1;
        }
    }
    else
    {
        Logger::fatal( "Logic error. Should never happen." );
    }


    /************** setup solver by specifying different settings **************/
    if ( setting_file != "" )
    {
        Logger::info( "Constructing solver from setting file: " + setting_file );
        aug_solver = new injmap::AugLagSolver( injmap::readSettings( setting_file ) );
    }
    else
    {
        // D+LSQ
        //aug_solver = new injmap::AugLagSolver( injmap::defaultSettings() );
        // D+ARAP
//        auto options = injmap::Settings( {{SE::W_D,    0},
//                                          {SE::W_ARAP, 1},
//                                          {SE::W_LSQ,  0}} );
        // use BD as only constraint
        auto options = injmap::Settings( {{SE::W_B,      0},
                                          {SE::W_D,      1},
                                          {SE::W_LSQ,    0.5},
                                          {SE::W_ARAP,   1.0},
                                          {SE::AARAP,    injmap::AArapVal::YES},
                                          {SE::AREA_WTD, injmap::AreaWtdVal::YES},
                                          {SE::BDBND,    5}} );
        aug_solver = new injmap::AugLagSolver( injmap::defaultSettings( options ) );
    }
    auto init_suc = aug_solver->initialize( rest_m, init_m, hdls );
    aug_solver->setFolder( out_folder );
    if ( init_suc )
    {
        Logger::info( "Solver initialized." );
    }
    else
    {
        delete aug_solver;
        Logger::fatal( "Cannot initialize solver." );
        return -1;
    }

    // setup viewer
    auto gold = RowVector3d( 255.0 / 255.0, 228.0 / 255.0, 58.0 / 255.0 );
    auto green = RowVector3d( 0.0, 0.7, 0.0 );
    auto gray = RowVector3d( 0.5, 0.5, 0.5 );
    auto black = RowVector4f( 0, 0, 0, 0 );
    RowVector3d mesh_c( 1, 1, 1 );

    igl::opengl::glfw::Viewer static_viewer;
    if ( mode == TRI )
    {
        static_viewer.data().set_mesh( rest_m.V3d(), rest_m.F() );
    }
    else
    {
        static_viewer.data().set_mesh( rest_m.V3d(), BF );
    }
    static_viewer.data().set_colors( mesh_c );
    static_viewer.data().add_points( igl::slice( rest_m.V3d(), hdls, 1 ), green );
//    static_viewer.data.add_points( igl::slice( init_m.V3d(), hdls, 1 ), green );
    static_viewer.core.background_color = black;
    static_viewer.core.lighting_factor = 0.0f;
    static_viewer.data().point_size = 15.0f;

    igl::opengl::glfw::Viewer iter_viewer;
    MatrixXd normals;
    if ( mode == TRI )
    {
        iter_viewer.data().set_mesh( init_m.V3d(), init_m.F() );
//        igl::per_corner_normals( init_m.V3d(), init_m.F(), 30, normals );
    }
    else
    {
        iter_viewer.data().set_mesh( init_m.V3d(), BF );
//        igl::per_corner_normals( init_m.V3d(), BF, 30, normals );
    }
    iter_viewer.data().set_colors( mesh_c );
    iter_viewer.data().set_normals( normals );
    // add handle points
    iter_viewer.data().add_points( igl::slice( init_m.V3d(), hdls, 1 ), green );
    // add edges colored by solver
    MatrixXd E_color;
    aug_solver->detEdgeColor( E_color );
    iter_viewer.data().set_edges( init_m.V3d(), init_m.E(), E_color );

    // adjust camera
    MatrixXd cam_mat;
    float zoom;
    Vector3f shift;
    iter_viewer.core.get_scale_and_shift_to_fit_mesh( 5.0 * init_m.V3d(), zoom, shift );
    iter_viewer.core.align_camera_center( 5.0 * init_m.V3d(), init_m.F() );
    iter_viewer.core.camera_dfar = iter_viewer.core.camera_dfar * 10.0f;

    // rendering params
    iter_viewer.core.background_color = black;
    iter_viewer.core.lighting_factor = 0.0f;
    iter_viewer.data().line_width = 10.0;
    iter_viewer.data().show_lines = false;
    iter_viewer.data().point_size = 15.0f;
    iter_viewer.core.is_animating = false;
    iter_viewer.core.animation_max_fps = 30;

    iter_viewer.callback_pre_draw = &preDraw_callback;
    iter_viewer.callback_key_down = &keyDown;
    Logger::info( "press [space] to force one iteration." );

    if ( !interactive )
    {
        aug_solver->run();
        MatrixXd res_V;
        aug_solver->getResultForRender( res_V );
        iter_viewer.data().set_vertices( res_V );
    }
    // launch viewer
//    static_viewer.launch();
    iter_viewer.launch();

    // after closing viewer, clean-up the solver
    if ( !aug_solver->finish() )
    {
        Logger::err( "Cannot end solver properly!" );
    }
    delete aug_solver;
    return 0;
}

void usage()
{
    Logger::info( string()
                  + "Compute injective maps for the given (possibly inverted) initial maps.\n"
                  + "usage: \n"
                  + "progname <mode> meshfiles\n\n"
                  + "mode: 2d/3d\n"
                  + "meshfiles: \n"
                  + "\trestmesh-file initmesh-file hdl-file [setting-file] (if mode is 2d)\n"
                  + "\ttet-file-base-name (if mode is 3d)\n"
    );
}

bool parse_args( int _argc, char **_argv )
{
    // track #left args as we churn them
    int argc_left = _argc;
    int arg_i = 0;

    // start processing
    if ( argc_left == 1 )
    {
        Logger::info( "No user-specified dataset. Using default." );
        cout << "rest mesh: " << restmesh_file << endl;
        cout << "init mesh: " << initmesh_file << endl;
        cout << "handles: " << hdl_file << endl;
        return true;
    }
    argc_left--;
    arg_i++;

    string mode_str = _argv[arg_i];
    mode = (mode_str == "2d" ? TRI : mode_str == "3d" ? TET : INVALID);
    if ( mode == INVALID )
    {
        Logger::err( "invalid mode specified -> " + mode_str );
        return false;
    }
    argc_left--;
    arg_i++;
    if ( !argc_left )
        return false;

    if ( mode == TRI )
    {
        if ( argc_left < 3 )
            return false;
        restmesh_file = _argv[arg_i];
        initmesh_file = _argv[arg_i + 1];
        hdl_file = _argv[arg_i + 2];
        argc_left -= 3;
        arg_i += 3;
    }
    else // mode = TET
    {
        assert( argc_left > 0 );
        tet_base = _argv[arg_i];
        argc_left--;
        arg_i++;
    }
    if ( argc_left >= 1 ) // last arg is a setting file
    {
        setting_file = _argv[arg_i];
        argc_left--;
        arg_i++;
    }
    assert( argc_left == 0 && arg_i == _argc );
    return true;
}

bool preDraw_callback( igl::opengl::glfw::Viewer &_viewer )
{
    typedef injmap::AugLagSolver::SolverStatus status;
    if ( force_iter
         || (_viewer.core.is_animating
             && aug_solver->solverStatus() == status::CAN_CONTINUE) )
    {
        aug_solver->iterateWithVis( _viewer );
        Logger::info( "After iteration: " );
        cout << "status: " << aug_solver->solverStatus() << endl;
    }
    if ( aug_solver->solverStatus() != status::CAN_CONTINUE )
        _viewer.core.is_animating = false;
    // make sure each time only 1 iter forced
    if ( force_iter ) force_iter = false;

    return false;
}

bool keyDown( igl::opengl::glfw::Viewer &_viewer, unsigned char _key, int _modifier )
{
    if ( _key == ' ' )
        force_iter = true;
    return false; // false means do not cancel
}