//
// Created by Yajie Yan on 8/31/17.
//

#ifndef INJECTIVEMAPPING_DEBUG_H
#define INJECTIVEMAPPING_DEBUG_H

#include <vector>
#include <string>

namespace injmap
{
    namespace dbg
    {
        using std::vector;
        using std::string;
        // debug dump info of many iterations
        struct iterinfodbg
        {
            vector<double> F; // total energy
            vector<double> D;
            vector<double> BD;
            vector<double> lsq;
            vector<double> arap;
            vector<double> pmax; // max{ norm (p at a simplex) }
            vector<double> step; // step length
            vector<double> pstep; // step * pmax
            vector<double> flipness; // \sum flipness_t
            vector<double> nflips; // total # flips

            bool exportToFiles(const string& _base) const;
            void clear();
        };
    }
}

#endif //INJECTIVEMAPPING_DEBUG_H
