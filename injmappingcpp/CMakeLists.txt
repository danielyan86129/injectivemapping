cmake_minimum_required(VERSION 3.7)
project(InjectiveMapping)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

############## Eigen ###############
# Eigen options
set(EIGEN_INC_DIR ../libs/eigen/)
IF (NOT EIGEN_INC_DIR)
    message(FATAL_ERROR "Please point the environment variable
    EIGEN_INC_DIR to the include directory of your Eigen installation.")
ENDIF ()
include_directories("${EIGEN_INC_DIR}")

############## libigl ###############
# Compilation flags: adapt to your needs
if (MSVC)
    # Enable parallel compilation
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP /bigobj")
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_BINARY_DIR})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_BINARY_DIR})
else ()
    # Libigl requires a modern C++ compiler that supports at least c++11
    #    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    set(CMAKE_CXX_STANDARD 17)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_SOURCE_DIR}/bin)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_SOURCE_DIR}/bin)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_SOURCE_DIR}/bin)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-deprecated-declarations")
    set(CMAKE_VERBOSE_MAKEFILE OFF)
endif ()

# libigl options: choose between header only and compiled static library
# Header-only is preferred for small projects. For larger projects the static build
# considerably reduces the compilation times
option(LIBIGL_USE_STATIC_LIBRARY "Use LibIGL as static library" OFF)

# libigl options: choose your dependencies (by default everything is OFF except opengl)
option(LIBIGL_USE_STATIC_LIBRARY "Use libigl as static library" OFF)
option(LIBIGL_WITH_ANTTWEAKBAR       "Use AntTweakBar"    OFF)
option(LIBIGL_WITH_CGAL              "Use CGAL"           OFF)
option(LIBIGL_WITH_COMISO            "Use CoMiso"         OFF)
option(LIBIGL_WITH_CORK              "Use Cork"           OFF)
option(LIBIGL_WITH_EMBREE            "Use Embree"         OFF)
option(LIBIGL_WITH_LIM               "Use LIM"            OFF)
option(LIBIGL_WITH_MATLAB            "Use Matlab"         OFF)
option(LIBIGL_WITH_MOSEK             "Use MOSEK"          OFF)
option(LIBIGL_WITH_OPENGL            "Use OpenGL"         ON)
option(LIBIGL_WITH_OPENGL_GLFW       "Use GLFW"           ON)
option(LIBIGL_WITH_OPENGL_GLFW_IMGUI "Use ImGui"          ON)
option(LIBIGL_WITH_PNG               "Use PNG"            OFF)
option(LIBIGL_WITH_PYTHON            "Use Python"         OFF)
option(LIBIGL_WITH_TETGEN            "Use Tetgen"         OFF)
option(LIBIGL_WITH_TRIANGLE          "Use Triangle"       OFF)
option(LIBIGL_WITH_VIEWER            "Use OpenGL viewer"  OFF)
option(LIBIGL_WITH_XML               "Use XML"            OFF)
if (LIBIGL_WITH_CGAL) # Do not remove or move this block, the cgal build system fails without it
    find_package(CGAL REQUIRED COMPONENTS Core)
    set(CGAL_DONT_OVERRIDE_CMAKE_FLAGS TRUE CACHE BOOL "CGAL's CMAKE Setup is super annoying ")
    include(${CGAL_USE_FILE})
endif ()

# Adding libigl: choose the path to your local copy libigl
# This is going to compile everything you requested
#message(FATAL_ERROR "${PROJECT_SOURCE_DIR}/../libigl/cmake")
#set(LIBIGL_INCLUDE_DIRS ../libs/libigl/include)
#add_subdirectory("${LIBIGL_INCLUDE_DIR}/../shared/cmake" "libigl")

# find libigl
set(ENV{LIBIGL_DIR} "../libs/libigl")

find_package(LIBIGL REQUIRED)

if (NOT LIBIGL_FOUND)
    message(FATAL_ERROR "libigl not found --- You can download it using: \n git clone --recursive
    https://github.com/libigl/libigl.git ${PROJECT_SOURCE_DIR}/../libs/libigl")
endif ()

# libigl information
message("libigl includes: ${LIBIGL_INCLUDE_DIR}")
message("libigl libraries: ${LIBIGL_LIBRARIES}")
message("libigl extra sources: ${LIBIGL_EXTRA_SOURCES}")
message("libigl extra libraries: ${LIBIGL_EXTRA_LIBRARIES}")
message("libigl definitions: ${LIBIGL_DEFINITIONS}")

# Prepare the build environment
include_directories(${LIBIGL_INCLUDE_DIR})
add_definitions(${LIBIGL_DEFINITIONS})

# linker flags
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl")

# grab all sdk-source files
file(GLOB sdkfiles *.cpp *.h)
# but exclude those cpp that implement templates, inlines, etc.
list(FILTER sdkfiles EXCLUDE REGEX ".*_imp.cpp$")

# set cur dir as include path
include_directories(${PROJECT_SOURCE_DIR})

# set files for aug-lag project
set(auglag_src ${sdkfiles} auglag/main_auglag.cpp)
add_executable(auglag_exe ${auglag_src} ${LIBIGL_EXTRA_SOURCES} debug.h debug.cpp)
target_link_libraries(auglag_exe ${LIBIGL_LIBRARIES} ${LIBIGL_EXTRA_LIBRARIES} igl::core igl::opengl_glfw)

# set files for iter-inj project
set(iterinj_src ${sdkfiles} iterinj/main_iterinj.cpp)
add_executable(iterinj_exe ${iterinj_src} ${LIBIGL_EXTRA_SOURCES})
target_link_libraries(iterinj_exe ${LIBIGL_LIBRARIES} ${LIBIGL_EXTRA_LIBRARIES} igl::core igl::opengl_glfw)

# compile flags for our executables
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
if (CMAKE_BUILD_TYPE MATCHES Debug)
    target_compile_definitions(auglag_exe PUBLIC DEBUG=1)
    target_compile_definitions(iterinj_exe PUBLIC DEBUG=1)
elseif (CMAKE_BUILD_TYPE MATCHES Release)
    target_compile_definitions(auglag_exe PUBLIC DEBUG=0)
    target_compile_definitions(iterinj_exe PUBLIC DEBUG=0)
endif ()