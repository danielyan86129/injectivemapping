//
// The solver that flows vertices whose neighborhood is non-flipping.
// Created by Yajie Yan on 4/25/18.
//

#ifndef INJECTIVEMAPPING_FLOWSOLVER_H
#define INJECTIVEMAPPING_FLOWSOLVER_H

#include "SolverBase.h"

namespace injmap
{
    class FlowSolver : public SolverBase
    {
    public:
        /**
         * public types
         */
        typedef SolverBase Base;
    public:
        /// ctors & de-ctros
        FlowSolver();
        ~FlowSolver();
        /// init solver
        void initSolver( SolverData *_data );
        /// set / get iteration mode
        virtual void setIterMode( IterMode _itermode );
        virtual IterMode getIterMode() const;

        /// assign viewer
        virtual void setViewer( igl::opengl::glfw::Viewer &_viewer );

        /// init viewer by uploading geometry
        virtual void initViewer();

        /// determine state of solver
        SolverStatus state() const;
        /// run one iteration of solver. output: new positions X_out
        virtual SolverStatus iterate( int _iter );
        /// compute the gradient (flow) at each vertex
        SolverStatus computeStep( int _iter );
        /// given per vert direction (flow), return the step of the flow that flips no face
        double nonFlipFlow( const MatrixXd &_X, const MatrixXi &_F, const MatrixXd &_dir,
                            const VectorXd &flip );
        /// take the step (post next V to the output V)
        SolverStatus takeStep( int _iter );

    protected:
        /**
         * helpers
         */
        void init_helper();
    protected:
        /**
         * flow-specific data
         */
        SparseMatrixXd m_L; // cot-laplacian
        SparseMatrixXd m_M; // mass matrix
    };
}

#endif //INJECTIVEMAPPING_FLOWSOLVER_H
