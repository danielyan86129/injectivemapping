//
// The solver that performs flow & relaxation iteratively.
// Created by Yajie Yan on 4/15/18.
//

#ifndef INJECTIVEMAPPING_ITERINJSOLVER_H
#define INJECTIVEMAPPING_ITERINJSOLVER_H

#include <vector>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/slim.h>
#include "utility.h"
#include "geom_utility.h"
#include "opt_utility.h"
#include "SolverUtil.h"
#include "SolverBase.h"

namespace injmap
{
    using namespace injmap;
    using ::util::minimesh;
    using std::vector;
    using injmap::ArrayXb;

    class IterInjSolver : public SolverBase
    {
    public:
        typedef SolverBase Base;
        enum SolverStep
        {
            COMPUTE_FLOW, TAKE_FLOW, COMPUTE_RELAX, TAKE_RELAX
        };

    public:
        IterInjSolver();

        ~IterInjSolver();

        /*
         * Major interfaces
         */
        /// init solver
        void initSolver( SolverData *_data );

        /// set flow times
        void setFlowN( int _n_flow );

        /// assign viewer
        void setViewer( igl::opengl::glfw::Viewer &_viewer );

        /// init viewer by uploading geometry
        void initViewer();

        /// @Brief return state of the solver
        SolverStatus state() const;

        /// @Brief set running mode of solver
        void setIterMode( IterMode _itermode );

        /// run one iteration of solver. output: new positions X_out
        SolverStatus iterate( int _iter );

        SolverStatus computeStep( int _iter );

        /// take the step (post next V to the output V)
        SolverStatus takeStep( int _iter );

        /// @Brief return per-vertex gradient vector
        void getPerVertStep( MatrixXd &_dir ) const;

    protected:
        /**
         * Member data
         */
        /// two solvers
        SolverBase *m_flow_solver;
        SolverBase *m_inj_solver;
        SolverStep m_cur_step;
        int m_flow_n; // max flows to perform
        int m_flow_cnt; // # flows performed so far
        int m_relax_n; // max relaxes to perform
        int m_relax_cnt; // # relaxes performed so far
        std::vector<MatrixXd> aux_dirs; // auxiliary directions (to display)
        /// the opt data
        igl::SLIMData m_slim_data; // optimization data for slim solver

        /**
         * helper functions
         */

        /// compute the flow and move the current V -> next V along the flow
        SolverStatus computeFlowStep( int _iter );

        /// compute the relaxation and update next V
        SolverStatus computeRelaxStep( int _iter );

        /// @Brief take the step produced by each solver
        SolverStatus takeFlow( int _iter );
        SolverStatus takeRelax( int _iter );

        /// @Brief compute a mix step
        SolverStatus computeMixStep( int _iter );
        SolverStatus takeMix( int _iter );

        /// @Brief determine whether to do another flow / relax step
        bool can_flow_more() const;
        bool can_relax_more() const;

        /// @Brief an iteration running one step
        SolverStatus run_one_step( IterMode _mode, int _iter );

        /**
         * rendering related helpers
         */
        /// determine color for each edge with output vts
        void getEdgesAndColorsOfFlipped( const MatrixXd &_V, const MatrixXi &_F,
                                         const VectorXd &_flips,
                                         MatrixXi &_E, MatrixXd &_color ) const;

        void getEdgesAndColorsOfFlipped( const MatrixXd &_V, const MatrixXi &_F,
                                         const VectorXd &_flips,
                                         MatrixXd &_P1, MatrixXd &_P2, MatrixXd &_color ) const;

    };
}

#endif //INJECTIVEMAPPING_ITERINJSOLVER_H
