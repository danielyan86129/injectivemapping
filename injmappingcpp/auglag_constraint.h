//
// Created by Yajie Yan on 8/19/17.
// Defines a set of constraints that can be used by an augmented lagrangian solver
//

#ifndef INJECTIVEMAPPING_AUGLAG_CONSTRAINT_H
#define INJECTIVEMAPPING_AUGLAG_CONSTRAINT_H

#include "energy_func.h"

namespace injmap
{
    /************** Base class for a aug-lag constraint **************/
    // Constraint type enum
    enum ConstraintType
    {
        EQL, IQL
    };

    class Constraint : public EnergyFunction
    {
    public:
        Constraint() : m_ct(ConstraintType::EQL)
        {}

        virtual ~Constraint() override
        {};

        //
        // is constraint violated?
        // return by how much it is violated
        virtual double isViolated(double) const = 0;

    protected:
        explicit Constraint(ConstraintType _ct) : m_ct(_ct)
        {}

        // type of this constraint. Initialized by subclass constructors.
        const ConstraintType m_ct;
    };

    /************** Detangler (Non-flipping constraint) **************/
    class Detangler : public Constraint
    {
    public:
        Detangler();

        ~Detangler() override;

        explicit Detangler(const Settings *_s);

        //
        // evaluate the constraint
        double evalForSimp(int _t, const VectorXd &_TX) const;

        ///
        /// \Brief evaluate G-hat, the part of objective corresponding to constraints D
        /// \param [in] _metric: vector of m entries for per-simplex weighting or only 1 entry for constant weighting
        double eval(const VectorXd &_k, const VectorXd &_lmds, const VectorXd &_TX, const VectorXd &_metric) const;

        //
        // compute gradient of the constraint
        VectorXd gradForSimp(const SparseMatrix<double> &_T,
                             int _t, const VectorXi &_simp_t, const MatrixXd &_Jt, double _det_Jt) const;

        ///
        /// \Brief compute gradient G for the part of objective corresponding to constraints D
        /// \param [in] _metric: vector of m entries for per-simplex weighting or only 1 entry for constant weighting
        VectorXd grad(const VectorXd &_k, const VectorXd &_lmds,
                      const SparseMatrix<double> &_T, const VectorXd &_TX,
                      const MatrixXi &_simplices, const VectorXd &_metric) const;

        //
        // given the left-hand value, is this constraint violated?
        // return |lh| if violated, 0 otherwise
        double isViolated(double _lh) const override;

    protected:
        //
        /// @Brief optimized for accumulating grad-D for a simplex t into the global gradient
        void accumulate_grad(const SparseMatrix<double> &_T, int _t, const VectorXi &_simp_t,
                             const MatrixXd &_Jt, double _det_Jt, double _k, double _lmd,
                             MatrixXd &_g_Jt, VectorXi &_pos,
                             VectorXd &_g_D, double _w = 1.0) const;
        //
        // TODO(maybe): add coefficients of the constraint in augmented lagrangian formulation
    };

    /************** Bounded distortion (BD) constraint **************/
    class BD : Constraint
    {
    public:
        BD();

        ~BD() override;

        explicit BD(const Settings *_s);

        //
        // evaluate the constraint
        double evalForSimp(const util::svdstruct &_svd) const;

        ///
        /// \Brief evaluate G-hat, the part of objective corresponding to constraints BD
        double eval(const VectorXd &_k, const VectorXd &_lmds,
                    const std::vector<util::svdstruct> &_svds, const VectorXd &_metric) const;

        //
        // compute gradient of the constraint for simplex t
        VectorXd gradForSimp(const SparseMatrix<double> &_T, const util::svdstruct &_svd,
                             int _t, const VectorXi &_simp_t) const;

        ///
        /// \Brief compute gradient G for the part of objective corresponding to constraints BD
        VectorXd grad(const std::vector<util::svdstruct> &_svds,
                      const VectorXd &_k, const VectorXd &_lmds,
                      const SparseMatrix<double> &_T,
                      const MatrixXi &_simplices, const VectorXd &_metric) const;

        //
        // given the left-hand value, is this constraint violated?
        // return |lh| if violated, 0 otherwise
        double isViolated(double _lh) const override;
    };
}

#endif //INJECTIVEMAPPING_AUGLAG_CONSTRAINT_H
