//
// The solver that flows vertices whose neighborhood is non-flipping.
// Created by Yajie Yan on 4/25/18.
//

#include <igl/slice_mask.h>
#include <igl/unique_rows.h>
#include <igl/slice_into.h>
#include <igl/flip_avoiding_line_search.h>
#include "flowsolver.h"
#include "my_cotmatrix.h"

namespace injmap
{

    FlowSolver::FlowSolver()
    {

    }

    FlowSolver::~FlowSolver()
    {

    }

    void
    FlowSolver::initSolver( SolverData *_data )
    {
        SolverBase::initSolver( _data );
        init_helper();
    }

    void FlowSolver::setIterMode( IterMode _itermode )
    {
        SolverBase::setIterMode( _itermode );
    }

    IterMode FlowSolver::getIterMode() const
    {
        return SolverBase::getIterMode();
    }

    void FlowSolver::setViewer( igl::opengl::glfw::Viewer &_viewer )
    {
        SolverBase::setViewer( _viewer );
    }

    void FlowSolver::initViewer()
    {

    }

    SolverStatus FlowSolver::state() const
    {
        double smallnum = m_data->init_m->scale() * 1e-4;
        if ( Base::m_step.norm() < smallnum )
            return CONVERGED;
        return CONTINUE;
    }

    SolverStatus FlowSolver::iterate( int _iter )
    {
        computeStep( _iter );
        takeStep( _iter );

        return SolverStatus::SUCCESS;
    }

    SolverStatus FlowSolver::computeStep( int _iter )
    {
        // initialize information
        int d = Base::m_data->init_m->F().cols() - 1;
        int n = Base::m_data->init_m->n();
        int m = Base::m_data->init_m->F().rows();
        const auto &V_out = Base::m_data->out_m->V3d();
        const MatrixXd &V_prev = V_out; // by default prev & next are all prev output
        m_data->Xnext = V_prev;
        m_step.setZero();// by default reset flow

        if ( !(Base::m_data->flip.array() > 0.).any() ) // no bad faces left, then no need to flow
        {
            printf( "flow step: skipped due to 0 bad faces\n" );
            return SKIPPED;
        }

        // update cot-laplacian
        igl::my_cotmatrix( V_prev, Base::m_data->out_m->F(), true, m_L );
        bool L_has_nan = std::isnan( m_L.norm() ); // check if cot-lap still has NaN
        // update mass matrix
        igl::massmatrix( V_prev, Base::m_data->out_m->F(), igl::MassMatrixType::MASSMATRIX_TYPE_BARYCENTRIC, m_M );
        printf( "Done: update cot-laplacian & mass matrix\n" );

        /* solve for the next X: (M-step*L)X_o = M*X_in */
        // form b
        MatrixXd b = m_M * V_prev;
        // form A
        Eigen::SparseMatrix<double> A;
        A = m_M - m_L;
        // indices for free & fixed vars

        // Call linear solver
        VectorXi free_ids = SolverUtil::setMinus( n, *m_data->hdls );
        auto err = SolverUtil::solveWithConstr( A, b, free_ids, *m_data->hdls, m_data->init_m->V3d()/*V_prev*/,
                                                m_data->Xnext );
        if ( err != SUCCESS )
            return err;
        printf( "Done: solving with constraints.\n" );

        // flow computed (TODO: figure out why non-crease vts still have non-zero flow)
        MatrixXd flow_d = m_data->Xnext - V_prev;
        printf( "|L|,L-has-nan?,|A|,|b|,|X-out|,|flow| = %f,%s,%f,%f,%f,%f\n",
                m_L.norm(), L_has_nan ? "true" : "false", A.norm(), b.norm(), m_data->Xnext.norm(), flow_d.norm() );

        /*** figure out steps for good and bad faces resp ***/
        // locate good and bad faces
        ArrayXb F_good_flag = (Base::m_data->flip.array() == 0.).eval();
        ArrayXb F_bad_flag = (Base::m_data->flip.array() > 0.).eval();
        MatrixXi good_F = igl::slice_mask( Base::m_data->init_m->F(), F_good_flag, 1 );
        MatrixXi bad_F = igl::slice_mask( Base::m_data->init_m->F(), F_bad_flag, 1 );
        VectorXi bad_Vi, tmp1, tmp2;
        igl::unique_rows( util::vec( bad_F ), bad_Vi, tmp1, tmp2 );
        // find faces that share bad-V (V of bad faces), as they will be affected when bad-V are flowed
        MatrixXi F_might_flip;
        SolverUtil::selectnbfacesIf( bad_Vi, Base::m_data->init_m->F(), m_data->V_F_adj, F_good_flag, F_might_flip );
        // init step with flows (masking out flows other than those for the bad-V)
        Base::m_step = MatrixXd::Zero( flow_d.rows(), flow_d.cols() );
        igl::slice_into( igl::slice( flow_d, bad_Vi, 1 ), bad_Vi, 1, m_step );
        // find the step
        double good_s = igl::flip_avoiding::compute_max_step_from_singularities( V_prev.leftCols( d ), bad_F,
                                                                                 m_step );
        // good_s = std::min( 1., good_s ) * 1.001;
        good_s *= 1.1;
        double bad_s = igl::flip_avoiding::compute_max_step_from_singularities( V_prev.leftCols( d ), F_might_flip,
                                                                                m_step );
        // bad_s = std::min( 1., bad_s ) * 0.999;
        bad_s *= 0.8;
        printf( "good-step,bad-m_step = %f,%f \n", good_s, bad_s );

        /*** take the right step by only updating bad-V ***/
        m_data->Xnext = V_prev;
        // V(ids) = V(ids) + flow_d(ids) * the constraining step
        auto final_s = std::min( good_s, bad_s );
        m_step *= final_s;
        igl::slice_into(
                igl::slice( V_prev, bad_Vi, 1 ) + igl::slice( m_step, bad_Vi, 1 ).eval(),
                bad_Vi, 1, m_data->Xnext );
        printf( "|step| = %f \n", Base::m_step.norm() );
        // m_V_next = V_prev + step;}

        /*** set step to the original flow (debug) ***/
        //m_step = (final_s * flow_d).eval();
        return SolverStatus::SUCCESS;
    }

    double FlowSolver::nonFlipFlow( const MatrixXd &_X, const MatrixXi &_F, const MatrixXd &_dir,
                                    const VectorXd &flip )
    {
        // init info
        int d = m_data->out_m->d();
        auto X = _X.leftCols( d );
        // good & bad step
        double good_s, bad_s;
        auto &dir = const_cast<MatrixXd &>(_dir);
        // find good step that flips bad faces back to normal
        ArrayXb F_good_flag = (flip.array() == 0.).eval();
        ArrayXb F_bad_flag = (flip.array() > 0.).eval();
        MatrixXi good_F = igl::slice_mask( _F, F_good_flag, 1 );
        MatrixXi bad_F = igl::slice_mask( _F, F_bad_flag, 1 );
        good_s = igl::flip_avoiding::compute_max_step_from_singularities(
                X, bad_F, dir );
        good_s *= 1.1;
        // find faces that share bad-V (V of bad faces), as they will be affected when bad-V are flowed
        VectorXi bad_Vi, tmp1, tmp2;
        igl::unique_rows( util::vec( bad_F ), bad_Vi, tmp1, tmp2 );
        MatrixXi F_might_flip;
        SolverUtil::selectnbfacesIf( bad_Vi, _F, m_data->V_F_adj, F_good_flag, F_might_flip );
        bad_s = igl::flip_avoiding::compute_max_step_from_singularities(
                X, F_might_flip, dir );
        bad_s *= 0.8;
        return std::min( bad_s, good_s );
    }

    SolverStatus FlowSolver::takeStep( int _iter )
    {
        // initialize information
        int d = Base::m_data->init_m->F().cols() - 1;
        int m = Base::m_data->init_m->F().rows();
        const MatrixXd &V_prev = m_data->out_m->V3d();

        m_data->out_m->setV( m_data->Xnext );
        int n_flips_prev = (Base::m_data->flip.array() > 0.0).count();
        SolverUtil::detFlips( d, m, m_data->T, m_data->out_m->V3d(), 0.0, Base::m_data->flip );
        int n_flips_next = (Base::m_data->flip.array() > 0.0).count();

        return SUCCESS;
    }

    /**
     * helpers
     */

    void FlowSolver::init_helper()
    {
        // compute cot laplacian
        igl::my_cotmatrix( m_data->out_m->V3d(), m_data->out_m->F(), true, m_L );
        // compute mass matrix
        igl::massmatrix( m_data->out_m->V3d(), m_data->out_m->F(), igl::MassMatrixType::MASSMATRIX_TYPE_BARYCENTRIC,
                         m_M );
    }
}