/*
    Uses gradient descent preconditioned by the hessian of the objective function
    Created by Yajie Yan on 4/29/18.
*/

#ifndef INJECTIVEMAPPING_RELAXER_PRECOND_GD_H
#define INJECTIVEMAPPING_RELAXER_PRECOND_GD_H

#include <Eigen/Sparse>
#include "SolverBase.h"
#include "symmdirichlet.h"

namespace injmap
{
    class RelaxerPrecondGD : public SolverBase
    {
    public:
        typedef SolverBase Base;
        RelaxerPrecondGD();
        ~RelaxerPrecondGD();

        /// initialize solver with given optimization data
        void initSolver( injmap::SolverData *_data );
        /// return solver state (can continue? converged? etc.)
        SolverStatus state() const;
        /// run one iteration of solver. output: new positions X_out
        virtual SolverStatus iterate( int _iter );
        /// compute gradient, step at each vertex
        SolverStatus computeStep( int _iter );
        /// take the step (post next V to the output V)
        SolverStatus takeStep( int _iter );

        /*
         * specific to this class
         */
        void setFixBadBoundary(bool _fix);

    protected:
        /**
         * data members
         */
        VectorXd m_simp_metric; // per simplex metric (area, volume)
        VectorXd m_pre_flip; // previous flipness
        VectorXi m_good_Fi; // previous good faces ids
        VectorXi m_hdls; // previous set of handle index
        bool m_fix_bad_bndry; // fix boundary V of bad region or allo them to move
        VectorXi m_free_ids; // previous set of free vts index
        // solver for TT*p = -gradient (m_p: search direction)
        Eigen::SimplicialLDLT<SparseMatrixXd> m_TT_solver;
        /// matrices used in sub system corresponding to directions at free vts:
        /// TT_ff * p_f = -grad_f + TT_fc * grad_c;
        SparseMatrixXd m_TT_ff, m_TT_fc;
        /*
         * energy instances
         */
        SymmDirichletEnergy m_SD_energy; // Note: not setup with any setting object in this solver
    protected:
        /**
         * helpers
         */
        bool isFlipChanged( const VectorXd &_pre_flip, const VectorXd &_cur_flip ) const;
        void build_system( const SparseMatrixXd &_TT,
                           const VectorXi &_free_Vi, const VectorXi &_fixed_Vi );
        SolverStatus solve_system( const MatrixXd &_b, const MatrixXd &_tgt,
                                   const VectorXi &_free_ids, const VectorXi &_fixed_ids,
                                   MatrixXd &_dir );
        double compute_energy( const MatrixXd &_X_new, const SparseMatrixXd &_T, const VectorXi &_FI,
                               const VectorXd &_FW ) const;
        void compute_gradient( const MatrixXd &_X_new, const SparseMatrixXd &_T, const VectorXi &_FI,
                               const VectorXd &_FW,
                               MatrixXd &_grad ) const;
    };
}

#endif //INJECTIVEMAPPING_RELAXER_PRECOND_GD_H
