//
// Utility functions useful to solver implementation
// Created by adobe on 4/15/18.
//

#include "SolverUtil.h"
#include "geom_utility.h"
#include "opt_utility.h"
#include <Eigen/SparseCholesky>
#include <igl/slice_mask.h>
#include <igl/slice_into.h>
#include <igl/is_border_vertex.h>
#include <unordered_set>
#include <iostream>

namespace injmap
{
    using namespace ::util;

    void SolverUtil::getBoundaryVids( const MatrixXd &_V, const MatrixXi &_F, VectorXi &_bnd_ids )
    {
        auto is_bnd = igl::is_border_vertex( _V, _F );
        _bnd_ids.resize( is_bnd.size() );
        int k = 0;
        for ( auto vi = 0; vi < is_bnd.size(); ++vi )
            if ( is_bnd[vi] )
            {
                _bnd_ids( k ) = vi;
                k++;
            }
        _bnd_ids.conservativeResize( k );
    }

    void SolverUtil::getNonBoundaryVids( const MatrixXd &_V, const MatrixXi &_F, VectorXi &_non_bnd_ids )
    {
        auto is_bnd = igl::is_border_vertex( _V, _F );
        _non_bnd_ids.resize( is_bnd.size() );
        VectorXi Vid_of_F, tmp1, tmp2;
        igl::unique_rows( util::vec( _F ), Vid_of_F, tmp1, tmp2 );
        int k = 0;
        for ( auto i = 0; i < Vid_of_F.size(); ++i )
        {
            auto vi = Vid_of_F( i );
            if ( !is_bnd[vi] )
            {
                _non_bnd_ids( k ) = vi;
                k++;
            }
        }
        _non_bnd_ids.conservativeResize( k );
    }

    void SolverUtil::initSolverData( const minimesh &_rest_m, const minimesh &_init_m, const VectorXi &_hdls,
                                     minimesh &_out_m,
                                     SolverData &_data )
    {
        _data.rest_m = &_rest_m;
        _data.init_m = &_init_m;
        _data.hdls = &_hdls;
        // init start mesh
        _data.out_m = &_out_m;
        igl::vertex_triangle_adjacency( _init_m.V(), _init_m.F(), _data.V_F_adj, _data.V_i_in_adjF );

        int d = _init_m.d();
        int sd = _init_m.sd();
        int m = _init_m.m();

        // compute operator T
        computeT( d, sd, _rest_m, _data.T, _data.TT );

        // compute flip-ness
        _data.flip.resize( m );
        SolverUtil::detFlips( d, m, _data.T, _data.out_m->V3d(), 0.0, _data.flip );
        _data.pre_flip = _data.flip;
    }

    void SolverUtil::enforceConstraint( const VectorXi &_Xi, const MatrixXd &_C, SparseMatrixXd &_A, MatrixXd &_b )
    {
        assert( _C.cols() == _b.cols() );
        /// change A first
        // build index of _Xi
        std::unordered_set<int> id_set;
        for ( auto i = 0; i < _Xi.size(); ++i )
        {
            id_set.insert( _Xi( i ) );
        }
        // then actually change A's entries
        for ( auto k = 0; k < _A.outerSize(); ++k )
        {
            for ( SparseMatrixXd::InnerIterator it( _A, k ); it; ++it )
            {
                int i = it.row(), j = it.col();
                auto &A_ij = it.valueRef();
                if ( id_set.count( i ) == 0 && id_set.count( j ) )
                {
                    _b.row( i ) -= A_ij * _C.row( j );
                    A_ij = 0.;
                }
            }
        }
        for ( auto k = 0; k < _A.outerSize(); ++k )
        {
            for ( SparseMatrixXd::InnerIterator it( _A, k ); it; ++it )
            {
                int i = it.row(), j = it.col();
                auto &A_ij = it.valueRef();
                if ( id_set.count( i ) && id_set.count( j ) == 0 )
                    it.valueRef() = 0.;
                else if ( i == j && id_set.count( i ) )
                    A_ij = 1.;
            }
        }
        /// then enforce corresponding X' values to right hand side
        igl::slice_into( igl::slice( _C, _Xi, 1 ), _Xi, 1, _b );
    }

    SolverStatus SolverUtil::solveWithConstr( const SparseMatrixXd &_A, const MatrixXd &_b,
                                              const VectorXi &_free, const VectorXi &_constr, const MatrixXd &_C,
                                              MatrixXd &_X )
    {
        // last 2 slides, https://cs.nyu.edu/~panozzo/gp18/Assignment4.pdf
        SparseMatrixXd A_ff, A_fc;
        igl::slice( _A, _free, _free, A_ff );
        igl::slice( _A, _free, _constr, A_fc );
        MatrixXd b_f = igl::slice( _b, _free, 1 );
        MatrixXd Xc = igl::slice( _C, _constr, 1 );
        MatrixXd b = b_f - A_fc * Xc;
        Eigen::SimplicialLDLT<SparseMatrixXd> lin_solver( A_ff );
        if ( lin_solver.info() != Eigen::Success )
        {
            //std::cout << "A_ff:" << std::endl;
            //std::cout << MatrixXd( A_ff ) << std::endl;
            return SolverStatus::LINFACTOR_FAIL;
        }
        // solve for free X
        MatrixXd X_f = lin_solver.solve( b ).eval();
        if ( lin_solver.info() != Eigen::Success )
            return SolverStatus::LINSOLVE_FAIL;
        igl::slice_into( X_f, _free, 1, _X );

        return SUCCESS;
    }

    VectorXi SolverUtil::setMinus( int _n, const VectorXi &_X )
    {
        VectorXi Y;
        setMinus( _n, _X, Y );
        return Y;
    }

    void SolverUtil::setMinus( int n, const VectorXi &_X, VectorXi &_Y )
    {
        _Y.resize( n );
        std::unordered_set<int> x_idx;
        for ( int i = 0; i < _X.size(); ++i )
        {
            x_idx.insert( _X( i ) );
        }
        int k = 0;
        for ( int i = 0; i < n; ++i )
        {
            if ( !x_idx.count( i ) )
            {
                _Y( k ) = i;
                k++;
            }
        }
        _Y.conservativeResize( k );
    }

    void SolverUtil::computeT( int d, int sd, const minimesh &_restmesh,
                               Eigen::SparseMatrix<double> &m_T, Eigen::SparseMatrix<double> &m_TT )
    {
        using namespace Eigen;

        int n = _restmesh.n();
        int m = _restmesh.m();
        auto B = MatrixXd::Identity( sd, sd ) - MatrixXd::Constant( sd, sd, 1.0 / sd );
        auto Id = MatrixXd::Identity( d, d );
        std::vector<Triplet<double>> T_entries;
        std::vector<Triplet<double>> T_awtd_entries;
        VectorXd areas( m );
        util::metric( _restmesh, areas );

        // T & T-area-weighted
        m_T.resize( d * d * m, d * n );
        Eigen::SparseMatrix<double> T_awtd;
        T_awtd.resize( d * d * m, d * n );
        m_TT.resize( d * n, d * n );
        MatrixXd T_simp_entries( d * d, d * sd );
        for ( auto t = 0; t < m; ++t )
        {
            auto a_wt = std::sqrt( areas( t ) );
            const auto &simp = _restmesh.F().row( t );
            const auto &V_simp = util::getEntry( _restmesh.V(), simp );
            // piece of T for cur simplex
            const auto &C = util::pinv( B * V_simp ) * B;
            util::kroneckerProduct( C, Id, T_simp_entries );
            //cout << "C = \n" << C << endl; // dbg
            //cout << "T-entries = \n" << T_simp_entries << endl; // dbg
            auto rrange = util::rowrangeOfSimp( t, d );
            //cout << "rrange = \n" << rrange << endl; // dbg
            // put T-simp-entries to proper location in T
            // note: to imbue handle constraints, some entries of T-area-weighted will be discarded
            for ( auto i = 0; i < T_simp_entries.rows(); ++i )
            {
                int r = rrange( 0 ) + i;
                for ( auto j = 0; j < sd; ++j )
                {
                    auto vi = simp( j );
                    auto ids_vi = util::idsOfVertexInX( vi, d, n );
                    for ( auto ii = 0; ii < ids_vi.size(); ++ii )
                    {
                        int c = ids_vi( ii );
                        double val = T_simp_entries( i, j * d + ii );
                        T_entries.push_back( Triplet<double>( r, c, val ) );
                        T_awtd_entries.push_back( Triplet<double>( r, c, a_wt * val ) );
                    }
                }
            }
        }
        m_T.setFromTriplets( T_entries.begin(), T_entries.end() );
        /*cout << "before pruning, # entries in T: " << m_T_ptr.nonZeros() << endl;
        m_T_ptr = m_T_ptr.pruned( m_restM.avgEdgeLen(), ::util::eps );
        cout << "after pruning with eps (edge-len) " << m_initM.avgEdgeLen()
             << ", # entries in T: " << m_T_ptr.nonZeros() << endl;*/
        T_awtd.setFromTriplets( T_awtd_entries.begin(), T_awtd_entries.end() );

        /************** forming TT **************/
        m_TT = (T_awtd.transpose() * T_awtd);
        //m_TT = (T_awtd.transpose() * T_awtd).pruned( m_restM.avgEdgeLen(), ::util::eps );
    }

    void SolverUtil::detFlips( int _d, int _m,
                               const VectorXd &_TX, double _thresh, VectorXd &_flip_flag )
    {
#ifdef DEBUG
        if ( _flip_flag.size() != _m )
        {
            logging::Logger::err( "_flip_flag size doesn't match num. of simplices!" );
            return;
        }
#endif
        MatrixXd mp;
        for ( auto i = 0; i < _m; ++i )
        {
            mp = util::getMap( _TX, i, _d );
            _flip_flag( i ) = mp.determinant() < _thresh;
        }
    }

    void SolverUtil::detFlips( int _d, int _m,
                               const Eigen::SparseMatrix<double> _T, const MatrixXd &_X, double _thresh,
                               VectorXd &_flip_flag )
    {
        detFlips( _d, _m, _T * util::vec( _d < _X.cols() ? _X.leftCols( _d ) : _X ), _thresh, _flip_flag );
    }

    void SolverUtil::selectnbfacesIf( const Eigen::VectorXi &_V, const MatrixXi _F,
                                      const std::vector<std::vector<int>> _V_F_adj, const ArrayXb &_cond,
                                      Eigen::MatrixXi &_retF )
    {
        ArrayXb final_mask = ArrayXb::Zero( _cond.size() );
        for ( auto i = 0; i < _V.size(); ++i )
        {
            int vi = _V( i );
            const auto &nbfs = _V_F_adj[vi];
            for ( auto ni : nbfs )
                final_mask( ni ) = _cond( ni );
        }
        _retF = igl::slice_mask( _F, final_mask, 1 );
    }

    MatrixXi SolverUtil::getFlipFaces( const MatrixXi &_F, const VectorXd &_flip )
    {
        return igl::slice_mask( _F, (_flip.array() > 0.), 1 );
    }

    MatrixXi SolverUtil::getNonFlipFaces( const MatrixXi &_F, const VectorXd &_flip )
    {
        return igl::slice_mask( _F, (_flip.array() == 0.), 1 );
    }

    void SolverUtil::findIds( ArrayXb _mask, VectorXi &_I1, VectorXi &_I2 )
    {
        _I1.resize( _mask.size() );
        _I2.resize( _mask.size() );
        int k1 = 0, k2 = 0;
        for ( auto i = 0; i < _mask.size(); ++i )
        {
            if ( _mask( i ) )
            {
                _I1( k1 ) = i;
                k1++;
            }
            else
            {
                _I2( k2 ) = i;
                k2++;
            }
        }
        _I1.conservativeResize( k1 );
        _I2.conservativeResize( k2 );
    }
}