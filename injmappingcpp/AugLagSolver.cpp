//
// Created by Yajie Yan on 8/15/17.
//

#include <unordered_set>
#include <string>
//#include <filesystem>
#include <igl/slice.h>
#include "AugLagSolver.h"
#include "logging.h"
#include "settings.h"
#include "utility.h"
#include "geom_utility.h"
#include "SolverUtil.h"

namespace injmap
{
    using std::string;
    using logging::Logger;
    using ::util::timecost;
    using ::util::myclock;
    typedef SettingEnum SE;

    AugLagSolver::AugLagSolver() : AugLagSolver( defaultSettings() )
    {}

    AugLagSolver::~AugLagSolver()
    {
        delete m_cur_iter;
        delete m_next_iter;
//        delete m_tmp_iter;
    }

    AugLagSolver::AugLagSolver( const Settings &_s ) : m_s( _s ), m_lsq( &m_s ), m_arap( &m_s ), m_D( &m_s ),
                                                       m_BD( &m_s )
    {
        m_initialized = false;
        m_cur_iter = new iterinout;
        m_next_iter = new iterinout;
//        m_tmp_iter = new iterinout;
    }

    void AugLagSolver::setFolder( const string &_folder )
    {
        // TODO: make path related operations os dependent (e.g. std filesystem)
        m_folder_name = _folder;
    }

    bool AugLagSolver::initialize( const minimesh &_restmesh, const minimesh &_initmesh,
                                   const VectorXi &_hdls )
    {
        cout << "#V/T = " << _restmesh.n() << "/" << _restmesh.m() << endl;
        Logger::info( "Initializing solver ..." );

        m_initialized = false;
        m_dbg_dump.clear();
        m_restM = _restmesh;
        m_initM = _initmesh;
        m_hdls = _hdls;
        m_s.at( SE::DIM ) = _restmesh.d();
        m_s.at( SE::N ) = _restmesh.n();
        m_s.at( SE::SDIM ) = _restmesh.d() + 1;
        m_s.at( SE::M ) = _restmesh.m();

        /************** form T & TT **************/
//        init_T( _restmesh );
        SolverUtil::computeT( m_s.at( SE::DIM ), m_s.at( SE::SDIM ), _restmesh, m_T, m_TT );
        //cout << "T = \n" << m_T_ptr << endl;
        // modify m_TT to enforce constraints
        constrain_H( m_TT, _hdls );
//        cout << "TT = \n" << m_TT << endl;
        // pre-factorize TT
        m_TT_solver.compute( m_TT );

        m_cur_iter = new iterinout( _restmesh.d(), (int) _restmesh.n(), (int) _restmesh.m() );
        m_next_iter = new iterinout( _restmesh.d(), (int) _restmesh.n(), (int) _restmesh.m() );
        /************** Initialize quantities for energies **************/
        m_cur_iter->X = util::vec( _initmesh.V() );
        auto &TX1 = m_cur_iter->TX1;
        TX1 = m_T * m_cur_iter->X;
        m_cur_iter->TXr = m_cur_iter->TX1;
        util::computeSVDs( TX1, _restmesh.d(), (int) _restmesh.m(), m_cur_iter->svds );
        util::findRots( m_cur_iter->svds, m_cur_iter->rots );
        util::metric( _restmesh, m_cur_iter->areas );
        //cout << "areas = " << RowVectorXd( m_cur_iter->areas ) << endl;

        /************** initialize quantities for constraints **************/
        m_cur_iter->k_D.setOnes(); //init_kD();
        init_lmd_D( TX1, m_cur_iter->lmd_D );
        m_cur_iter->k_B.setOnes();
        init_lmd_B( m_cur_iter->svds, m_cur_iter->lmd_B );

        /************** initialize flags **************/
        det_flips( m_cur_iter->TXr, m_cur_iter->flips );
        det_deform( m_cur_iter->svds, m_s.at( SE::BDBND ), m_cur_iter->deforms );
        //cout << "flips = " << RowVectorXd( m_cur_iter->flips ) << endl;
        //cout << "deforms = " << RowVectorXd( m_cur_iter->deforms ) << endl;

        /************** cur objective value **************/
        double wlsq, wD, wB, warap;
        if ( m_s.at( SE::AARAP ) == AArapVal::YES )
        {
            //save current weights
            wlsq = m_s.at( SE::W_LSQ );
            wD = m_s.at( SE::W_D );
            wB = m_s.at( SE::W_B );
            warap = m_s.at( SE::W_ARAP );
            // first step is pure arap
            m_s.at( SE::W_LSQ ) = 0.0;
            m_s.at( SE::W_D ) = 0.0;
            m_s.at( SE::W_B ) = 0.0;
            m_s.at( SE::W_ARAP ) = warap;
        }
        Vector4d energies;
        m_cur_iter->F_val = eval_F( TX1, m_cur_iter->TXr,
                                    m_cur_iter->k_D, m_cur_iter->lmd_D,
                                    m_cur_iter->k_B, m_cur_iter->lmd_B, m_s.at( SE::BDBND ),
                                    m_cur_iter->svds, m_cur_iter->rots, m_cur_iter->areas,
                                    m_s.at( SE::DBG_DUMP ) ? &energies : nullptr );
        if ( m_s.at( SE::DBG_DUMP ) )
        {
            m_dbg_dump.F.push_back( m_cur_iter->F_val );
            m_dbg_dump.lsq.push_back( energies( 0 ) );
            m_dbg_dump.arap.push_back( energies( 1 ) );
            m_dbg_dump.D.push_back( energies( 2 ) );
            m_dbg_dump.BD.push_back( energies( 3 ) );
            m_dbg_dump.flipness.push_back( m_cur_iter->flips.sum() );
            m_dbg_dump.nflips.push_back( m_cur_iter->flips.rowwise().any().sum() );
        }

        if ( m_s.at( SE::AARAP ) == AArapVal::YES )
        {
            // restore weights
            m_s.at( SE::W_LSQ ) = wlsq;
            m_s.at( SE::W_D ) = wD;
            m_s.at( SE::W_B ) = wB;
            m_s.at( SE::W_ARAP ) = warap;
        }

        /************** copy cur-iter to next-iter to initialize its values **************/
        *m_next_iter = *m_cur_iter;
        m_initialized = true;

        Logger::info( "Solver initialized." );
        cout << "# flips = " << m_cur_iter->flips.rowwise().any().sum() << endl;
        return m_initialized;
    }

    AugLagSolver::SolverStatus AugLagSolver::solverStatus() const
    {
        int iter = m_cur_iter->i;
        int max_iter = m_s.at( SE::MAX_ITER );
        double wD = m_s.at( SE::W_D );
        double wBD = m_s.at( SE::W_B );

        auto converged = [&]() -> bool
        {
            if ( wBD )
                return m_cur_iter->deforms.sum() <= 0.0;
            return m_cur_iter->flips.sum() <= 0.0;
        };

        if ( converged() )
            return SolverStatus::CONVERGED;
        else if ( !m_cur_iter->ls_succ || m_cur_iter->i >= max_iter )
            return SolverStatus::FAIL_TO_CONVERGE;
        else
            return SolverStatus::CAN_CONTINUE;
    }

    bool AugLagSolver::finish()
    {
        bool suc = true;
        /************** dump dbg info to files **************/
        if ( m_s.at( SE::DBG_DUMP ) )
        {
            Logger::info( "Dumping debug info to files..." );
            suc &= m_dbg_dump.exportToFiles( m_folder_name + "dbg/" );
            if ( !suc )
                Logger::err( "Cannot complete file dumping!" );
            else
                Logger::info( "File dumped." );
        }
        return suc;
    }

    bool AugLagSolver::run()
    {
        if ( !m_initialized )
        {
            Logger::fatal( "Solver not initialized!" );
            return false;
        }

        bool iter_succ = false;
        int iter = 0;
        while ( solverStatus() == SolverStatus::CAN_CONTINUE )
        {
            iter++;
            iter_succ = iterate( m_cur_iter );
            if ( !iter_succ )
                break;
        }

        /************** verify results **************/
        if ( solverStatus() == SolverStatus::CONVERGED )
            Logger::info( "Optimization completed after " + std::to_string( iter ) + " iter.s" );
        else if ( solverStatus() == SolverStatus::FAIL_TO_CONVERGE )
        {
            Logger::info( "Optimization couldn't terminate (max-iter reached)." );
            return false;
        }
        else if ( !iter_succ )
        {
            Logger::info( "Optimization failed at iter " + std::to_string( iter ) );
            return false;
        }
        else
        {
            Logger::fatal( "Logic Error!" );
            return false;
        }

        return true;
    }

    void AugLagSolver::getResultForRender( MatrixXd &_V3d ) const
    {
        _V3d = util::unvec( m_cur_iter->X, 3 );
        minimesh::augmentVtsForRender( _V3d );
    }

    bool AugLagSolver::iterate( iterinout *&_inout )
    {
        auto suc = m_s.at( SE::AARAP ) == AArapVal::YES
                   ? iterateAARAP( *_inout, *m_next_iter )
                   : iterateRegular( *_inout, *m_next_iter );
        if ( suc )
            std::swap( _inout, m_next_iter );
        return suc;
    }

    bool AugLagSolver::iterateRegular( const iterinout &_in, iterinout &_out,
                                       bool _update_D, bool _update_B )
    {
        // increment iterator counter
        _out.i = _in.i + 1;
        Logger::info( "----------- start of iteration " + std::to_string( _out.i ) + " -----------" );

        if ( !m_initialized )
        {
            Logger::fatal( "Solver not initialized!" );
            return false;
        }

        int d = m_s.at( SE::DIM );
        int m = m_s.at( SE::M );
        double BDK = m_s.at( SE::BDBND );

        /// start timing of iteration
        timecost total_t, grad_t, cons_rh_t, ls_t, solve_t, update_t;
        total_t.start();

        /************** find direction and line-search along that **************/
        VectorXd TX = m_T * _in.X;
        //cout << "TX = " << RowVectorXd( TX ) << endl;
        //cout << "_in.Txr = " << RowVectorXd( _in.TXr ) << endl;
        grad_t.start();
        VectorXd rh = grad_F( TX, _in.TXr, _in.svds, _in.rots,
                              _in.k_D, _in.lmd_D, _in.k_B, _in.lmd_B,
                              _in.areas, m_restM.F() );
        grad_t.end();
        cons_rh_t.start();
        constrain_rh( rh, m_hdls );
        cons_rh_t.end();

        solve_t.start();
        VectorXd p = m_TT_solver.solve( -rh );
        solve_t.end();

        // line search
        double F_val = eval_F( TX, _in.TXr,
                               _in.k_D, _in.lmd_D,
                               _in.k_B, _in.lmd_B, m_s.at( SE::BDBND ),
                               _in.svds, _in.rots, _in.areas, nullptr );

        ls_t.start();
        auto ls_res = linesearch( _in.X, p, F_val, rh, _in.TXr,
                                  _in.k_D, _in.lmd_D, _in.k_B, _in.lmd_B, BDK,
                                  _in.svds, _in.rots, _in.areas );
        ls_t.end();
        if ( m_s.at( SE::DBG_DUMP ) )
        {
            double max_p = util::unvec( p, d ).rowwise().norm().maxCoeff();
            m_dbg_dump.pmax.push_back( max_p );
            m_dbg_dump.step.push_back( ls_res.step );
            m_dbg_dump.pstep.push_back( max_p * ls_res.step );
        }

        /************** evaluate line-search result **************/
        _out.ls_succ = ls_res.suc;
        if ( !ls_res.suc || m_s.at( SE::VERBOSE ) )
        {
            Logger::info( string( "linesearch iter " ) + std::to_string( ls_res.i ) );
            //std::cout << "rh = " << std::endl << RowVectorXd( rh ) << std::endl;
            Logger::info( "old/new-F_val: " + std::to_string( F_val ) + "/" + std::to_string( ls_res.new_F ) );
            //cout << "p = \n" << RowVectorXd( m_p ) << endl;
            double max_p = util::unvec( p, d ).rowwise().norm().maxCoeff();
            Logger::info( string( "|p|, step, |m_p|*m_step = " ) +
                          std::to_string( max_p ) + ", " +
                          std::to_string( ls_res.step ) + ", " +
                          std::to_string( ls_res.step * max_p ) );

            if ( !ls_res.suc )
            {
                Logger::err( string() +
                             "Linesearch failed (optimization from this point on cannot be trusted)! " );
                //return false;
            }
        }

        /************** TODO: add ada-k here **************/

        /************** record some info of cur iter **************/
        _out.p = p;
        _out.step = ls_res.step;
        _out.F_val = ls_res.new_F;

        /************** update quantities for next iter **************/
        // update X & X-related
        _out.X = _in.X + ls_res.step * p;
        _out.TX1 = _in.TX1;
        _out.TXr = m_s.at( SE::REF_MAP ) == RefMapVal::X1 ? _in.TX1 : TX;
        VectorXd TnewX = m_T * _out.X;

        update_t.start();
        // svds and rots
        util::computeSVDs( TnewX, d, m, _out.svds );
        util::findRots( _out.svds, _out.rots );

        // update for constraint D
        if ( _update_D || m_s.at( SE::W_D ) )
        {
            VectorXd c_D = VectorXd::Zero( m );
            for ( auto i = 0; i < c_D.size(); ++i )
            {
                c_D( i ) = m_D.evalForSimp( i, TnewX );
            }
            _out.lmd_D = update_lambdas( _in.k_D, c_D, _in.lmd_D );
        }
        // of course we almost always want to update flip status
        det_flips( TnewX, _out.flips );

        // update for constraint BD
        if ( _update_B || m_s.at( SE::W_B ) )
        {
            VectorXd c_B = VectorXd::Zero( m );
            for ( auto i = 0; i < c_B.size(); ++i )
            {
                c_B( i ) = m_BD.evalForSimp( _out.svds[i] );
            }
            _out.lmd_B = update_lambdas( _in.k_B, c_B, _in.lmd_B );
            // out-of-bound deform status
            det_deform( _out.svds, BDK, _out.deforms );
        }

        int n_flips = 0;
        for ( auto i = 0; i < _out.flips.size(); ++i )
            n_flips += _out.flips( i ) > 0.0;
        cout << "# flips = " << n_flips << endl;

        update_t.end();

        /*// Alternatively, update gradient in the end.
         * //This makes no difference unless aarap is used.
         * _out.gradF = grad_F( TnewX, _out.TXr, _out.svds, _out.rots,
                              _out.k_D, _out.lmd_D, _out.k_B, _out.lmd_B,
                              _out.areas, m_restM.F() );*/

        Logger::info( "----------- end of iteration " + std::to_string( _out.i ) + " -----------" );

        total_t.end();

        /// end of timing
        cout << "Total time: " << total_t.d.count() << endl;
        cout << "grad time: " << grad_t.d.count() << endl;
        cout << "constrain-rh time: " << cons_rh_t.d.count() << endl;
        cout << "back-solve time: " << solve_t.d.count() << endl;
        cout << "ls time: " << ls_t.d.count() << endl;
        cout << "update time: " << update_t.d.count() << endl;

        /// save energy related info
        if ( m_s.at( SE::DBG_DUMP ) )
        {
            Vector4d energies;
            double F_val = eval_F( TnewX, _out.TXr,
                                   _out.k_D, _out.lmd_D,
                                   _out.k_B, _out.lmd_B, m_s.at( SE::BDBND ),
                                   _out.svds, _out.rots, _out.areas, m_s.at( SE::DBG_DUMP ) ? &energies : nullptr );
            m_dbg_dump.F.push_back( F_val );
            m_dbg_dump.lsq.push_back( energies( 0 ) );
            m_dbg_dump.arap.push_back( energies( 1 ) );
            m_dbg_dump.D.push_back( energies( 2 ) );
            m_dbg_dump.BD.push_back( energies( 3 ) );
            m_dbg_dump.flipness.push_back( m_cur_iter->flips.sum() );
            m_dbg_dump.nflips.push_back( m_cur_iter->flips.rowwise().any().sum() );
        }

        return true;
    }

    bool AugLagSolver::iterateAARAP( const iterinout &_in, iterinout &_out )
    {
        //save current weights
        double wlsq, wD, wB, warap;
        wlsq = m_s.at( SE::W_LSQ );
        wD = m_s.at( SE::W_D );
        wB = m_s.at( SE::W_B );
        warap = m_s.at( SE::W_ARAP );
        if ( _in.i % 2 == 0 ) // pure arap
        {
            m_s.at( SE::W_LSQ ) = 0.0;
            m_s.at( SE::W_D ) = 0.0;
            m_s.at( SE::W_B ) = 0.0;
            m_s.at( SE::W_ARAP ) = warap;
        }
        else
        {
            m_s.at( SE::W_LSQ ) = wlsq;
            m_s.at( SE::W_D ) = wD;
            m_s.at( SE::W_B ) = wB;
            m_s.at( SE::W_ARAP ) = 0.0;
        }
        bool suc = iterateRegular( _in, _out, wD > 0.0, wB > 0.0 );

        // restore weights
        m_s.at( SE::W_LSQ ) = wlsq;
        m_s.at( SE::W_D ) = wD;
        m_s.at( SE::W_B ) = wB;
        m_s.at( SE::W_ARAP ) = warap;
        return suc;
    }

    bool AugLagSolver::iterateWithVis( igl::opengl::glfw::Viewer&_viewer )
    {
        // run an iteration first
        bool succ = iterate( m_cur_iter );
        // then update viewer with the results
        int d = m_s.at( SE::DIM );
        MatrixXd V = util::unvec( m_cur_iter->X, d );
        minimesh::augmentVtsForRender( V );
        _viewer.data().set_vertices( V );
        _viewer.data().compute_normals();
        MatrixXd E_color;
        detEdgeColor( E_color );
        _viewer.data().set_edges( V, m_restM.E(), E_color );
        return succ;
    }

    void AugLagSolver::detEdgeColor( const minimesh &_mesh,
                                     const VectorXd &_flips, const VectorXd &_deforms,
                                     MatrixXd &_color ) const
    {
        int sd = m_s.at( SE::SDIM );
        int d = m_s.at( DIM );
        const auto &E = _mesh.E();
        if ( _color.rows() != E.rows() || _color.cols() != E.cols() )
        {
            _color.resize( E.rows(), 3 );
        }
        auto default_c = Eigen::Vector3d( 0, 0, 0 );
        _color.col( 0 ).setConstant( default_c( 0 ) );
        _color.col( 1 ).setConstant( default_c( 1 ) );
        _color.col( 2 ).setConstant( default_c( 2 ) );
        MatrixXi es_f( d == 2 ? 3 : 6, 2 ); // tri or tet
        auto flip_c = Eigen::RowVector3d( 1.0, 0.0, 0.0 );
        auto deform_c = Eigen::RowVector3d( 0.0, 0.0, 1.0 );

        // \possibly need to set them to another color if deformed a "lot"
        if ( m_s.at( SE::W_B ) )
            for ( auto i = 0; i < _deforms.size(); ++i )
            {
                if ( _deforms( i ) <= 0.0 )
                    continue;
                minimesh::mkEdgesFromSimp( _mesh.F().row( i ), es_f );
                for ( auto j = 0; j < es_f.rows(); ++j )
                {
                    auto e = es_f.row( j );
                    _color.row( _mesh.getEdgeID( e ) ) = deform_c;
                }
            }
        // \Note: this comes last so it can take highest priority
        // (by overriding the ealier set colors)
        // set flipped faces' edges to a color
        for ( auto i = 0; i < _flips.size(); ++i )
        {
            if ( _flips( i ) <= 0.0 )
                continue;
            minimesh::mkEdgesFromSimp( _mesh.F().row( i ), es_f );
            for ( auto j = 0; j < es_f.rows(); ++j )
            {
                auto e = es_f.row( j );
                _color.row( _mesh.getEdgeID( e ) ) = flip_c;
            }
        }
    }

    void AugLagSolver::detEdgeColor( MatrixXd &_E_color ) const
    {
        detEdgeColor( m_restM, m_cur_iter->flips, m_cur_iter->deforms, _E_color );
    }

    /*****************************
    / Below are protected helpers
    ******************************/

    double AugLagSolver::eval_F( const VectorXd &_TX, const VectorXd &_TXr,
                                 const VectorXd &_k_D, const VectorXd &_lmds_D,
                                 const VectorXd &_k_B, const VectorXd &_lmds_B, double _BDK,
                                 const std::vector<util::svdstruct> &_svds, const MatrixXd &_rots,
                                 const VectorXd &_metric,
                                 Vector4d *const _breakdown = nullptr ) const
    {
        bool awtd = m_s.at( SE::AREA_WTD );
        VectorXd const_m = VectorXd::Constant( 1, 1.0 );
        double F = 0.0;
        auto e_lsq = m_s.at( W_LSQ ) > 0.0 ? m_s.at( W_LSQ ) * m_lsq.eval( _TX, _TXr, _metric ) : 0.0;
        auto e_arap = m_s.at( W_ARAP ) > 0.0 ?
                      m_s.at( W_ARAP ) * m_arap.eval( _TX, _rots, _metric ) : 0.0;
        auto c_D = m_s.at( W_D ) > 0.0 ?
                   m_s.at( W_D ) * m_D.eval( _k_D, _lmds_D, _TX, awtd ? _metric : const_m ) : 0.0;
        auto c_B = m_s.at( W_B ) > 0.0 ?
                   m_s.at( W_B ) * m_BD.eval( _k_B, _lmds_B, _svds, awtd ? _metric : const_m ) : 0.0;
        F = e_lsq + e_arap + c_D + c_B;
        if ( _breakdown )
            *_breakdown = Vector4d( e_lsq, e_arap, c_D, c_B );
//        cout << "TX-TXr = " << RowVectorXd( _TX - _TXr ) << endl;
//        cout << "e_lsq/arap/D/B = " << e_lsq << "/" << e_arap << "/" << c_D << "/" << c_B << endl;

        return F;
    }

    AugLagSolver::LSret
    AugLagSolver::linesearch( const VectorXd &_X, const VectorXd &_p, double _F, const VectorXd &_grd_F,
                              const VectorXd &_TXr, const VectorXd &_k_D, const VectorXd &_lmds_D,
                              const VectorXd &_k_B, const VectorXd &_lmds_B, double _BDK,
                              const std::vector<util::svdstruct> &_svds, const MatrixXd &_rots, const VectorXd &_areas,
                              double _c, double _b )
    {
        int d = m_s.at( SE::DIM );
        int m = m_s.at( SE::M );
        double new_F = 0.0;
        double step = 1.0;
        bool decreased = false;
        int i = 0, max_i = 30;
        std::vector<util::svdstruct> new_svds;
        if ( m_s.at( W_B ) > 0 )
            new_svds.resize( _svds.size() );
        while ( true )
        {
            auto new_X = _X + step * _p;
            auto TnewX = m_T * new_X;
            // recompute ssvds
            if ( m_s.at( W_B ) > 0 )
                util::computeSVDs( TnewX, d, m, new_svds );
            new_F = eval_F( TnewX, _TXr, _k_D, _lmds_D, _k_B, _lmds_B, _BDK,
                            m_s.at( W_B ) > 0 ? new_svds : _svds,
                            _rots, _areas );
            decreased = new_F <= _F + _c * step * _grd_F.dot( _p );

            i++;
            if ( decreased ) break;
            if ( i == max_i )
            {
                logging::Logger::info( "linesearch: max-iter reached." );
                break;
            }

            // else, continue
            step = _b * step;
        }
        return {decreased, step, i, new_F};
    }

    VectorXd AugLagSolver::update_lambdas( const VectorXd &_k, const VectorXd &_g, const VectorXd &_lmds ) const
    {
        auto new_lmds = _lmds;
        new_lmds.setZero();
        for ( auto i = 0; i < _lmds.size(); ++i )
            new_lmds( i ) = _g( i ) <= _lmds( i ) / _k( i ) ? _lmds( i ) - _k( i ) * _g( i ) : 0.0;
        return new_lmds;
    }

    VectorXd AugLagSolver::update_G_t( double _kt, double _lmd_t, double _g_t, const VectorXd &_gradg_t ) const
    {
        return Eigen::VectorXd();
    }

    VectorXd
    AugLagSolver::evalG_for_D( const VectorXd &_k, const VectorXd &_lmds,
                               const VectorXd &_TX, const MatrixXi &_simps ) const
    {
        return Eigen::VectorXd();
    }

    VectorXd AugLagSolver::evalG_for_B( const std::vector<util::svdstruct> &_svds,
                                        double _BDK, const VectorXd &_k, const VectorXd &_lmds,
                                        const VectorXd &_TX,
                                        const MatrixXi &_simps ) const
    {
        return Eigen::VectorXd();
    }

    VectorXd AugLagSolver::grad_F( const VectorXd &_TX, const VectorXd &_TXr,
                                   const std::vector<util::svdstruct> &_svds, const MatrixXd &_rots,
                                   const VectorXd &_k_D, const VectorXd &_lmds_D,
                                   const VectorXd &_k_B, const VectorXd &_lmds_B,
                                   const VectorXd &_metric,
                                   const MatrixXi &_simps ) const
    {
        bool awtd = m_s.at( SE::AREA_WTD );
        VectorXd const_m = VectorXd::Constant( 1, 1.0 );
        VectorXd g_lsq, g_arap, g_c_d, g_c_bd;
        g_lsq = g_arap = g_c_d = g_c_bd = VectorXd::Zero( m_s.at( DIM ) * m_s.at( N ) );
        if ( m_s.at( W_LSQ ) > 0.0 )
            g_lsq = m_s.at( W_LSQ ) * m_lsq.grad( m_T, _TX, _TXr, _metric, _simps );
        if ( m_s.at( W_ARAP ) > 0.0 )
            g_arap = m_s.at( W_ARAP ) * m_arap.grad( _TX, m_T, _rots, _metric, _simps );
        if ( m_s.at( W_D ) > 0.0 )
            g_c_d = m_s.at( W_D ) * m_D.grad( _k_D, _lmds_D, m_T, _TX, _simps,
                                              awtd ? _metric : const_m );
        if ( m_s.at( W_B ) > 0.0 )
            g_c_bd = m_s.at( W_B ) * m_BD.grad( _svds, _k_B, _lmds_B, m_T, _simps,
                                                awtd ? _metric : const_m );

        std::cout << "w-lsq= " << m_s.at( W_LSQ ) << std::endl;
        std::cout << "w-D= " << m_s.at( W_D ) << std::endl;
        std::cout << "w-arap= " << m_s.at( W_ARAP ) << std::endl;
        /*std::cout << "grad lsq=" << std::endl << RowVectorXd( g_lsq ) << std::endl;
        std::cout << "grad arap=" << std::endl << RowVectorXd( g_arap ) << std::endl;
        std::cout << "grad D=" << std::endl << RowVectorXd( g_c_d ) << std::endl;
        std::cout << "grad BD=" << std::endl << RowVectorXd( g_c_bd ) << std::endl;*/
        /*std::cout << "k-B = " << RowVectorXd( _k_B ) << " | lmd-B = " << RowVectorXd( _lmds_B ) << endl;*/
        return g_lsq + g_arap + g_c_d + g_c_bd;
    }

    void AugLagSolver::init_lmd_D( const VectorXd &_TX, VectorXd &_lmds ) const
    {
        for ( auto i = 0; i < m_initM.m(); ++i )
            _lmds( i ) = -std::min( 0.0, util::getMap( _TX, i, m_initM.d() ).determinant() );
    }

    void AugLagSolver::init_lmd_B( const std::vector<util::svdstruct> &_svds, VectorXd &_lmds ) const
    {
        for ( int i = 0; i < m_initM.m(); ++i )
            _lmds( i ) = m_BD.isViolated( m_BD.evalForSimp( _svds[i] ) );
    }

    void AugLagSolver::det_flips( const VectorXd &_TX, VectorXd &_flip_flag ) const
    {
#ifdef DEBUG
        if ( _flip_flag.size() != m_restM.m() )
        {
            logging::Logger::err( "_flip_flag size doesn't match num. of simplices!" );
            return;
        }
#endif
        MatrixXd mp;
        for ( auto i = 0; i < m_restM.m(); ++i )
        {
            mp = util::getMap( _TX, i, m_restM.d() );
            _flip_flag( i ) = m_D.isViolated( m_D.evalForSimp( i, _TX ) );
//            _flip_flag( i ) = m_D.evalForSimp( i, _TX );
        }
    }

    void AugLagSolver::constrain_H( SparseMatrix<double> &_TT, const VectorXi &_hdls ) const
    {
        // put handle's_map coordinates indices (in X) into a set
        std::unordered_set<int> hdl_set;
        for ( auto i = 0; i < _hdls.size(); ++i )
        {
            auto vi = _hdls( i );
            auto ids_vi = util::idsOfVertexInX( vi, (int) m_s.at( SE::DIM ), (int) m_s.at( SE::N ) );
            for ( auto ii = 0; ii < ids_vi.size(); ++ii )
            {
                hdl_set.insert( ids_vi( ii ) );
            }
        }
        // iterateRegular through sparse matrix TT and modify entries properly
        for ( auto k = 0; k < _TT.outerSize(); ++k )
        {
            for ( SparseMatrix<double>::InnerIterator it( _TT, k ); it; ++it )
            {
                if ( hdl_set.count( it.row() ) || hdl_set.count( it.col() ) )
                    it.valueRef() = it.row() == it.col() ? 1.0 : 0.0;
            }
        }
        _TT.pruned();
    }

    void AugLagSolver::constrain_rh( VectorXd &_rh, const VectorXi &_hdls ) const
    {
        for ( auto i = 0; i < _hdls.size(); ++i )
        {
            auto hi = _hdls( i );
            auto vi_ids = util::idsOfVertexInX( hi, m_s.at( SE::DIM ), m_s.at( SE::N ) );
            for ( auto ii = 0; ii < vi_ids.size(); ++ii )
                _rh( vi_ids( ii ) ) = 0.0;
        }
    }

    void AugLagSolver::det_deform( const std::vector<util::svdstruct> &_svds, double _BDK, VectorXd &_deforms ) const
    {
        for ( auto t = 0; t < _svds.size(); ++t )
        {
            const auto &svd = _svds[t];
            auto _d = m_BD.evalForSimp( svd );
            _deforms( t ) = m_BD.isViolated( _d );
        }
    }
}
