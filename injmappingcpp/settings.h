//
// Created by Yajie Yan on 8/14/17.
//

#ifndef INJECTIVEMAPPING_SETTINGS_H
#define INJECTIVEMAPPING_SETTINGS_H

#include <unordered_map>
#include <vector>
#include <string>

namespace injmap
{
    using std::unordered_map;
    using std::vector;
    using std::string;

    // possible settings
    // Note: to add new setting, make sure to
    // add new enum-string entry in build_setting_strings() &
    // add new default value in defaultSettings()
    enum SettingEnum
    {
        // optimization
                AARAP, MAX_ITER, VERBOSE,
        // objective function
                W_LSQ, W_ARAP, W_D, W_B,
        // line search
                LS_SCHEME,
        // k-related
                ADA_k, kD, kB, lmdD, lmdB,
        // BD-related
                BDBND,
        // lsq-related
                REF_MAP,
        // mesh specific
                DIM, SDIM, N, M,
        // constraints related
                AREA_WTD,
        /// program wide congtrols
                DBG_DUMP // dump debug info to file
    };

    enum YesNo
    {
        YES = 1, NO = 0
    };
    // enable or disable aARAP
    typedef YesNo AArapVal;
    // possible verbose settings
    typedef YesNo VerboseVal;
    // possible settings for linesearch scheme
    enum LSSchemeVal
    {
        FULL = 0, BT = 1
    };
    // Adaptive-k setting values
    typedef YesNo AdaKVal;
    // possible settings for reference map
    enum RefMapVal
    {
        Xi = 0, X1 = 1
    };
    // possible settings for AREA_WTD
    typedef YesNo AreaWtdVal;
    /// prog wide settings
    typedef YesNo DBGDumpVal;

    struct EnumHash
    {
        template<typename T>
        std::size_t operator()(T t) const
        {
            return static_cast<std::size_t>(t);
        }
    };

    typedef unordered_map<SettingEnum, double, EnumHash> Settings;

/*****************************
 * Setting API
 *****************************/
    Settings defaultSettings();

    Settings defaultSettings(const Settings &_keyvals);

    void changeSettings(
            Settings &_settings,
            const Settings &_keyvals);

    /// \Breif read settings from given file
    Settings readSettings(const string &_setting_file);
}
#endif //INJECTIVEMAPPING_SETTINGS_H
