//
// Created by Yajie Yan on 4/14/18.
//

/// stl
#include <string>
#include <cstdio> // for printf(), etc.
/// libigl
#include <igl/barycenter.h>
#include <igl/cotmatrix.h>
#include <igl/massmatrix.h>
#include <igl/slice.h>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/colormap.h>
/// ours'
#include "import.h"
#include "opt_utility.h"
#include "IterInjSolver.h"
#include "SolverUtil.h"

using std::string;
using namespace injmap;

char pathsep = '/';
#ifdef _WIN32
pathsep = '\\';
#endif

void usage( int _argc, char **_argv )
{
    string progname = _argv[0];
    auto find_i = progname.rfind( pathsep, progname.length() );
    if ( find_i != string::npos )
        progname = progname.substr( find_i + 1 );
    printf( "usage: \n%s folder_name restmesh_name initmesh_name hdlfile_name [n-flows] \n", progname.c_str() );
}

int main( int _argc, char **_argv )
{
    if ( _argc < 5 )
    {
        usage( _argc, _argv );
        exit( -1 );
    }

    // parse cmd line
    string folder = _argv[1];
    string rest_file = folder + pathsep + _argv[2];
    string init_file = folder + pathsep + _argv[3];
    string hdl_file = folder + pathsep + _argv[4];
    int n_flows = 1;
    if ( _argc > 5 )
        n_flows = std::stoi( _argv[5] );

    // import mesh data for optimization
    ::util::minimesh rest_m, init_m, out_m;
    VectorXi hdls;
    bool import_suc = ::util::Importer::importMeshData( rest_file, init_file, hdl_file, rest_m, init_m, hdls );
    if ( !import_suc )
    {
        printf( "Error: failed to load input data from folder: %s", folder.c_str() );
        usage( _argc, _argv );
        exit( -1 );
    }
    out_m = init_m;

    // create solver data for this session
    SolverData solver_data;
    SolverUtil::initSolverData( rest_m, init_m, hdls, out_m, solver_data );
    printf( "Done: solver data initialized. \n" );

    // create viewer
    igl::opengl::glfw::Viewer viewer;
    // initialize solver
    IterInjSolver solver;
    solver.setViewer( viewer );
    solver.initSolver( &solver_data );
    solver.setFlowN( n_flows );
    printf( "Done: solver initialized. \n" );

    // iteration related
    bool do_iterate = false;
    int iter = 0; // count iteration
    MatrixXd X_out;

    const auto &keyDown = [&]( igl::opengl::glfw::Viewer &_viewer, unsigned char _key, int _mod ) -> bool
    {
//        printf( "detected key: %c\n", _key );
        // toggle animation
        if ( _key == ' ' || _key == 's' || _key == 'S' /*|| _key == 'p' || _key == 'P'*/ )
        {
            do_iterate = true;
        }
        if ( _key == 'g' || _key == 'G' )
            _viewer.core.is_animating = !_viewer.core.is_animating;

        // determine iteration mode
        if ( _key == ' ' )
        {
            solver.setIterMode( IterMode::FULL_ITER );
        }
        else if ( _key == 's' || _key == 'S' )
        {
//            solver.setIterMode( IterMode::BY_STEP );
            solver.setIterMode( IterMode::BY_STEP_MIX );
        }
        else if ( _key == 'g' || _key == 'G' )
        {
            if ( solver.getIterMode() != IterMode::NON_STOP )
                solver.setIterMode( IterMode::NON_STOP );
            else
                solver.setIterMode( IterMode::STOP );
        }
        /*else if ( _key == 'p' || _key == 'P' )
        {
            solver.setIterMode( IterMode::STOP );
        }*/
        else
            return true;
        return false;
    };
    const auto &preDraw = [&]( igl::opengl::glfw::Viewer &_viewer ) -> bool
    {
        if ( do_iterate || solver.getIterMode() == IterMode::NON_STOP )
        {
            printf( "------ iter %d ------ \n", iter );
            auto suc = solver.iterate( iter );
            iter++;
            if ( suc == SolverStatus::LINSOLVE_FAIL )
            {
                printf( "Error: linear solve failed.\n" );
                exit( -1 );
            }
            solver.getX( X_out );
            // update vertices to display
            _viewer.data().set_vertices( X_out );
            do_iterate = false;
        }
        return false;
    };

    // set up renderer
    solver.initViewer();
    viewer.callback_key_down = keyDown;
    viewer.callback_pre_draw = preDraw;
    viewer.core.animation_max_fps = 120.;
    viewer.core.is_animating = false;

    // go
    viewer.launch();

    return 0;
}
