//
// Base class for solvers
// Created by Yajie Yan on 4/25/18.
//

#include "SolverBase.h"
#include "SolverUtil.h"
#include <igl/vertex_triangle_adjacency.h>

namespace injmap
{
    SolverBase::SolverBase()
    {

    }

    SolverBase::~SolverBase()
    {

    }

    void SolverBase::initSolver( SolverData *_data )
    {
        m_data = _data;
        m_step = m_data->out_m->V3d();
        m_step.setZero();
        m_grad = m_data->out_m->V3d();
        m_grad.setZero();
        m_p = m_data->out_m->V3d();
        m_p.setZero();
    }

    void SolverBase::setIterMode( IterMode _itermode )
    {
        m_iter_mode = _itermode;
    }

    IterMode SolverBase::getIterMode() const
    {
        return m_iter_mode;
    }

    void SolverBase::setViewer( igl::opengl::glfw::Viewer &_viewer )
    {
        m_viewer_ptr = &_viewer;
    }

    void SolverBase::initViewer()
    {

    }

    void SolverBase::getX( Eigen::MatrixXd &_V ) const
    {
        _V = m_data->out_m->V3d();
    }

    void SolverBase::getTempOutX( Eigen::MatrixXd &_V ) const
    {
        _V = m_data->Xnext;
    }

    void SolverBase::getPerVertStep( Eigen::MatrixXd &_dir ) const
    {
        _dir = m_step;
    }

    void SolverBase::getPerVertGrad( MatrixXd &_grad ) const
    {
        _grad = m_grad;
        if ( _grad.cols() == 2 )
            minimesh::augmentVtsForRender( _grad );
    }

    void SolverBase::getLinesearch( MatrixXd &_dir ) const
    {
        _dir = m_p;
    }

    void SolverBase::getFlip( Eigen::VectorXd &_flip ) const
    {
        _flip = m_data->flip;
    }

    /**
     * private helpers
     */

    void SolverBase::init_helper()
    {

    }
}