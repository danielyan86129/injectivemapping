//
// Relax solver using gradient descent preconditioned by the hessian of the objective function
// Created by Yajie Yan on 4/29/18.
//

#include <igl/volume.h>
#include <igl/doublearea.h>
#include <igl/setunion.h>
#include <igl/slice.h>
#include <igl/slice_into.h>
#include <igl/flip_avoiding_line_search.h>
#include <igl/slice_mask.h>
#include <igl/is_border_vertex.h>
#include "relaxer_precond_gd.h"

namespace injmap
{
    RelaxerPrecondGD::RelaxerPrecondGD()
    {

    }

    RelaxerPrecondGD::~RelaxerPrecondGD()
    {

    }

    void RelaxerPrecondGD::initSolver( injmap::SolverData *_data )
    {
        SolverBase::initSolver( _data );

        auto d = m_data->out_m->d();
        auto m = m_data->out_m->m();
        auto n = m_data->out_m->n();
        // init solver for TT
        m_TT_solver.compute( m_data->TT );
        // init flipness, good faces, free vertices, hdls, gradient, per-simplex metric
        m_pre_flip = VectorXd::Zero( 0 );
        m_hdls = *m_data->hdls;
        m_fix_bad_bndry = true;
        m_good_Fi = VectorXi::LinSpaced( m, 0, m - 1 );
        m_free_ids = VectorXi::LinSpaced( n, 0, n - 1 );
        if ( d == 2 )
        {
            igl::doublearea( m_data->init_m->V3d(), m_data->init_m->F(), m_simp_metric );
            m_simp_metric *= 0.5;
        }
        else // d == 3
        {
            igl::volume( m_data->init_m->V3d(), m_data->init_m->F(), m_simp_metric );
        }
    }

    SolverStatus RelaxerPrecondGD::state() const
    {
        double smallnum = m_data->init_m->scale() * 1e-3;
        if ( Base::m_step.norm() < smallnum ) // tiny step means convergence
            return SolverStatus::CONVERGED;
        return SolverStatus::CONTINUE;
    }

    SolverStatus RelaxerPrecondGD::iterate( int _iter )
    {
        return SUCCESS;
    }

    SolverStatus RelaxerPrecondGD::computeStep( int _iter )
    {
        /// initialize info
        // for readability
        auto out_m = Base::m_data->out_m;
        const auto &orig_hdls = *Base::m_data->hdls;
        auto &flip = Base::m_data->flip;
        auto d = out_m->d();

        // init prev & next X to the output before calling this solver
        const MatrixXd &Xprev = out_m->V3d();
        Base::m_step.setZero();
        Base::m_p.setZero();
        Base::m_grad.setZero();

        /// skip if no good faces to relax
        if ( !(flip.array() == 0.).any() )
            return SKIPPED;

        /// recompute handles, grad structure, and rebuild linear system if flipness change
        //bool flip_changed = isFlipChanged( m_pre_flip, flip );
        bool flip_changed = true; // force recompute hdls and linear system

        if ( flip_changed )
        {
            m_pre_flip = flip;
            // update face ids
            VectorXi bad_Fi;
            SolverUtil::findIds( flip.array() == 0., m_good_Fi, bad_Fi );
            // update handles: orig + bad faces V ids or non-boundary V ids
            VectorXi tmp1, tmp2;
            MatrixXi bad_F = igl::slice( out_m->F(), bad_Fi, 1 );
            VectorXi bad_Vi;
            if ( !m_fix_bad_bndry ) // allow bndry of bad region move but fix rest bad V
                SolverUtil::getNonBoundaryVids( Xprev, bad_F, bad_Vi );
            else // fix all V in bad region
                igl::setunion( util::vec( bad_F ), util::vec( bad_F ), bad_Vi, tmp1, tmp2 );
            igl::setunion( bad_Vi, orig_hdls, m_hdls, tmp1, tmp2 );
            // update free vts ids
            SolverUtil::setMinus( out_m->n(), m_hdls, m_free_ids );
            // skip if no dof left
            if ( m_free_ids.size() == 0 )
                return SKIPPED;
            std::cout << "relax: fixed V ids (" << m_hdls.size() << "): " << Eigen::RowVectorXi( m_hdls ) << std::endl;
        }

        // re-build linear system
        if ( flip_changed )
        {
            // want dir components to be 0 for handles
            auto tgt = Eigen::RowVector3d::Zero();
            build_system( m_data->TT, m_free_ids, m_hdls );
        }

        /// compute gradient (right-hand side of system)
        compute_gradient( Xprev, m_data->T, m_good_Fi, m_simp_metric, Base::m_grad );
        //std::cout << "grad = " << m_grad << std::endl;
        printf( "Done: |gradient| = %f\n", m_grad.norm() );

        /// back solve for search directions
        SolverStatus err = solve_system( -m_grad.leftCols( 2 ), Eigen::RowVector3d( 0., 0., 0. ), m_free_ids, m_hdls,
                                         Base::m_p );
        if ( err != SUCCESS )
            return err;
        //std::cout << "p = " << Base::step << std::endl;
        printf( "Done: |p| = %f\n", Base::m_p.norm() );

        /// flip-avoiding line search
        auto energy_func = [&]( MatrixXd _X_new ) -> double
        {
            // only compute energy for good faces
            return compute_energy( _X_new, m_data->T, m_good_Fi, m_simp_metric );
        };
        MatrixXi m_good_F = igl::slice( out_m->F(), m_good_Fi, 1 );
        double old_E = 0., new_E = 0.;
        old_E = energy_func( Xprev );
        MatrixXd Xcur, Xnext;
        if ( d == 2 )
        {
            Xcur = Xprev.leftCols( 2 );
            Xnext = Xcur + Base::m_p.leftCols( 2 );
            new_E = igl::flip_avoiding_line_search( m_good_F, Xcur, Xnext, energy_func );
            minimesh::augmentVtsForRender( Xcur );
        }
        else
        {
            Xcur = Xprev;
            Xnext = Xcur + Base::m_p;
            new_E = igl::flip_avoiding_line_search( m_good_F, Xcur, Xnext, energy_func );
        }
        printf( "old/new E = %f/%f\n", old_E, new_E );
        if ( new_E > old_E )
        {
            printf( "Line search fails\n" );
            return LS_FAIL;
        }
        m_data->Xnext = Xcur;

        // final step
        Base::m_step = m_data->Xnext - Xprev;

        return SUCCESS;
    }

    SolverStatus RelaxerPrecondGD::takeStep( int _iter )
    {
        double tmp_diff = (m_data->Xnext - m_data->out_m->V3d()).norm();
        printf( "Debug: tmp_diff = %f\n", tmp_diff );
        m_data->out_m->setV( m_data->Xnext );
        // update flip
        SolverUtil::detFlips( m_data->out_m->d(), m_data->out_m->m(), m_data->T, m_data->out_m->V3d(), 0.,
                              m_data->flip );
        return SUCCESS;
    }

    void RelaxerPrecondGD::setFixBadBoundary( bool _fix )
    {
        m_fix_bad_bndry = _fix;
    }

    /*
     * helpers
     */
    bool RelaxerPrecondGD::isFlipChanged( const VectorXd &_pre_flip, const VectorXd &_cur_flip ) const
    {
        int n_flip = (_cur_flip.array() > 0.).count();
        int pre_n_flip = (_pre_flip.array() > 0.).count();
        return n_flip != pre_n_flip;
    }

    void RelaxerPrecondGD::build_system( const SparseMatrixXd &_TT,
                                         const VectorXi &_free_Vi, const VectorXi &_fixed_Vi )
    {
        // init info
        auto d = m_data->out_m->d();
        auto n = m_data->out_m->n();
        // convert free & fixed to list of indices into _TT
        VectorXi free_idxs( d * _free_Vi.size() ), fixed_idxs( d * _fixed_Vi.size() );
        for ( auto i = 0; i < _free_Vi.size(); ++i )
        {
            int vi = _free_Vi( i );
            VectorXi vi_ids = util::idsOfVertexInX( vi, d, n );
            free_idxs.block( i * vi_ids.size(), 0, vi_ids.size(), 1 ) = vi_ids;
        }
        for ( auto i = 0; i < _fixed_Vi.size(); ++i )
        {
            int vi = _fixed_Vi( i );
            VectorXi vi_ids = util::idsOfVertexInX( vi, d, n );
            fixed_idxs.block( i * vi_ids.size(), 0, vi_ids.size(), 1 ) = vi_ids;
        }
        // build linear system for free vars only
        igl::slice( _TT, free_idxs, free_idxs, m_TT_ff );
        igl::slice( _TT, free_idxs, fixed_idxs, m_TT_fc );
        m_TT_solver.compute( m_TT_ff );
    }

    SolverStatus
    RelaxerPrecondGD::solve_system( const MatrixXd &_b, const MatrixXd &_tgt,
                                    const VectorXi &_free_ids, const VectorXi &_fixed_ids,
                                    MatrixXd &_dir )
    {
        auto d = m_data->out_m->d();

        MatrixXd X_c = _tgt;
        if ( _tgt.cols() > d )
            X_c = X_c.leftCols( 2 );
        if ( _tgt.rows() == 1 && _fixed_ids.size() > 1 )
        {
            X_c = X_c.replicate( _fixed_ids.size(), 1 );
        }
        VectorXd b_f = util::vec( igl::slice( _b, _free_ids, 1 ) );
        b_f += m_TT_fc * util::vec( X_c );
        VectorXd p_f = m_TT_solver.solve( b_f );
        if ( m_TT_solver.info() != Eigen::Success )
            return LINSOLVE_FAIL;
        igl::slice_into( util::unvec( p_f, d ), _free_ids, Vector2i::LinSpaced( d, 0, d - 1 ), _dir );

        return SUCCESS;
    }

    double RelaxerPrecondGD::compute_energy( const MatrixXd &_X_new, const SparseMatrixXd &_T, const VectorXi &_FI,
                                             const VectorXd &_FW ) const
    {
        int d = m_data->out_m->d();
        double E = 0.;
        if ( d < _X_new.cols() )
        {
            VectorXd X_new = util::vec( (MatrixXd) _X_new.leftCols( d ) );
            E = m_SD_energy.eval( d, X_new, _T, _FI, _FW );
        }
        else
            E = m_SD_energy.eval( d, util::vec( _X_new ), _T, _FI, _FW );
        return E;
    }

    void RelaxerPrecondGD::compute_gradient( const MatrixXd &_X_new, const SparseMatrixXd &_T, const VectorXi &_FI,
                                             const VectorXd &_FW,
                                             MatrixXd &_grad ) const
    {
        auto d = m_data->out_m->d();
        VectorXd grad = util::vec( (MatrixXd) _grad.leftCols( d ) );
        if ( d < _X_new.cols() )
        {
            VectorXd X_new = util::vec( (MatrixXd) _X_new.leftCols( d ) );
            m_SD_energy.grad( _T, _T * X_new, _FW, m_data->out_m->F(), _FI, grad );
        }
        else
            m_SD_energy.grad( _T, _T * util::vec( _X_new ), _FW, m_data->out_m->F(), _FI, grad );
        _grad = util::unvec( grad, d );
        if ( d == 2 )
            minimesh::augmentVtsForRender( _grad );
    }

}