//
// Created by Yajie Yan on 8/14/17.
//

#include "energy_func.h"

namespace injmap
{
    typedef SettingEnum SE; // enum of settings

    injmap::EnergyFunction::EnergyFunction()
    {

    }

    injmap::EnergyLSQ::EnergyLSQ()
    {

    }

    void EnergyFunction::gradMapT( const SparseMatrix<double> &_T, int _t, const VectorXi &_simp_t,
                                   MatrixXd &_gr_Jt, VectorXi &_pos ) const
    {
        auto d = _simp_t.size() - 1;
        auto n = _T.cols() / d; // _T is of size d^2 * dn
        Vector2i rrange = util::rowrangeOfSimp( _t, d );
        for ( auto i = 0; i < _simp_t.size(); ++i )
        {
            auto vi = _simp_t( i );
            auto vi_ids = util::idsOfVertexInX( vi, d, n );

            for ( auto ii = 0; ii < vi_ids.size(); ++ii )
            {
                _pos( i * d + ii ) = vi_ids( ii );
                _gr_Jt.col( i * d + ii ) =
                        _T.col( vi_ids( ii ) ).segment( rrange( 0 ), rrange( 1 ) - rrange( 0 ) + 1 );
            }
        }
    }

    injmap::EnergyLSQ::EnergyLSQ( const injmap::Settings *_s )
    {
        this->setSettings( _s );
    }

    double injmap::EnergyLSQ::eval( const Eigen::VectorXd &_TX, const Eigen::VectorXd &_TXr,
                                    const VectorXd &_areas ) const
    {
        int d = m_s->at( SE::DIM );
        int m = m_s->at( SE::M );
        double lsq = 0.0;
//        cout << "lsq-t = ";
        for ( auto i = 0; i < m; ++i )
        {
            auto lsq_t = (util::getMap( _TX, i, d ) - util::getMap( _TXr, i, d )).squaredNorm() * _areas( i );
            lsq += lsq_t;
//            cout << lsq_t << " ";
        }
//        cout << endl;
        lsq *= 0.5;
        return lsq;
    }

    Eigen::VectorXd injmap::EnergyLSQ::grad( const Eigen::SparseMatrix<double> &_T,
                                             const Eigen::VectorXd &_TX, const Eigen::VectorXd &_TXr,
                                             const VectorXd &_areas, const Eigen::MatrixXi &_simplices ) const
    {
        int d = m_s->at( SE::DIM );
        int n = m_s->at( SE::N );
        int m = m_s->at( SE::M );
        int sd = m_s->at( SE::SDIM );
        VectorXd glsq = VectorXd::Zero( d * n );
        MatrixXd gr_Jt = MatrixXd::Zero( d * d, d * sd );
        VectorXi pos = VectorXi::Zero( gr_Jt.cols() );
        VectorXd TX_TXr = _TX - _TXr;
        //std::cout << "TX-TXr= " << std::endl << RowVectorXd( TX_TXr ) << std::endl;

        for ( auto i = 0; i < m; ++i )
        {
            auto a = _areas( i );
            VectorXd diff = util::vec( util::getMap( TX_TXr, i, d ) );
            //cout << "lsq: TX_t - TXr_t = \n" << RowVectorXd( diff ) << endl;
            gradMapT( _T, i, _simplices.row( i ), gr_Jt, pos );
//            cout << "write-to-pos = " << RowVectorXi( pos ) << endl;
            for ( auto j = 0; j < pos.size(); ++j )
            {
                //cout << "gr-Jt row = " << gr_Jt.transpose().row( j ) << endl;
                double rh = a * gr_Jt.transpose().row( j ) * diff;
                glsq( pos( j ) ) += rh;
                //cout << "glsq[pos[i]] = " << rh << endl;
            }

            //std::cout << "gr_Jt= " << std::endl << gr_Jt << std::endl;
            //std::cout << "cur grd-lsq= " << std::endl << glsq << std::endl;
        }
        return glsq;
    }

/*****************************
/Impl of class EnergyARAP
******************************/
    EnergyARAP::EnergyARAP()
    {

    }

    EnergyARAP::EnergyARAP( const Settings *_s )
    {
        this->setSettings( _s );
    }

    double
    EnergyARAP::eval( const Eigen::VectorXd &_TX, const Eigen::MatrixXd &_rots, const Eigen::VectorXd &_areas ) const
    {
        int d = m_s->at( SE::DIM );
        int m = m_s->at( SE::M );
        auto e = 0.0;
        VectorXd diff_TX_R = _TX - util::vec( _rots );
        for ( auto i = 0; i < m; ++i )
        {
            e += util::getMap( diff_TX_R, i, d ).squaredNorm() * _areas( i );
        }
        e *= 0.5;
        return e;
    }

    Eigen::VectorXd
    EnergyARAP::grad( const Eigen::VectorXd &_TX, const Eigen::SparseMatrix<double> &_T,
                      const Eigen::MatrixXd &_rots, const Eigen::VectorXd &_areas,
                      const Eigen::MatrixXi &_simplices ) const
    {
        int d = m_s->at( SE::DIM );
        int n = m_s->at( SE::N );
        int m = m_s->at( SE::M );
        int sd = m_s->at( SE::SDIM );
        VectorXd grd = VectorXd::Zero( d * n );
        VectorXd diff_TX_R = _TX - util::vec( _rots );
        MatrixXd gr_Jt = MatrixXd::Zero( d * d, d * sd );
        VectorXi pos = VectorXi::Zero( gr_Jt.cols() );

        for ( auto i = 0; i < m; ++i )
        {
            auto a = _areas( i );
            auto vec_diff = util::vec( util::getMap( diff_TX_R, i, d ) );
            /*cout << "vec_diff = \n"<<vec_diff<<endl;*/
            gradMapT( _T, i, _simplices.row( i ), gr_Jt, pos );
            for ( auto j = 0; j < pos.size(); ++j )
            {
                grd( pos( j ) ) += a * gr_Jt.transpose().row( j ) * vec_diff;
            }
        }
        return grd;
    }

}