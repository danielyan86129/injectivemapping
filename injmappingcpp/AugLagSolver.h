//
// Created by Yajie Yan on 8/15/17.
// Interface of the augmented lagrangian solver
//

#ifndef INJECTIVEMAPPING_AUGLAGRANGIAN_H
#define INJECTIVEMAPPING_AUGLAGRANGIAN_H

#include <igl/opengl/glfw/Viewer.h>
#include "opt_utility.h"
#include "energy_func.h"
#include "auglag_constraint.h"
#include "debug.h"

namespace injmap
{
    using ::util::minimesh;

    /*! TODO: put this in cpp
     * The input/output structure of an iteration
     */
    struct iterinout
    {
        iterinout( int _d, int _n, int _m )
        {
            ls_succ = true;
            // scalars
            i = 0;
            step = 0.0;
            F_val = 0.0;
            // init per-vertex attr to 0
            X = VectorXd( _d * _n );
            p = X;
            // init per-simplex attr to 0
            areas = VectorXd::Zero( _m );
            k_D = lmd_D = areas;
            k_B = lmd_B = areas;
            flips = VectorXd::Zero( _m );
            deforms = flips;
            svds.resize( _m );
            // init maps to identities
            TX1 = VectorXd::Zero( _d * _d * _m );
            for ( auto i = 0; i < _m; ++i )
            {
                Map<MatrixXd>( TX1.data() + i * _d * _d, _d, _d ).setIdentity();
            }
            TXr = TX1;
            rots = util::unvec( TX1, _m );
        }

        iterinout()
        {}

        bool ls_succ; // did line search succeed?
        int i; // which iteration is this?
        VectorXd X; // cur positions
        VectorXd p; // search direction
        double step; // step taken along direction
        double F_val; // value of objective after this iteration
        VectorXd TX1, TXr; // the first and reference maps
        VectorXd areas;
        std::vector<util::svdstruct> svds;
        MatrixXd rots;
        VectorXd k_D;
        VectorXd lmd_D;
        VectorXd k_B;
        VectorXd lmd_B;
        VectorXd flips;
        VectorXd deforms;
        // TODO: finish this struct...
    };

    /*!
     * The augmented lagrangian solver class
     * (managing the settings by itself)
     */
    class AugLagSolver
    {
    public:
        AugLagSolver();

        ~AugLagSolver();

        explicit AugLagSolver( const Settings& _s );

        //
        /// @\Brief set the folder used for multi-purposes from the given file name
        void setFolder(const string& _folder);
        /*!
         * initialize the solver
         * @param _restmesh the rest shape
         * @param _initmesh simply moves the handles of rest shape to target positions
         * @param _hdls a list of indices of the vertices to move
         * @return
         */
        bool initialize( const minimesh& _restmesh, const minimesh& _initmesh, const VectorXi& _hdls );
        ///
        /// @Brief finish left-over from an optimization session, e.g. dump files, cleaning up, etc.
        bool finish();
        ///
        /// run the optimization
        /// \return successful or not
        bool run();

        void getResultForRender( MatrixXd& _V3d ) const;

        /// possible verdict of current iteration result
        enum SolverStatus
        {
            CAN_CONTINUE, CONVERGED, FAIL_TO_CONVERGE
        };

        ///
        /// \return current status of the solver based on internal iteration results
        /// caller can determine whether to stop solving or not
        SolverStatus solverStatus() const;

        ///
        /// compute one (regular / aARAP) iteration
        /// \param _inout   contains input parameters & overwritten with results of cur iteration.
        /// \return         successful or not
        bool iterate( iterinout*& _inout );

        /// @\Brief carry out a regular iteration (optimize all terms enabled through settings)
        bool iterateRegular( const iterinout& _in, iterinout& _out,
                             bool _update_D = false, bool _update_B = false );

        ///
        /// @Brief carry out an aARAP iteration (without enabling "aARAP" through settings)
        bool iterateAARAP( const iterinout& _in, iterinout& _out );

        ///
        /// @Brief Performs next iteration based on current iter output
        /// and uploads next-iter result to the viewer for visualization
        /// @result cur-iter output will be updated accordingly as well
        bool iterateWithVis( igl::opengl::glfw::Viewer& _igl_viewer );

        ///
        /// @Brief determine the color for each edge based on it's nb simplices'
        /// flip-ness & deformation status
        /// \param [in] _E      given set of edges (#edges by 2)
        /// \param [in] _mesh   the mesh where the edges are from (e.g. rest pose mesh if connectivity doesn't change)
        /// \param [in] _flips
        /// \param [in] _deforms    current flip and deformation status per simplex
        /// \param [out] _color #E by 3 colors
        void detEdgeColor( const minimesh& _mesh,
                           const VectorXd& _flips, const VectorXd& _deforms,
                           MatrixXd& _color ) const;

        ///
        /// @Brief color edges based on given iteration result
        void detEdgeColor( MatrixXd& _E_color ) const;

    protected:

        ///
        /// enforce handles by modifying the given hess matrix
        /// \param _TT      the hess matrix to enforce constraints
        /// \param _hdls    the handles to enforce
        void constrain_H( SparseMatrix<double>& _TT, const VectorXi& _hdls ) const;

        ///
        /// modify the right-hand side of the system TT*p = rh to enforce the handles
        /// need to call this whenever rh changes
        /// \param _rh      the right-hand side gradient
        /// \param _hdls    the handles to enforce
        void constrain_rh( VectorXd& _rh, const VectorXi& _hdls ) const;

        ///
        /// evaluate the objective function
        /// \param [out] _breakdown if a 4d vector is given, fill it with 4 components: lsq, arap, D, BD
        double eval_F( const VectorXd& _TX, const VectorXd& _TXr,
                       const VectorXd& _k_D, const VectorXd& _lmds_D,
                       const VectorXd& _k_B, const VectorXd& _lmds_B, double _BDK,
                       const std::vector<util::svdstruct>& _svds, const MatrixXd& _rots,
                       const VectorXd& _metric,
                       Vector4d* _breakdown ) const;

        /*!
         * The structure returned from a line search routine
         */
        struct LSret
        {
            bool suc;       // success?
            double step;    // step to take
            int i;          // # iter took to finish
            double new_F;   // objective value at finish
        };

        /*!
         * Perform BACK-TRACKING line-search (starting at X along direction p)
         * @param _X        starting point
         * @param _p        search direction
         * @param _F        cur objective at X
         * @param _grd_F    gradient of F at X
         * @param _T
         * @param _TXr      the reference maps (e.g. initial, last-ter, etc)
         * @param _k_D
         * @param _lmds_D
         * @param _k_B
         * @param _lmds_B
         * @param _BDK
         * @param _svds
         * @param _rots
         * @param _areas    area of simplex in rest mesh
         * @param _c        (line-search) sufficient decrease ratio
         * @param _b        (line-search) contractive rate
         * @return          a struct indicating any success and associated info
         */
        LSret linesearch( const VectorXd& _X, const VectorXd& _p, double _F, const VectorXd& _grd_F,
                          const VectorXd& _TXr,
                          const VectorXd& _k_D, const VectorXd& _lmds_D,
                          const VectorXd& _k_B, const VectorXd& _lmds_B, double _BDK,
                          const std::vector<util::svdstruct>& _svds, const MatrixXd& _rots,
                          const VectorXd& _areas,
                          double _c = 0.001, double _b = 0.5 );

        /*!
         * update the multipliers for some hard constraints
         * @param _k    the penalty coefficient
         * @param _g    values of the constraint
         * @param _lmds lagrangian multipliers
         * @return      the updated lambdas
         */
        VectorXd update_lambdas( const VectorXd& _k, const VectorXd& _g, const VectorXd& _lmds ) const;

        /*!
         * update the gradient for the constraint part of the objective function for a single simplex t
         */
        VectorXd update_G_t( double _kt, double _lmd_t, double _g_t, const VectorXd& _gradg_t ) const;

        /*!
         * Compute the gradient of the term G-hat corresponding to detangler constraints
         * @param _k        the penalty coef for D constraints
         * @param _lmds     lagrangian multipliers for D constraints
         * @param _T        the linear operator that maps X to its deformation
         * @param _TX       the map jacobians
         * @param _simps    the faces of (current-iter) mesh
         * @return          vector G
         */
        VectorXd evalG_for_D( const VectorXd& _k, const VectorXd& _lmds,
                              const VectorXd& _TX, const MatrixXi& _simps ) const;

        /*!
         * Compute the gradient of the term G-hat corresponding to bounded-distortion constraints
         * @param _svds     svd for each face of mesh (after an iteration)
         * @param _BDK      BD bound constant K
         * @param _k        the penalty coef for BD constraints
         * @param _lmds     lagrangian multipliers for BD constraints
         * @param _T        the linear operator that maps X to its deformation
         * @param _TX       the maps' jacobians
         * @param _simps    the simplices of the mesh
         * @return
         */
        VectorXd evalG_for_B( const std::vector<util::svdstruct>& _svds, double _BDK,
                              const VectorXd& _k, const VectorXd& _lmds,
                              const VectorXd& _TX, const MatrixXi& _simps ) const;

        ///
        /// \brief compute the total gradient of objective function
        VectorXd grad_F( const VectorXd& _TX, const VectorXd& _TXr,
                         const std::vector<util::svdstruct>& _svds, const MatrixXd& _rots,
                         const VectorXd& _k_D, const VectorXd& _lmds_D,
                         const VectorXd& _k_B, const VectorXd& _lmds_B,
                         const VectorXd& _metric,
                         const MatrixXi& _simps ) const;

        ///
        /// \brief initialize multipliers for D constraints
        void init_lmd_D( const VectorXd& _TX, VectorXd& _lmds ) const;

        ///
        /// \brief initialize multipliers for B constraints
        void init_lmd_B( const std::vector<util::svdstruct>& _svds, VectorXd& _lmds ) const;

        ///
        /// @Brief determine flip-ness
        void det_flips( const VectorXd& _TX, VectorXd& _flip_flag ) const;

        ///
        /// @Brief determine deformation (worse than k-bound)
        void det_deform( const std::vector<util::svdstruct>& _svds, double _BDK, VectorXd& _deforms ) const;

    protected:
        // the settings specific to this solver
        // need to re-initialize solver once this is changed to different settings
        Settings m_s;

        // initialized or not?
        bool m_initialized;
        ///
        /// Once initialized, below states remain unchanged
        ///
        // the meshes to optimize
        minimesh m_restM, m_initM;
        VectorXi m_hdls;
        string m_initM_name, m_folder_name;
        // energies
        EnergyLSQ m_lsq;
        EnergyARAP m_arap;
        // constraints
        Detangler m_D;
        BD m_BD;
        // A linear operator mapping X to deformation maps
        SparseMatrix<double> m_T;
        // the hessian
        SparseMatrix<double> m_TT;
        // solver for: TT*p = -gradient (solving for m_p)
        Eigen::SimplicialLDLT<decltype( m_TT )> m_TT_solver;
        //Eigen::SparseLU<decltype( m_TT )> m_TT_solver;
        ///
        /// following states likely to change between iterations
        // cur/next iteration result
        iterinout* m_cur_iter;
        iterinout* m_next_iter;
        // debug related
        dbg::iterinfodbg m_dbg_dump;
    };
}


#endif //INJECTIVEMAPPING_AUGLAGRANGIAN_H
