//
// importer of mesh data (2d and 3d)
// Created by Yajie Yan on 4/14/18.
//

#ifndef INJECTIVEMAPPING_IMPORT_H
#define INJECTIVEMAPPING_IMPORT_H

#include <string>
#include "utility.h"
#include "geom_utility.h"

namespace util
{
    using std::string;
    class Importer
    {
    public:
        /* read a 2d mesh file */
        static bool importMesh2D( const string& _filename, minimesh& _tri_mesh );
        /* read a set of data: rest mesh, init mesh, and handles */
        static bool importMeshData( const string& _rest_file, const string& _init_file, const string& _hdl_file,
                             minimesh& _rest_m, minimesh& _init_m, VectorXi& _hdls );
        static bool importTetData( const string& _filebase, minimesh& _rest, minimesh& _init, MatrixXi& _BF, MatrixXi& _hdls );
    };
}

#endif //INJECTIVEMAPPING_IMPORT_H
