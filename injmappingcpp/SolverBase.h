//
// Base class for solvers
// Created by Yajie Yan on 4/25/18.
//

#ifndef INJECTIVEMAPPING_SOLVERBASE_H
#define INJECTIVEMAPPING_SOLVERBASE_H

#include "utility.h"
#include "geom_utility.h"
#include "opt_utility.h"
#include "SolverUtil.h"
#include <igl/opengl/glfw/Viewer.h>
#include <vector>

namespace injmap
{
    class SolverBase
    {
    public:
        typedef ::util::minimesh minimesh;

    public:
        /// ctor & de-ctor
        SolverBase();
        virtual ~SolverBase();

        /// @Brief initialize solver (data must be properly initialized)
        virtual void initSolver( SolverData *_data );

        /// set / get iteration mode
        virtual void setIterMode( IterMode _itermode );
        virtual IterMode getIterMode() const;

        /// query state of solver from just finished iteration
        virtual SolverStatus state() const = 0;

        /// assign viewer
        virtual void setViewer( igl::opengl::glfw::Viewer &_viewer );

        /// init viewer by uploading geometry
        virtual void initViewer();

        /// run one iteration of solver. output: new positions X_out
        virtual SolverStatus iterate( int _iter ) = 0;
        virtual SolverStatus computeStep( int _iter ) = 0;
        virtual SolverStatus takeStep( int _iter ) = 0;

        /// return output vertices positions
        virtual void getX( MatrixXd &_V ) const;

        /// return temp output vertices positions
        virtual void getTempOutX( MatrixXd &_V ) const;

        /// return current per-vertex step (gradient scaled by linesearch step)
        virtual void getPerVertStep( MatrixXd &_dir ) const;

        /// return per-vertex gradient
        virtual void getPerVertGrad( MatrixXd &_grad ) const;

        /// return per-vertex search direction
        virtual void getLinesearch( MatrixXd &_dir ) const;

        /// return flipness
        virtual void getFlip( VectorXd &_flip ) const;

    private:
        /**
         * SolverBase specific helpers
         */

        void init_helper();
    protected:
        /**
         * protected data memebers
         */

        /// ref to external stuff
        igl::opengl::glfw::Viewer *m_viewer_ptr;
        /// solver settings
        IterMode m_iter_mode;
        /// the opt data
        SolverData *m_data;
        MatrixXd m_step; // per vertex step (direction scaled)
        MatrixXd m_grad; // per vertex gradient (of energy)
        MatrixXd m_p; // search direction (might be different than gradient)

        /*SparseMatrixXd *m_T_ptr; // V to per-simplex-map operator
        minimesh *m_out_m;
        std::vector<std::vector<int>> m_V_F_adj; // nb faces for each v
        std::vector<std::vector<int>> m_V_i_in_adjF; // [i,j]: for vi, the idx of vi within nb face j
        const minimesh *m_rest_m, *m_init_m;
        const MatrixXi *m_hdls;
        VectorXd m_flip; // flip > 0.0, no-flip = 0.0
        MatrixXd m_V_next;*/
    };
}


#endif //INJECTIVEMAPPING_SOLVERBASE_H
