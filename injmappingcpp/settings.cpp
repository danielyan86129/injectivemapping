//
// Created by Yajie Yan on 8/21/17.
//

#include <fstream>
#include "settings.h"
#include "logging.h"

namespace injmap
{
    typedef SettingEnum SE;
    using std::ifstream;

    static bool s_map_initialized = false;
    static unordered_map<string, SE> s_map; // map: setting name -> its enum value
    void build_setting_strings()
    {
        s_map["AARAP"] = AARAP;
        s_map["MAX_ITER"] = MAX_ITER;
        s_map["VERBOSE"] = VERBOSE;
        s_map["W_LSQ"] = W_LSQ;
        s_map["W_ARAP"] = W_ARAP;
        s_map["W_D"] = W_D;
        s_map["W_B"] = W_B;
        s_map["LS_SCHEME"] = LS_SCHEME;
        s_map["ADA_k"] = ADA_k;
        s_map["kD"] = kD;
        s_map["kB"] = kB;
        s_map["lmdD"] = lmdD;
        s_map["lmdB"] = lmdB;
        s_map["BDBND"] = BDBND;
        s_map["REF_MAP"] = REF_MAP;
        s_map["DIM"] = DIM;
        s_map["SDIM"] = SDIM;
        s_map["N"] = N;
        s_map["M"] = M;
        s_map["AREA_WTD"] = AREA_WTD;
        s_map["DBG_DUMP"] = DBG_DUMP;
        s_map_initialized = true;
    }

    Settings defaultSettings()
    {
        if (!s_map_initialized)
            build_setting_strings();
        Settings s;
        s[AARAP] = AArapVal::NO;
        s[MAX_ITER] = 1000;
        s[VERBOSE] = VerboseVal::YES;
        s[W_LSQ] = 0.5;
        s[W_ARAP] = 0.0;
        s[W_D] = 1.0;
        s[W_B] = 0.0;
        s[LS_SCHEME] = LSSchemeVal::BT;
        s[ADA_k] = AdaKVal::NO;
        s[kD] = 1.0;
        s[kB] = 1.0;
        s[lmdD] = 1.0;
        s[lmdB] = 1.0;
        s[BDBND] = 50.0;
        s[REF_MAP] = RefMapVal::Xi;
        s[DIM] = s[SDIM] = s[N] = s[M] = 0.0;
        s[AREA_WTD] = AreaWtdVal::YES;
        s[DBG_DUMP] = DBGDumpVal::NO;
        return s;
    }

    Settings defaultSettings(const Settings &_keyvals)
    {
        auto s = injmap::defaultSettings();
        for (auto it : _keyvals)
            s[it.first] = it.second;
        return s;
    }

    void changeSettings(Settings &_settings, const Settings &_keyvals)
    {

    }

    int next_non_space(std::istream &_stream)
    {
        int c;
        do
            c = _stream.get();
        while (std::isspace(c));
        _stream.unget();
        return c;
    }

    Settings readSettings(const string &_setting_file)
    {
        auto s = defaultSettings();
        ifstream file_in(_setting_file);
        if (!file_in) {
            logging::Logger::err("Cannot open setting file (returning default settings): \n" + _setting_file);
            return s;
        }

        string cmt;
        string name = "";
        double val = -1;
        int c;
        char ln[1024];
        int len = 1024;
        while (c = next_non_space(file_in), c != std::char_traits<char>::eof()) {
            if (c == '#')// skip comment
                file_in.getline(ln, len);
            else if (std::isalpha(c))// read in name-value pair
            {
                file_in >> name >> val;
                if (s_map.find(name) == s_map.end())
                    logging::Logger::err("unknown setting: \"" + name + "\"");
                else
                    s[s_map.at(name)] = val;
            } else {
                file_in.getline(ln, len);
                logging::Logger::fatal("Bad format! reading line: " + string(ln));
            }
        }

        return s;
    }
}