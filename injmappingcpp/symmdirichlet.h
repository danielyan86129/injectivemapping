/***********************************
 * Symmetric Dirichlet energy
** Created by Yajie Yan on 4/30/18.
***********************************/

#ifndef INJECTIVEMAPPING_SYMMDIRICHLET_H
#define INJECTIVEMAPPING_SYMMDIRICHLET_H

#include "energy_func.h"

namespace injmap
{
    class SymmDirichletEnergy : public EnergyFunction
    {
    public:
        SymmDirichletEnergy();
        ~SymmDirichletEnergy() = default;
        explicit SymmDirichletEnergy( const Settings *_s );

        /// evaluate symmetric dirichlet energy
        double eval( int _d, const MatrixXd &_X_new, const SparseMatrixXd &_T, const VectorXi &_Fi,
                     const VectorXd &_FW ) const;
        /// @Brief compute gradient of symmetric dirichlet energy for all vts from contribution of given simplices
        /// @param _W: weight for each face (e.g. area/vol)
        /// @param _simplices: matrix of all faces
        /// @param _SI : list of face ids. only they will contribute to final per-vert gradient
        void grad( const Eigen::SparseMatrix<double> &_T, const Eigen::VectorXd &_TX,
                   const Eigen::VectorXd &_W,
                   const Eigen::MatrixXi &_simplices, const VectorXi &_SI,
                   VectorXd &_grad ) const;
        VectorXd grad( const SparseMatrix<double> &_T, const VectorXd &_TX, const VectorXd &_metrics,
                       const MatrixXi &_simplices, const VectorXi _SI ) const;
    };
}

#endif //INJECTIVEMAPPING_SYMMDIRICHLET_H
