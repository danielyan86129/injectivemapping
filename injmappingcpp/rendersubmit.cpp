//
// Utility functions used to interact with libigl's viewer
// Created by Yajie Yan on 4/18/18.
//

#include "rendersubmit.h"

void RenderSubmit::addLines( igl::opengl::glfw::Viewer&_viewer, const MatrixXd &_start, const MatrixXd &_dir,
                             const MatrixXd &_colors )
{
    MatrixXd ends = _start + _dir;
    _viewer.data().add_edges( _start, ends, _colors );
}

void RenderSubmit::setLines( igl::opengl::glfw::Viewer&_viewer, const MatrixXd &_start, const MatrixXd &_dir,
                             const MatrixXd &_colors )
{
    int ne = _start.rows();
    MatrixXd P = _start;
    P.conservativeResize( ne * 2, Eigen::NoChange );
    P.bottomRows( ne ) = _start + _dir;
    MatrixXi E( ne, 2 );
    for ( auto i = 0; i < ne; ++i )
    {
        E( i, 0 ) = i;
        E( i, 1 ) = i + ne;
    }
    _viewer.data().set_edges( P, E, _colors );
}