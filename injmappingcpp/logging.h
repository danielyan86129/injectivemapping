//
// Created by Yajie Yan on 8/18/17.
//

#ifndef INJECTIVEMAPPING_LOGGING_H
#define INJECTIVEMAPPING_LOGGING_H

#include <string>
#include <iostream>

namespace logging
{
    using std::string;
    using std::cout; using std::cerr; using std::endl;

    /*!
     * \brief a simple logging class
     */
    class Logger
    {
        Logger() = delete;
        Logger( const Logger &_l ) = delete;
        ~Logger() = default;
    public:
        ///
        /// \Summary output an error message
        static inline void err( const string &_err_s )
        { cerr << "Error: "<< _err_s << endl; }
        ///
        /// just some information
        static inline void info( const string &_info )
        { cout << _info << endl; }
        ///
        /// \Brief fatal error. will abort program.
        static void fatal( const std::string &_msg )
        {
            cerr << "Fatal error: "<<_msg<<endl;
            exit(-1);
        }
    };
}

#endif //INJECTIVEMAPPING_LOGGING_H
