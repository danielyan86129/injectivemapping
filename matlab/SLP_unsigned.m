% ---- parameters ----
handleFile = 'data/James-test1.MAT';
inFile = 'data/James-test1.obj';
outFile = 'data/James-test1-unsigned.obj';

maxIter = 2;
epsilon = 0.001;
opts = sdpsettings('verbose',0);

% ---- import data ----
handles = load(handleFile);
handles = handles.Expression1;

initMesh = readObj(inFile);
vert = initMesh.v;
face = initMesh.f.v;

nv = length(vert);
nf = length(face);

% ---- variables ----
A = sdpvar(nf, 1);
X = sdpvar(nv, 1);
Y = sdpvar(nv, 1);

% ---- objective ----
objective = sum(A);

% ---- init ----
hdlX = vert(handles, 1);
hdlY = vert(handles, 2);
curX = vert(: , 1);
curY = vert(: , 2);


for iter = 1:maxIter
    fprintf('iter %d\n', iter);
    % ---- X step ----
    hV = [X, curY, ones(nv,1)];

    constraints = [ A >= 0 , X(handles) == hdlX ];

    for i = 1:nf
        constraints = [ constraints, -A(i) <= 0.5 * det(hV(face(i,:),:), 'polynomial') <= A(i) ];
    end

    sol = optimize(constraints, objective, opts);

    curX = value(X);
    
    fprintf('E = %f\n', value(objective));

    % ---- terminiation check ----
    minA = 1000;
    for i = 1:nf
        curA = 0.5 * det(value(hV(face(i,:),:)));
        if curA < minA
            minA = curA;
        end
    end
    fprintf('min area = %f\n', minA);
    if minA > 0.99 * epsilon
        break
    end

    % ---- Y step ----
    hV = [curX, Y, ones(nv,1)];

    constraints = [ A >= 0 , Y(handles) == hdlY ];

    for i = 1:nf
        constraints = [ constraints, -A(i) <= 0.5 * det(hV(face(i,:),:), 'polynomial') <= A(i) ];
    end

    sol = optimize(constraints, objective, opts);

    curY = value(Y);
    
    fprintf('E = %f\n', value(objective));
    

    % ---- terminiation check ----
    minA = 1000;
    for i = 1:nf
        curA = 0.5 * det(value(hV(face(i,:),:)));
        if curA < minA
            minA = curA;
        end
    end
    fprintf('min area = %f\n\n', minA);
    if minA > 0.99 * epsilon
        break
    end

end

% ---- show result mesh ----
triplot(face, curX, curY);

% ---- export result -----
meshObj.vertices = [curX, curY, zeros(nv,1)];
meshObj.objects(1).type = 'f';
meshObj.objects(1).data.vertices = face;

write_wobj(meshObj, outFile);