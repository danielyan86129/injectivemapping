To use the code, you should have YALMIP installed. If not, follow the [guide](https://yalmip.github.io/tutorial/installation/) to install it first.

SLP_unsigned.m and SLP_sumNeg.m are scipts correspond to (LP1, LP2) and (LP3.1, LP3.2) in James' note on [Sequential LP Formulation](https://paper.dropbox.com/doc/Sequential-LP-Formulation--AWhSo7z3YX27QvQrRP9j6FsuAg-eOkgKHiV1C4rOZJUNSDfc) .
Parameter settings are on the top of the files,
```matlab
% ---- parameters ----
handleFile = 'data/James-test1.MAT';
inFile = 'data/James-test1.obj';
outFile = 'data/James-test1-unsigned.obj';

maxIter = 2;
epsilon = 0.001;
opts = sdpsettings('verbose',0);
```
in which *handleFile* stores handle vertex IDs in a column vector, *inFile* stores the initial mesh in OBJ format, *outFile* specifies the file to store the resulting mesh. *epsilon* is a small non-negative number, which tells SLP to terminate when the minimum signed triangle area is no less than *epsilon*. It is also used in formulation (LP3.1, LP3.2). *opts* specifies settings for YALMIP, e.g., which solver to use.

