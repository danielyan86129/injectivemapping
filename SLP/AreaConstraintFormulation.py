from Formulation import Formulation

import pymesh
import numpy as np
import scipy.sparse

class AreaConstraintFormulation(Formulation):
    def compute(self, unknown_coordinate=0):
        assert(unknown_coordinate in (0, 1));

        self.__dof = self.mesh.num_vertices
        self.__unknown_coordinate = unknown_coordinate;

        self.generate_inequality_constraints();
        self.generate_equality_constraints();

    def set_solution(self, sol):
        num_vertices = self.mesh.num_vertices;
        vertices = np.copy(self.mesh.vertices);
        vertices[:, self.__unknown_coordinate] += sol[:num_vertices];
        self.mesh = pymesh.form_mesh(vertices, self.mesh.faces);

    def get_warm_start(self):
        x = np.zeros(self.dof);
        #x[:self.mesh.num_vertices] = self.mesh.vertices[:,self.__unknown_coordinate];
        return x;

    def generate_inequality_constraints(self):
        """ Let A be the area matrix.  We need

             -A * dx  <= 0;

           for all inverted triangles.  I.e. signed area of inverted triangles
           must increase.
        """
        if not self.mesh.has_attribute("face_area"):
            self.mesh.add_attribute("face_area");
        areas = self.mesh.get_attribute("face_area").ravel();

        A = self.generate_area_matrix(self.__unknown_coordinate);
        self.__A_le = -A[areas < 0.0, :];
        self.__b_le = np.zeros(self.__A_le.shape[0]);
        #self.__b_le = 2 * areas[areas < 0.0];

    def generate_equality_constraints(self):
        """ dx = 0 for fixed vertices.
        """
        num_constraints = len(self.constrained_vertices);
        row = np.arange(num_constraints, dtype=int);
        col = self.constrained_vertices;
        value = np.ones(num_constraints);
        self.__A_eq = scipy.sparse.csc_matrix((value, (row, col)),
                shape=(num_constraints, self.dof));

        self.__b_eq = np.zeros(num_constraints);

    @property
    def dof(self):
        return self.__dof;

    @property
    def A_eq(self):
        return self.__A_eq;

    @property
    def b_eq(self):
        return self.__b_eq;

    @property
    def A_le(self):
        return self.__A_le;

    @property
    def b_le(self):
        return self.__b_le;

    @property
    def unknown_coordinate(self):
        return self.__unknown_coordinate;
