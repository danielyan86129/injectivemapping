#!/usr/bin/env python

"""
Remove inverted 2D triangles.
"""

import argparse
import json
import logging
import numpy as np
import os.path
import pymesh

def parse_args():
    parser = argparse.ArgumentParser(__doc__);
    parser.add_argument("--formulation", "-f", default="unsigned_area",
            choices=(
                "arap",
                "dirichlet",
                "unsigned_area",
                "unsigned_area_w_dirichlet",
                "unsigned_area_w_l2",
                "unsigned_area_w_linf",
                "negative_area",
                "negative_area_w_dirichlet",
                "minmax",
                "scaling_factor",
                ));
    parser.add_argument("--num-iterations", "-n", type=int, default=10000,
            help="Total number of iterations");
    parser.add_argument("--free-boundary", action="store_true");
    parser.add_argument("input_mesh");
    parser.add_argument("output_mesh");
    return parser.parse_args();

def correct_orientation(mesh):
    vertices = mesh.vertices;
    faces = mesh.faces;
    v0 = vertices[faces[:,0], :];
    v1 = vertices[faces[:,1], :];
    v2 = vertices[faces[:,2], :];
    area = np.cross(v1-v0, v2-v0);
    if np.sum(area) < 0:
        vertices = vertices[:, [1,0]];
        return pymesh.form_mesh(vertices, faces);
    else:
        return mesh;

def load_input(input_mesh_file, free_boundary):
    name, ext = os.path.splitext(input_mesh_file);
    path, basename = os.path.split(name);

    constraint_file = name + ".json";
    rest_mesh = None;
    if os.path.exists(constraint_file):
        with open(constraint_file) as fin:
            data = json.load(fin);
            constrained_vertices = data.get("idx", []);
            rest_mesh_file = data.get("rest", None);
            rest_mesh_file = os.path.join(path, rest_mesh_file);
            if rest_mesh_file is not None and os.path.exists(rest_mesh_file):
                rest_mesh = pymesh.load_mesh(rest_mesh_file);
    else:
        raise IOError(".json file missing");

    mesh = pymesh.load_mesh(input_mesh_file, drop_zero_dim=True);
    mesh = correct_orientation(mesh);
    assert(mesh.dim == 2);
    if rest_mesh is not None:
        assert(rest_mesh.num_vertices == mesh.num_vertices);
        assert(rest_mesh.num_faces == mesh.num_faces);

    if not free_boundary:
        constrained_vertices += mesh.boundary_vertices.tolist();
    constrained_vertices = np.unique(constrained_vertices);

    return mesh, rest_mesh, constrained_vertices;

def save_output(output_file, mesh):
    if not mesh.has_attribute("face_area"):
        mesh.add_attribute("face_area");
    areas = mesh.get_attribute("face_area").ravel();
    orientation = areas >= 0;
    mesh.add_attribute("orientation");
    mesh.set_attribute("orientation", orientation);
    pymesh.save_mesh(output_file, mesh, "orientation");

def setup(mesh, rest_mesh, constrained_vertices, formulation):
    if formulation == "unsigned_area":
        from UnsignedAreaFormulation import UnsignedAreaFormulation as Formulation
        from Solver import ScipySolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        s = ScipySolver();
    elif formulation == "unsigned_area_w_dirichlet":
        from UnsignedAreaWithDirichletFormulation import UnsignedAreaWithDirichletFormulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        s = OSQPSolver();
    elif formulation == "unsigned_area_w_l2":
        from UnsignedAreaWithL2Formulation import UnsignedAreaWithL2Formulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        s = OSQPSolver();
    elif formulation == "unsigned_area_w_linf":
        from UnsignedAreaWithLinfFormulation import UnsignedAreaWithLinfFormulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        s = OSQPSolver();
    elif formulation == "negative_area":
        from NegativeAreaFormulation import NegativeAreaFormulation as Formulation
        from Solver import ScipySolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        f.eps = 1e-3;
        s = ScipySolver();
    elif formulation == "negative_area_w_dirichlet":
        from NegativeAreaWithDirichletFormulation import NegativeAreaWithDirichletFormulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        f.eps = 1e-3;
        s = OSQPSolver();
    elif formulation == "minmax":
        from MinmaxFormulation import MinmaxFormulation as Formulation
        from Solver import ScipySolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        f.eps = 1e-3
        s = ScipySolver()
    elif formulation == "arap":
        from ArapFormulation import ArapFormulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        f.eps = 1e-3;
        s = OSQPSolver();
    elif formulation == "dirichlet":
        from DirichletFormulation import DirichletFormulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        s = OSQPSolver();
    elif formulation == "scaling_factor":
        from ScalingFactorFormulation import ScalingFactorFormulation as Formulation
        from Solver import OSQPSolver
        f = Formulation(mesh, rest_mesh, constrained_vertices);
        s = OSQPSolver();
    else:
        raise NotImplementedError("Formulation '{}' is not supported".format(formulation));

    return f,s;

def create_formulation(mesh, rest_mesh, constrained_vertices):
    from UnsignedAreaFormulation import UnsignedAreaFormulation
    from UnsignedAreaWithDirichletFormulation import UnsignedAreaWithDirichletFormulation
    from UnsignedAreaWithL2Formulation import UnsignedAreaWithL2Formulation
    #return UnsignedAreaFormulation(mesh, rest_mesh, constrained_vertices);
    #return UnsignedAreaWithDirichletFormulation(mesh, rest_mesh, constrained_vertices);
    return UnsignedAreaWithL2Formulation(mesh, rest_mesh, constrained_vertices);

def create_solver():
    from Solver import ScipySolver, OSQPSolver
    #return ScipySolver();
    return OSQPSolver();

def is_feasible(mesh):
    if not mesh.has_attribute("face_area"):
        mesh.add_attribute("face_area");
    areas = mesh.get_attribute("face_area");
    return np.amin(areas) >= 0;

def has_converged(obj_values):
    if len(obj_values) < 4:
        return False;
    curr_val_x = obj_values[-1];
    prev_val_x = obj_values[-3];
    diff_x = prev_val_x - curr_val_x;
    curr_val_y = obj_values[-2];
    prev_val_y = obj_values[-4];
    diff_y = prev_val_y - curr_val_y;
    return abs(diff_x) < 1e-6 and abs(diff_x) / prev_val_x < 1e-6 and \
        abs(diff_y) < 1e-6 and abs(diff_y) / prev_val_y < 1e-6;

def main():
    args = parse_args();
    mesh, rest_mesh, constrained_vertices = load_input(
            args.input_mesh, args.free_boundary);

    opt_formula, solver = setup(mesh, rest_mesh, constrained_vertices,
            args.formulation);

    obj_values = [];
    for i in range(args.num_iterations):
        # Optimize for x.
        opt_formula.compute(0);
        obj_0 = solver.solve(opt_formula);
        #pymesh.save_mesh("debug_{:03}_x.msh".format(i), opt_formula.mesh);

        # Optimize for y.
        opt_formula.compute(1);
        obj_1 = solver.solve(opt_formula);
        #pymesh.save_mesh("debug_{:03}_y.msh".format(i), opt_formula.mesh);

        obj_values.append(obj_0);
        obj_values.append(obj_1);
        logging.debug("objective: \t{} \t{}".format(obj_0, obj_1));

        if is_feasible(opt_formula.mesh) and has_converged(obj_values):
            print("converged in {} iterations".format(i+1));
            break;
        elif has_converged(obj_values):
            print("optimization converged but with inversions");
            break;
        #if is_stucked(obj_values):
        #    print("coordinate descent appear to have stucked");
        #    break;

    save_output(args.output_mesh, opt_formula.mesh);

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main();

