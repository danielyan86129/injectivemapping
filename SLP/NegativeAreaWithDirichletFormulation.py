from NegativeAreaFormulation import NegativeAreaFormulation
import pymesh
import scipy.sparse

class NegativeAreaWithDirichletFormulation(NegativeAreaFormulation):
    def compute(self, unknown_coordinate=0):
        super(NegativeAreaWithDirichletFormulation,
                self).compute(unknown_coordinate);

        num_faces = self.mesh.num_faces;
        assembler = pymesh.Assembler(self.rest_mesh);
        L = assembler.assemble("laplacian");
        self.__Q = scipy.sparse.bmat([[L, None],
                    [None, scipy.sparse.csc_matrix((num_faces, num_faces))]],
                    format="csc");

    @property
    def Q(self):
        return self.__Q;
