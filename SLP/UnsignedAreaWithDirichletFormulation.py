from UnsignedAreaFormulation import UnsignedAreaFormulation
import pymesh
import scipy.sparse
import numpy as np

class UnsignedAreaWithDirichletFormulation(UnsignedAreaFormulation):
    def compute(self, unknown_coordinate=0):
        super(UnsignedAreaWithDirichletFormulation,
                self).compute(unknown_coordinate);

        num_faces = self.mesh.num_faces;
        assembler = pymesh.Assembler(self.rest_mesh);
        L = assembler.assemble("laplacian");
        self.__Q = scipy.sparse.bmat([[L, None],
                    [None, scipy.sparse.csc_matrix((num_faces, num_faces))]],
                    format="csc");

        x = self.mesh.vertices[:, unknown_coordinate];
        self.__c = np.zeros(self.dof);
        self.__c[self.mesh.num_vertices:] = 1;
        self.__c[:self.mesh.num_vertices] = L * x;
        self.__C = 0.5 * np.dot(x, L*x);

    @property
    def Q(self):
        return self.__Q;

    @property
    def c(self):
        return self.__c;

    @property
    def C(self):
        return self.__C;
