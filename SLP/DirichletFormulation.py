from AreaConstraintFormulation import AreaConstraintFormulation

import pymesh
import numpy as np
import numpy.linalg
import scipy.sparse

class DirichletFormulation(AreaConstraintFormulation):
    def compute(self, unknown_coordinate=0):
        super(DirichletFormulation, self).compute(unknown_coordinate);
        self.generate_objective();

    def generate_objective(self):
        """
        f(x) = x^T L x
             = dx^T L dx + 2 x_0^T L dx + C
        """
        self.__c = np.zeros(self.dof);
        self.__c[self.mesh.num_vertices:] = 1;
        num_vertices = self.mesh.num_vertices;
        num_faces = self.mesh.num_faces;

        assembler = pymesh.Assembler(self.rest_mesh);
        L = assembler.assemble("laplacian");

        x = self.mesh.vertices[:, self.unknown_coordinate];
        self.__c[:num_vertices] = L * x;
        self.__Q = L;
        self.__C = 0.5 * np.trace(np.dot(self.mesh.vertices.T, (L *
            self.mesh.vertices)));
        #self.__Q = scipy.sparse.bmat([[L, None],
        #            [None, scipy.sparse.csc_matrix((num_faces, num_faces))]],
        #            format="csc");

    @property
    def c(self):
        return self.__c;

    @property
    def Q(self):
        return self.__Q;

    @property
    def C(self):
        return self.__C;
