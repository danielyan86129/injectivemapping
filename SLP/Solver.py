import logging
import numpy as np
import scipy.sparse

class Solver(object):
    def solve(self, formulation):
        raise NotImplementedError("this method need to be implemented by subclass");

class ScipySolver(Solver):
    def solve(self, formulation):
        if formulation.Q is not None:
            logging.warning("Ignoring quadratic term");
        c = formulation.c
        A_eq = formulation.A_eq
        b_eq = formulation.b_eq
        A_le = formulation.A_le
        b_le = formulation.b_le

        import scipy.optimize
        r = scipy.optimize.linprog(c, A_le, b_le, A_eq, b_eq,
                bounds = [(None, None) for i in range(formulation.dof)],
                method = "interior-point",
                #method = "simplex",
                options= {
                    #"disp": True,
                    "tol": 1e-6,
                    "maxiter": 1e6,
                    "sparse": True,
                    #"lstsq": True,
                    }
                );

        if r.success:
            obj = r.fun;
            formulation.set_solution(r.x);
        else:
            logging.error("LP failed with status: {}".format( r.status));
            logging.error(r.message);
            assert(False);

        return obj + formulation.C;

class OSQPSolver(Solver):
    def __init__(self):
        import osqp;
        self.solver = osqp.OSQP();

    def solve(self, formulation):
        A = scipy.sparse.bmat([
            [formulation.A_eq],
            [formulation.A_le],
            ], format="csc");
        l = np.concatenate([formulation.b_eq,
                np.full(formulation.A_le.shape[0], -np.inf)]);
        u = np.concatenate([formulation.b_eq, formulation.b_le]);
        self.solver.setup(
                formulation.Q,
                formulation.c,
                A, l, u,
                verbose=False,
                polish=True,
                #eps_prim_inf=0,
                #eps_dual_inf=0,
                eps_abs=5e-10,
                #eps_rel=1e-12,
                max_iter=1e7,
                #polish_refine_iter=1e7,
                #rho=1e-12,
                #adaptive_rho_fraction=1.0,
                warm_start=True,
                );

        primal_start = formulation.get_warm_start();
        self.solver.warm_start(x=primal_start);
        r = self.solver.solve();
        if r.info.status == "solved":
            obj = r.info.obj_val;
            formulation.set_solution(r.x);
        else:
            logging.error("OSQP failed with status: {}".format(r.info.status));
            logging.error("Error code: {}".format(r.info.status_val));
            assert(False);

        return obj + formulation.C;


