from UnsignedAreaFormulation import UnsignedAreaFormulation

import pymesh
import numpy as np
import numpy.linalg
import scipy.sparse

class ArapFormulation(UnsignedAreaFormulation):
    def compute(self, unknown_coordinate=0):
        self.__unknown_coordinate = unknown_coordinate;
        super(ArapFormulation, self).compute(unknown_coordinate);

    def generate_objective(self):
        """
        f(x) = |e(x) - R e(x)|^2
             = x^T L x - 2 x^T R L x_r + C;
             = (x0+dx)^T L (x0+dx) - 2 (x0+dx)^T G^T A R G x_r + C;
             = 2x0^T L dx + dx^T L dx - 2dx^T G^T A R G x_r + C
             = dx^T L dx + 2x0^T L dx - 2x_r G^T R^T A G dx + C
             = dx^T L dx + (2x0^T L - 2x_r G^T R^T A G) dx + C
        """
        dim = self.mesh.dim;
        faces = self.rest_mesh.faces;
        num_vertices = self.rest_mesh.num_vertices;
        num_faces = self.rest_mesh.num_faces;

        if not self.rest_mesh.has_attribute("vertex_valance"):
            self.rest_mesh.add_attribute("vertex_valance");
        valance = self.rest_mesh.get_vertex_attribute("vertex_valance").ravel();
        if not self.rest_mesh.has_attribute("face_area"):
            self.rest_mesh.add_attribute("face_area");
        areas = self.rest_mesh.get_attribute("face_area");

        assembler = pymesh.Assembler(self.rest_mesh);
        L = assembler.assemble("laplacian");
        G = assembler.assemble("gradient");
        A = scipy.sparse.kron(scipy.sparse.diags(areas), np.identity(dim));
        assert(G.shape[0] == num_faces * dim);
        assert(G.shape[1] == num_vertices);
        grad = G * self.mesh.vertices;

        l0 = A * (G * self.rest_mesh.vertices);
        self.__c = np.zeros(self.dof);

        if self.__unknown_coordinate == 0:
            R = np.zeros((num_faces * 2, 2));
            R_diag = [];
            for i in range(num_faces):
                f = faces[i];
                u,s,vh = numpy.linalg.svd(grad[i*dim:(i+1)*dim,:].T);
                R_diag.append(np.dot(u, vh));
            self.__R = scipy.sparse.block_diag(R_diag);

        x_r = self.rest_mesh.vertices[:, self.__unknown_coordinate];
        x_0 = self.mesh.vertices[:, self.__unknown_coordinate];
        self.__c[:num_vertices] = L * x_0 -\
                x_r.T * G.T * self.__R * A * G;
        self.__Q = scipy.sparse.bmat([[L, None],
                    [None, scipy.sparse.csc_matrix((num_faces, num_faces))]],
                    format="csc");

    @property
    def c(self):
        return self.__c;

    @property
    def Q(self):
        return self.__Q;

    @property
    def A_le(self):
        return scipy.sparse.csc_matrix((0, self.dof));

    @property
    def b_le(self):
        return np.zeros(0);

