from Formulation import Formulation

import pymesh
import numpy as np
import scipy.sparse

class ScalingFactorFormulation(Formulation):
    def compute(self, unknown_coordinate=0):
        assert(unknown_coordinate in (0, 1));

        self.__dof = self.mesh.num_vertices;
        self.__unknown_coordinate = unknown_coordinate;

        self.generate_objective();
        self.generate_equality_constraints();

    def set_solution(self, sol):
        num_vertices = self.mesh.num_vertices;
        vertices = np.copy(self.mesh.vertices);
        x0 = vertices[:,self.__unknown_coordinate];
        dx = sol[:num_vertices];
        vertices[:, self.__unknown_coordinate] += sol[:num_vertices];
        self.mesh = pymesh.form_mesh(vertices, self.mesh.faces);

    def get_warm_start(self):
        return np.zeros(self.__dof);

    def generate_objective(self):
        if not self.rest_mesh.has_attribute("face_area"):
            self.rest_mesh.add_attribute("face_area");
        areas = self.rest_mesh.get_face_attribute("face_area").ravel();
        assert(np.all(areas > 0));

        x = self.mesh.vertices[:,self.__unknown_coordinate];
        A = self.generate_area_matrix(self.__unknown_coordinate);
        D = scipy.sparse.dia_matrix(([[1.0 / a for a in areas]], [0]),
                shape=(len(areas), len(areas)));
        #L = self.generate_dual_laplacian();
        L = self.generate_dual_matrix();
        self.__Q = A.T * D.T * L * D * A;
        self.__c = self.__Q * x;
        self.__C = 0.5 * np.dot(x, self.__c)

    def generate_dual_laplacian(self):
        self.rest_mesh.enable_connectivity();
        row_ids = [];
        col_ids = [];
        values = [];
        for i in range(self.rest_mesh.num_faces):
            adj_faces = self.rest_mesh.get_face_adjacent_faces(i);
            row_ids.append(i);
            col_ids.append(i);
            values.append(len(adj_faces));
            for j in adj_faces:
                row_ids.append(i);
                col_ids.append(j);
                values.append(-1);
        return scipy.sparse.coo_matrix((values, (row_ids, col_ids)),
                shape=(self.rest_mesh.num_faces, self.rest_mesh.num_faces));

    def generate_dual_matrix(self):
        self.rest_mesh.enable_connectivity();
        row_ids = [];
        col_ids = [];
        values = [];
        for i in range(self.rest_mesh.num_faces):
            adj_faces = self.rest_mesh.get_face_adjacent_faces(i);
            for j in adj_faces:
                row_ids.append(i);
                col_ids.append(i);
                values.append(1);
                row_ids.append(j);
                col_ids.append(j);
                values.append(1);
                row_ids.append(i);
                col_ids.append(j);
                values.append(-1);
                row_ids.append(j);
                col_ids.append(i);
                values.append(-1);
        return scipy.sparse.coo_matrix((values, (row_ids, col_ids)),
                shape=(self.rest_mesh.num_faces, self.rest_mesh.num_faces));

    def generate_equality_constraints(self):
        """ dx = 0 for fixed vertices.
        """
        num_constraints = len(self.constrained_vertices);
        row = np.arange(num_constraints, dtype=int);
        col = self.constrained_vertices;
        value = np.ones(num_constraints);
        self.__A_eq = scipy.sparse.csc_matrix((value, (row, col)),
                shape=(num_constraints, self.dof));

        self.__b_eq = np.zeros(num_constraints);

    @property
    def dof(self):
        return self.__dof;

    @property
    def A_eq(self):
        return self.__A_eq;

    @property
    def b_eq(self):
        return self.__b_eq;

    @property
    def A_le(self):
        return np.zeros((0, self.dof));

    @property
    def b_le(self):
        return np.zeros(0);

    @property
    def c(self):
        return self.__c;

    @property
    def Q(self):
        return self.__Q;

    @property
    def C(self):
        return self.__C;

