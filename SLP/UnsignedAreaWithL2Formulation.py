from UnsignedAreaFormulation import UnsignedAreaFormulation
import pymesh
import scipy.sparse

class UnsignedAreaWithL2Formulation(UnsignedAreaFormulation):
    def compute(self, unknown_coordinate=0):
        super(UnsignedAreaWithL2Formulation, self).compute(unknown_coordinate);

        num_faces = self.mesh.num_faces;
        self.__Q = scipy.sparse.identity(self.dof).tocsc();

    @property
    def Q(self):
        return self.__Q;
