from Formulation import Formulation

import pymesh
import numpy as np
import scipy.sparse

class UnsignedAreaFormulation(Formulation):
    def compute(self, unknown_coordinate=0):
        assert(unknown_coordinate in (0, 1));

        self.__dof = self.mesh.num_vertices + self.mesh.num_faces;
        self.__unknown_coordinate = unknown_coordinate;

        self.generate_objective();
        self.generate_inequality_constraints();
        self.generate_equality_constraints();

    def set_solution(self, sol):
        num_vertices = self.mesh.num_vertices;
        vertices = np.copy(self.mesh.vertices);
        vertices[:, self.__unknown_coordinate] += sol[:num_vertices];
        self.mesh = pymesh.form_mesh(vertices, self.mesh.faces);

    def get_warm_start(self):
        if not self.mesh.has_attribute("face_area"):
            self.mesh.add_attribute("face_area");
        areas = self.mesh.get_attribute("face_area");
        x = np.zeros(self.dof);
        #x[:self.mesh.num_vertices] = self.mesh.vertices[:,self.__unknown_coordinate];
        x[self.mesh.num_vertices:] = np.absolute(areas) + 1e-3;
        return x;

    def generate_objective(self):
        self.__c = np.zeros(self.dof);
        self.__c[self.mesh.num_vertices:] = 1;

    def generate_inequality_constraints(self):
        """ Let A be the area matrix.  We need

             A * dx - a_i <= -area
            -A * dx - a_i <=  area
                     -a_i <= 0
        """
        if not self.mesh.has_attribute("face_area"):
            self.mesh.add_attribute("face_area");
        areas = self.mesh.get_attribute("face_area").ravel() * 2;

        A = self.generate_area_matrix(self.__unknown_coordinate);
        I = scipy.sparse.identity(self.mesh.num_faces);

        self.__A_le = scipy.sparse.bmat([
            [   A, -I],
            [  -A, -I],
            [None, -I]], format="csc");
        #self.__b_le = np.zeros(3 * self.mesh.num_faces);
        self.__b_le = np.concatenate([-areas, areas,
            np.zeros(self.mesh.num_faces)]);

    def generate_equality_constraints(self):
        """ dx = 0 for fixed vertices.
        """
        num_constraints = len(self.constrained_vertices);
        row = np.arange(num_constraints, dtype=int);
        col = self.constrained_vertices;
        value = np.ones(num_constraints);
        self.__A_eq = scipy.sparse.csc_matrix((value, (row, col)),
                shape=(num_constraints, self.dof));

        self.__b_eq = np.zeros(num_constraints);
        #self.__b_eq = self.mesh.vertices[
        #        self.constrained_vertices, self.__unknown_coordinate];

    @property
    def dof(self):
        return self.__dof;

    @property
    def c(self):
        return self.__c;

    @property
    def A_eq(self):
        return self.__A_eq;

    @property
    def b_eq(self):
        return self.__b_eq;

    @property
    def A_le(self):
        return self.__A_le;

    @property
    def b_le(self):
        return self.__b_le;

