#!/usr/bin/env python

"""
Render with wire frame and inversion highlight.
"""

import argparse
import os
import os.path
import subprocess
import pymesh
import json
import tempfile
import numpy as np

def parse_args():
    parser = argparse.ArgumentParser(__doc__);
    parser.add_argument("--free-boundary", action="store_true",
            help="whether mesh boundary is free.");
    parser.add_argument("mesh_file");
    return parser.parse_args();

def correct_orientation(mesh):
    vertices = mesh.vertices;
    faces = mesh.faces;
    v0 = vertices[faces[:,0], :];
    v1 = vertices[faces[:,1], :];
    v2 = vertices[faces[:,2], :];
    area = np.cross(v1-v0, v2-v0);
    if np.sum(area) < 0:
        vertices = vertices[:, [1,0]];
        return pymesh.form_mesh(vertices, faces);
    else:
        return mesh;

def generate_constraint_mesh(mesh, mesh_file, free_boundary=False):
    if free_boundary:
        anchors = np.zeros((0, mesh.dim));
    else:
        anchors = mesh.vertices[mesh.boundary_vertices];

    name, ext = os.path.splitext(mesh_file);
    path, name = os.path.split(name);
    if "_" in name:
        fields = name.split("_");
        name = fields[0];
    json_file = os.path.join("data", name + ".json");
    assert(os.path.exists(json_file));
    with open(json_file, 'r') as fin:
        data = json.load(fin);
        idx = data.get("idx", []);
        constraints = mesh.vertices[idx];

    all_anchors = np.vstack([anchors, constraints]);
    faces = np.zeros((0, 3));
    anchor_mesh = pymesh.form_mesh(all_anchors, faces);
    label = np.zeros(anchor_mesh.num_vertices);
    label[len(anchors):] = 1;
    anchor_mesh.add_attribute("label");
    anchor_mesh.set_attribute("label", label);
    return anchor_mesh;

def main():
    args = parse_args();
    cmd = "render.py --renderer mitsuba --width=1200 --height=1000 -B n --transparent-bg --front-direction=Z --facing-camera --head-on -S {}"

    scene = {
                "views": [
                    {
                        "type": "composite",
                        "views": [
                            {
                                "type": "wire_network",
                                "wire_network": None,
                                "bbox": [[-5, -5, -5], [5, 5, 5]],
                                "color": "black",
                                "radius": 0.025,
                            },
                            {
                                "type": "mesh_only",
                                "mesh": None,
                                "color": "light_red",
                                "bbox": [[-5, -5, -5], [5, 5, 5]],
                            },
                            {
                                "type": "mesh_only",
                                "mesh": None,
                                "color": "yellow",
                                "bbox": [[-5, -5, -5], [5, 5, 5]],
                            },
                            {
                                "type": "sphere",
                                "scalar": "label",
                                "color_map": "contrast_yr",
                                "radius_range": [0.1, 0.2],
                                "bounds": [0, 1],
                                "view": {
                                    "type": "mesh_only",
                                    "mesh": None,
                                    "bbox": [[-5, -5, -5], [5, 5, 5]],
                                }
                            }
                        ],
                        "name": None
                    }
                ]
            };

    temp_dir = tempfile.gettempdir();

    f = args.mesh_file;
    name, ext = os.path.splitext(f);
    path, basename = os.path.split(name);

    wire_file = os.path.join(temp_dir, "{}.wire".format(basename));
    mesh = pymesh.load_mesh(f, drop_zero_dim=True);
    mesh = correct_orientation(mesh);
    V,E = pymesh.mesh_to_graph(mesh);
    V = np.hstack([V, np.zeros((mesh.num_vertices, 1))]);
    wires = pymesh.wires.WireNetwork.create_from_data(V, E);
    wires.write_to_file(wire_file);

    if not mesh.has_attribute("orientation"):
        mesh.add_attribute("face_area");
        areas = mesh.get_attribute("face_area").ravel();
        mesh.add_attribute("orientation");
        mesh.set_attribute("orientation", areas >= 0.0);

    orientation = mesh.get_attribute("orientation").ravel().astype(bool);
    bad_triangles = np.logical_not(orientation);
    bad_indices = np.arange(mesh.num_faces, dtype=int)[bad_triangles];
    bad_mesh = pymesh.submesh(mesh, bad_indices, 0);
    bad_mesh_file = os.path.join(temp_dir, "{}_bad.msh".format(basename));
    pymesh.save_mesh(bad_mesh_file, bad_mesh, "orientation");

    V[:,2] -= 1e-3;
    offset_mesh = pymesh.form_mesh(V, mesh.faces);
    offset_mesh_file = os.path.join(temp_dir, "{}_offset.msh".format(basename));
    pymesh.save_mesh(offset_mesh_file, offset_mesh);

    constraint_mesh = generate_constraint_mesh(mesh, args.mesh_file,
            args.free_boundary);
    constraint_mesh_file = os.path.join(temp_dir, "{}_constraint.msh".format(basename));
    pymesh.save_mesh(constraint_mesh_file, constraint_mesh, "label");

    scene["views"][0]["views"][0]["wire_network"] = wire_file;
    scene["views"][0]["views"][1]["mesh"] = os.path.abspath(bad_mesh_file);
    scene["views"][0]["views"][2]["mesh"] = os.path.abspath(offset_mesh_file);
    scene["views"][0]["views"][3]["view"]["mesh"] = os.path.abspath(constraint_mesh_file);
    scene["views"][0]["name"] = "{}.png".format(name);

    json_file = os.path.join(temp_dir, "{}.json".format(basename));
    with open(json_file, 'w') as fout:
        json.dump(scene, fout);
    subprocess.check_call(cmd.format(json_file).split());

if __name__ == "__main__":
    main();

