import scipy.sparse

class Formulation(object):
    """ Base class for the formulation of LP and QP problems.

        min 0.5 * x^T Q x + c^T x + C
        s.t.
            A_eq x  = b_eq
            A_le x <= b_le

        Note that one can set Q to None to reduce the problem to LP.
        C is the optional constant that does not change the minimizer.
    """

    def __init__(self, mesh, rest_mesh=None, constrained_vertices=[]):
        self.mesh = mesh;
        self.rest_mesh = rest_mesh;
        self.constrained_vertices = constrained_vertices;

    def compute(self, unknown_coordinate=0):
        raise NotImplementedError(
                "This method need to be implemented by subclass");

    def set_solution(self, sol):
        raise NotImplementedError(
                "This method need to be implemented by subclass");

    def get_warm_start(self):
        raise NotImplementedError(
                "This method need to be implemented by subclass");

    def generate_area_matrix(self, unknown_coordinate=0):
        """ Assemble a #F by #V matrix A such that
                facet_areas = A * x  if unknown_coordinate==0.
                facet_areas = A * y  if unknown_coordinate==1.
        """
        assert(unknown_coordinate in (0, 1));
        if unknown_coordinate == 0:
            vid = 1;
            factor = 1;
        else:
            vid = 0;
            factor = -1;

        num_vertices = self.mesh.num_vertices;
        num_faces = self.mesh.num_faces;
        vertices = self.mesh.vertices;
        row = [];
        col = [];
        value = [];
        for i,f in enumerate(self.mesh.faces):
            v0 = vertices[f[0]];
            v1 = vertices[f[1]];
            v2 = vertices[f[2]];

            row.append(i);
            col.append(f[0]);
            value.append((v1[vid] - v2[vid]) * factor);
            row.append(i);
            col.append(f[1]);
            value.append((v2[vid] - v0[vid]) * factor);
            row.append(i);
            col.append(f[2]);
            value.append((v0[vid] - v1[vid]) * factor);

        A = scipy.sparse.csc_matrix((value, (row, col)),
                shape=(num_faces, num_vertices));
        return A;

    @property
    def dof(self):
        return 0;

    @property
    def Q(self):
        return None;

    @property
    def c(self):
        return None;

    @property
    def A_eq(self):
        return None;

    @property
    def b_eq(self):
        return None;

    @property
    def A_le(self):
        return None;

    @property
    def b_le(self):
        return None;

    @property
    def C(self):
        return 0;
