#!/usr/bin/env python

"""
Extract the anchors as a mesh.
"""

import argparse
import pymesh
import numpy as np
import json
import os.path

def parse_args():
    parser = argparse.ArgumentParser(__doc__);
    parser.add_argument("input_mesh");
    parser.add_argument("output_mesh");
    return parser.parse_args();

def correct_orientation(mesh):
    vertices = mesh.vertices;
    faces = mesh.faces;
    v0 = vertices[faces[:,0], :];
    v1 = vertices[faces[:,1], :];
    v2 = vertices[faces[:,2], :];
    area = np.cross(v1-v0, v2-v0);
    if np.sum(area) < 0:
        vertices = vertices[:, [1,0]];
        return pymesh.form_mesh(vertices, faces);
    else:
        return mesh;

def main():
    args = parse_args();
    mesh = pymesh.load_mesh(args.input_mesh, drop_zero_dim=True);
    mesh = correct_orientation(mesh);
    anchors = mesh.vertices[mesh.boundary_vertices];

    name, ext = os.path.splitext(args.input_mesh);
    json_file = name + ".json";
    with open(json_file, 'r') as fin:
        data = json.load(fin);
        idx = data.get("idx", []);
        constraints = mesh.vertices[idx];

    all_anchors = np.vstack([anchors, constraints]);
    faces = np.zeros((0, 3));
    anchor_mesh = pymesh.form_mesh(all_anchors, faces);
    label = np.zeros(anchor_mesh.num_vertices);
    label[len(anchors):] = np.arange(len(constraints)) + 1;
    anchor_mesh.add_attribute("label");
    anchor_mesh.set_attribute("label", label);

    pymesh.save_mesh(args.output_mesh, anchor_mesh, "label");

if __name__ == "__main__":
    main();

